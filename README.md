Grouped timers for Pebble
=========================
<a href="https://gitlab.com/spencewenski/pebble_grouped_timers/commits/master"><img alt="build status" src="https://gitlab.com/spencewenski/pebble_grouped_timers/badges/master/build.svg" /></a>

This is a powerful timer app that allows you to group timers into lists. Timer
groups can be set to automatically progress through the list, starting each
timer after the previous one finishes. Multiple timers can run at the same time,
even if they are in the same group.


## Documentation
Code documentation can be found [here](https://spencewenski.gitlab.io/pebble_grouped_timers/).


## Installation

### Automatically from the app store
- [Web link](https://apps.getpebble.com/applications/58f985730dfc329fda001649)
- [App store deep link](pebble://appstore/58f985730dfc329fda001649)

### Manually from a `.pbw` file
1. Download the `pebble_grouped_timers.pbw` file on your phone from one of the following:
    - [Latest debug build](https://gitlab.com/spencewenski/pebble_grouped_timers/builds/artifacts/master/browse/?job=debug_deploy)
    - [1.5 release build](https://gitlab.com/spencewenski/pebble_grouped_timers/builds/artifacts/1.5/browse/?job=release_deploy)
    - [1.5 debug build](https://gitlab.com/spencewenski/pebble_grouped_timers/builds/artifacts/1.5/browse/?job=debug_deploy)
    - [1.4 release build](https://gitlab.com/spencewenski/pebble_grouped_timers/builds/artifacts/1.4/browse/?job=release_deploy)
    - [1.4 debug build](https://gitlab.com/spencewenski/pebble_grouped_timers/builds/artifacts/1.4/browse/?job=debug_deploy)
    - [1.3 release build](https://gitlab.com/spencewenski/pebble_grouped_timers/builds/artifacts/1.3/browse/?job=release_deploy)
    - [1.3 debug build](https://gitlab.com/spencewenski/pebble_grouped_timers/builds/artifacts/1.3/browse/?job=debug_deploy)
    - [1.2 release build](https://gitlab.com/spencewenski/pebble_grouped_timers/builds/artifacts/1.2/browse/?job=release_deploy)
    - [1.2 debug build](https://gitlab.com/spencewenski/pebble_grouped_timers/builds/artifacts/1.2/browse/?job=debug_deploy)
2. The app should automatically open in the Pebble app.
3. Click "Ok" in the Pebble app to complete the installation.

### From source
1. Install the [pebble sdk](https://developer.pebble.com/sdk/)
2. Download, build, and install:

        git clone https://gitlab.com/spencewenski/pebble_grouped_timers.git
        cd pebble_grouped_timers
        # Setup the repo
        ./setup.sh
        # Build the app
        make
        # Install on an emulator
        make install
        # Install on your watch (through your phone's developer connection)
        pebble install --phone <ip-address>


## Settings
Each setting is available as a global app setting and as a group setting.
Group settings have an extra option to use the app setting.

### Repeat styles
How to repeat when a timer elapses.
- None: Don't repeat. Continue to the next timer, but don't repeat the group.
- Single: Repeat the current timer.
- Group: Continue to the next timer, and repeat the group when the last timer
elapses.

### Progress styles
How to progress when a timer elapses.
- Auto start next: Automatically continue to the next timer when the current
timer elapses.
- Wait for user: Wait for the user to stop the current timer before
automatically continuing to the next timer.

### Vibrate intervals
How long to wait between each vibration.
- Every few seconds
- Every minute
- Every five minutes

### Vibrate styles
How to vibrate when a timer elapses.
- Short pulse: Single short pulse
- Long pulse: Single long pulse
- Double pulse: Double short pulse
- Extended (since 1.4): Continuous vibration for 5, 10, 20, 30, or 60 seconds.


## Advanced settings

### Extended vibes
Enable or disable the 'Extended' vibration style.
