#!/usr/bin/env sh
#
# Setup the directory on first download

# Setup submodules
git submodule update --init --recursive

# Configure git
git config commit.template .gitmessage
