#ifndef MODEL_H
#define MODEL_H

/**
 * @addtogroup Model
 * @{
 *
 * The {@link Model} is a singleton representing all the data of the app.
 *
 * The {@link Model} manages all the {@link Group Groups}, {@link Timer Timers},
 * and {@link Settings} of the app, as well as notifying the {@link View Views}
 * of changes in the app's data.
 */

#include "Settings_fields.h"
#include "Advanced_settings_fields.h"
#include "Timer_state.h"

struct Group;
struct Timer;
struct View;
struct Settings;

/**
 * Tell the {@link Model} that the app has started.
 *
 * Called once when the app is opened.
 */
void model_start();

/**
 * Tell the {@link Model} that the app is closing.
 *
 * Caled once when the app is being closed.
 */
void model_stop();

/**
 * Get the next available {@link Group} id.
 *
 * @return The next available {@link Group} id.
 */
int model_get_next_group_id();

/**
 * Add the {@link Group} to the {@link Model}.
 *
 * @param group {@link Group} to add to the {@link Model}.
 */
void model_add_group(struct Group* group);

/**
 * Remove the {@link Group} from the {@link Model}.
 *
 * @param group {@link Group} to remove from the {@link Model}.
 */
void model_remove_group(struct Group* group);

/**
 * Get the {@link Group} with the given id.
 *
 * @param  group_id Id of the {@link Group} to get from the {@link Model}.
 * @return          {@link Group} with the given id, or NULL if no {@link Group}
 *                  has the given id.
 */
struct Group* model_get_group(int group_id);

/**
 * Get the next available {@link Timer} id.
 *
 * @return The next available {@link Timer} id.
 */
int model_get_next_timer_id();

/**
 * Add the {@link Timer} to the {@link Model}.
 *
 * @param timer     {@link Timer} to add to the {@link Model}.
 * @param group_id  The id of the {@link Group} that the {@link Timer} belongs
 *                  to.
 */
void model_add_timer(struct Timer* timer, int group_id);

/**
 * Remove the {@link Timer} from the {@link Model}.
 *
 * @param timer {@link Timer} to remove from the {@link Model}.
 */
void model_remove_timer(struct Timer* timer);

/**
 * Get the {@link Timer} with the given id.
 *
 * @param  timer_id Id of the {@link Timer} to get from the {@link Model}.
 * @return          {@link Timer} with the given id, or NULL if no {@link Timer}
 *                  has the given id.
 */
struct Timer* model_get_timer(int timer_id);

/**
 * Get the {@link Timer} that comes after the {@link Timer} with the given id.
 *
 * The {@link Settings} of the {@link Timer Timer's} {@link Group} are used to
 * determine the next {@link Timer}.
 *
 * @param  timer_id The current {@link Timer}.
 * @return          The {@link Timer} that comes after the {@link Timer} with
 *                  the given id.
 */
struct Timer* model_get_next_timer(int timer_id);

/**
 * Get the global app {@link Settings}.
 *
 * @return The global app {@link Settings}.
 */
struct Settings* model_get_settings();

/**
 * Get the {@link Advanced_settings}.
 *
 * @return The {@link Advanced_settings}.
 */
struct Advanced_settings* model_get_advanced_settings();

/**
 * Notify the {@link View Views} of the {@link Timer Timer's} duration.
 *
 * @param timer_id {@link Timer} that the notification is referring to.
 * @param duration Duration of the {@link Timer} in seconds.
 */
void model_notify_duration(int timer_id, int duration);

/**
 * Notify the {@link View Views} of how much timer is left for the given
 * {@link Timer}.
 *
 * @param timer_id  {@link Timer} that the notification is referring to.
 * @param remaining The number of seconds remaining for the {@link Timer}.
 */
void model_notify_remaining(int timer_id, int remaining);

/**
 * Notify the {@link View Views} that the {@link Timer} was deleted.
 *
 * @param timer_id {@link Timer} that was deleted.
 */
void model_notify_timer_gone(int timer_id);

/**
 * Notify the {@link View Views} of the {@link Timer Timer's}
 * {@link Timer_state state}.
 *
 * @param timer_id    {@link Timer} that the notification is referring to
 * @param timer_state New {@link Timer_state} of the {@link Timer}.
 */
void model_notify_timer_state(int timer_id, Timer_state_t timer_state);

/**
 * Notify the {@link View Views} that the {@link Group} exists.
 *
 * @param group_id {@link Group} that the notification is referring to.
 */
void model_notify_group_exists(int group_id);

/**
 * Notify the {@link View Views} that the {@link Group} was deleted.
 *
 * @param group_id {@link Group} that the notification is referring to.
 */
void model_notify_group_gone(int group_id);

/**
 * Notify the {@link View Views} of the app or {@link Group} {@link Settings}.
 *
 * @param group_id         Id of the {@link Group} that the {@link Settings} are
 *                         for, or negative if the {@link Settings} are the
 *                         global app {@link Settings}.
 * @param repeat_style     The {@link Repeat_style_t} for the {@link Group} or
 *                         app.
 * @param progress_style   The {@link Progress_style_t} for the {@link Group} or
 *                         app.
 * @param vibrate_interval The {@link Vibrate_interval_t} for the {@link Group} or
 *                         app.
 * @param vibrate_style    The {@link Vibrate_style_t} for the {@link Group} or
 *                         app.
 */
void model_notify_settings(int group_id, Repeat_style_t repeat_style,
  Progress_style_t progress_style, Vibrate_interval_t vibrate_interval,
  Vibrate_style_t vibrate_style);

/**
 * Notify the {@link View Views} of the {@link Advanced_settings}.
 *
 * @param extended_vibes_state  The {@link Extended_vibes_state_t}.
 */
void model_notify_advanced_settings(
  Extended_vibes_state_t extended_vibes_state);

/**
 * Notify the {@link View Views} of the current {@link Timer}.
 *
 * @param timer_id          New {@link Timer} to track.
 * @param previous_timer_id The previous {@link Timer} that was being tracked,
 *                          or negative a) if no previous timer was being
 *                          tracked, or b) the {@link View} should force update
 *                          the {@link Timer} it is tracking.
 */
void model_notify_timer_tracking(int timer_id, int previous_timer_id);

/**
 * Notify the {@link View Views} that an error occured.
 *
 * @param msg The error message.
 */
void model_notify_error(char* msg);

/**
 * Attach the {@link View} to the {@link Model}.
 *
 * After this, the {@link View} will receive all notifications until it is
 * detached with {@link model_detach_view}.
 *
 * @param view {@link View} to attach to the {@link Model}.
 */
void model_attach_view(struct View* view);

/**
 * Detach the {@link View} from the {@link Model}.
 *
 * Expects that the {@link View} was previous attached to the {@link Model} with
 * {@link model_attach_view}, but fails gracefully if this is not the case.
 *
 * @param view {@link View} to detach from the {@link Model}.
 */
void model_detach_view(struct View* view);

/** @} // Model */

#endif /*MODEL_H*/
