/**
 * @internal
 * @addtogroup List
 * @{
 */

#include "List.h"
#include "Utility.h"
#include "assert.h"

#include <pebble.h>

/**
 * Default allocation size of new {@link List}.
 */
#define DEFAULT_ARRAY_SIZE 3

/**
 * How much to grow the allocation size by when the current size is exceeded.
 */
#define GROW_FACTOR 2

/**
 * Data required for a {@link List}.
 */
struct List {
  /**
   * Points to an array of pointers to void.
   */
  void** array;
  /**
   * Number of elements currently allocated in the array.
   */
  int allocated_size;
  /**
   * Number of elements currently in the array.
   */
  int size;
};

/**
 * Initialize an empty {@link List}.
 *
 * @param list {@link List} object to initialize.
 */
static void init_empty_intern(struct List* list);

/**
 * Grow allocated size of the {@link List} by the {@link GROW_FACTOR}
 *
 * @param list {@link List} to grow the allocated size of.
 */
static void grow_list_intern(struct List* list);

struct List* list_create()
{
	struct List* list = safe_alloc(sizeof(struct List));
  init_empty_intern(list);
  return list;
}

static void init_empty_intern(struct List* list)
{
  assert(list);
  list->array = safe_alloc(sizeof(void*) * DEFAULT_ARRAY_SIZE);
  list->allocated_size = DEFAULT_ARRAY_SIZE;
  list->size = 0;
}

void list_destroy(struct List* list)
{
  assert(list);
	free(list->array);
  list->array = NULL;
  free(list);
}

void list_clear(struct List* list)
{
  assert(list);
	free(list->array);
  init_empty_intern(list);
}

void list_add(struct List* list, void* item)
{
  assert(list);
  if (list->size >= list->allocated_size) {
    grow_list_intern(list);
  }
  list->array[list->size++] = item;
}

static void grow_list_intern(struct List* list)
{
  int new_allocated_size = GROW_FACTOR * (list->size + 1);
  void** new_array = safe_alloc(sizeof(void*) * new_allocated_size);
  for (int i = 0; i < list->size; ++i) {
    new_array[i] = list->array[i];
  }
  free(list->array);
  list->array = new_array;
  list->allocated_size = new_allocated_size;
}

int list_size(const struct List* list)
{
  assert(list);
  return list->size;
}

int list_empty(const struct List* list)
{
  assert(list);
  return list->size ? 0 : 1;
}

void* list_get(const struct List* list, int index)
{
  assert(list);
  if (!in_range(index, 0, list->size)) {
    APP_LOG(APP_LOG_LEVEL_ERROR, "invalid index");
    return NULL;
  }
  return list->array[index];
}

void list_remove(struct List* list, int index)
{
  assert(list);
  if (!in_range(index, 0, list->size)) {
    APP_LOG(APP_LOG_LEVEL_ERROR, "invalid index: %d", index);
    return;
  }
  // The order of the list matters, so we need to shift all elements after
  // index forward by one
  for (int i = index; i < list->size - 1; ++i) {
    list->array[i] = list->array[i + 1];
  }
  list->array[--list->size] = NULL;
}

void list_remove_ptr(struct List* list, void* data_ptr)
{
  assert(list);
  assert(data_ptr);
  int index = list_find_ptr_index(list, data_ptr);
  if (!in_range(index, 0, list->size)) {
    return;
  }
  list_remove(list, index);
}

void list_for_each(const struct List* list, List_for_each_fp_t func_ptr)
{
  assert(list);
  assert(func_ptr);
  for (int i = 0; i < list->size; ++i) {
    func_ptr(list->array[i]);
  }
}

void* list_find(const struct List* list, const void* data_ptr, List_compare_fp_t func_ptr)
{
  assert(list);
  assert(func_ptr);
  return list_find_arg(list, data_ptr, func_ptr);
}

void* list_find_ptr(const struct List* list, const void* data_ptr)
{
  assert(list);
  int index = list_find_ptr_index(list, data_ptr);
  return in_range(index, 0, list->size) ? list->array[index] : NULL;
}

int list_find_ptr_index(const struct List* list, const void* data_ptr)
{
  assert(list);
  for (int i = 0; i < list->size; ++i) {
    if (data_ptr == list->array[i]) {
      return i;
    }
  }
  return -1;
}

void* list_find_arg(const struct List* list, const void* arg_ptr, List_compare_arg_fp_t func_ptr)
{
  assert(list);
  for (int i = 0; i < list->size; ++i) {
    if (!func_ptr(arg_ptr, list->array[i])) {
      return list->array[i];
    }
  }
  return NULL;
}


/** @} // List */
