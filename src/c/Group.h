#ifndef GROUP_H
#define GROUP_H

/**
 * @addtogroup Group
 * @{
 *
 * A group of {@link Timer Timers}.
 */

struct Group;
struct Timer;
struct List;
struct Settings;

/**
 * Create a {@link Group}.
 *
 * @param  group_id Id of the {@link Group} to create.
 * @return          A new {@link Group} with the given id.
 */
struct Group* group_create(int group_id);

/**
 * Destroy the given {@link Group}.
 *
 * @param group {@link Group} to destroy.
 */
void group_destroy(struct Group* group);

/**
 * Load a {@link Group} from persistent storage.
 *
 * Should be used when the app is starting to load saved {@link Group Groups}.
 *
 * @return  A {@link Group} loaded from persistent storage.
 */
struct Group* group_load();

/**
 * Save the given {@link Group} to persistent storage.
 *
 * @param group {@link Group} to save to persistent storage.
 */
void group_save(const struct Group* group);

/**
 * Delete the given {@link Group} from persistent storage.
 *
 * @param group {@link Group} to delete from persistent storage.
 */
void group_persist_delete(const struct Group* group);

/**
 * Compare the id of the given {@link Group} to the group id.
 *
 * @param  group_id Id to compare to the {@link Group Group's} id.
 * @param  group    {@link Group} to compare to the given id.
 * @return          Zero if the ids are equal, positive if the given id
 *                  larger than the {@link Group Group's} id, negative if the
 *                  given id is smaller than the {@link Group Group's} id,
 */
int group_compare_id(int* group_id, const struct Group* group);

/**
 * Broadcast the {@link Group Group's} state to the {@link Model}.
 *
 * @param group {@link Group} to broadcast the state of.
 */
void group_broadcast_state(const struct Group* group);

/**
 * Get the id of the given {@link Group}.
 *
 * @param  group  {@link Group} to get the id of.
 * @return        The id of the given {@link Group}.
 */
int group_get_id(const struct Group* group);

/**
 * Get the {@link Settings} of the given {@link Group}.
 *
 * @param  group  {@link Group} to get the {@link Settings} of.
 * @return        The {@link Settings} of the given {@link Group}.
 */
struct Settings* group_get_settings(const struct Group* group);

/**
 * Add the {@link Timer} to the {@link Group}.
 *
 * @param group {@link Group} to add the {@link Timer} to.
 * @param timer {@link Timer} to add to the {@link Group}.
 */
void group_add_timer(struct Group* group, struct Timer* timer);

/**
 * Remove the {@link Timer} from the {@link Group}.
 *
 * @param group {@link Group} to remove the {@link Timer} from.
 * @param timer {@link Timer} to remove from the {@link Timer}.
 */
void group_remove_timer(struct Group* group, struct Timer* timer);

/**
 * Get the number of {@link Timer Timers} in the {@link Group}.
 *
 * @param  group  {@link Group} to get the size of.
 * @return        The number of {@link Timer Timers} in the {@link Group}.
 */
int group_size(const struct Group* group);

/**
 * Get the {@link Timer} at the given index in the {@link Group}.
 *
 * @param  group  {@link Group} to get the {@link Timer} from.
 * @param  index  The index of the {@link Timer} to get.
 * @return        The {@link Timer} at the given index in the {@link Group}, or
 *                NULL if the index is outside of [0, group size)
 */
struct Timer* group_get_timer(const struct Group* group, int index);

/**
 * Get the {@link Timer} with the given id.
 *
 * @param  group    {@link Group} to get the {@link Timer} from.
 * @param  timer_id Id of the {@link Timer} to get.
 * @return          The {@link Timer} with the given id, or NULL if no timer in
 *                  this {@link Group} has the given id.
 */
struct Timer* group_get_timer_by_id(const struct Group* group, int timer_id);

/**
 * Get the next timer (according to the progress and repeat settings) to start
 * after the given timer.
 *
 * @param  group    {@link Group} to get the next {@link Timer} from.
 * @param  timer_id The id of the current {@link Timer}. Used to calculate the
 *                  next {@link Timer}.
 * @return          The next timer or NULL if there is no next timer.
 */
struct Timer* group_get_next_timer(const struct Group* group, int timer_id);

/** @} // Group */

#endif /*GROUP_H*/
