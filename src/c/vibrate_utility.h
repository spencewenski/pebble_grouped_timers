#ifndef VIBRATE_UTILITY_H
#define VIBRATE_UTILITY_H

/**
 * @addtogroup vibrate_utility Vibrate utility
 * @{
 *
 * Utility methods used throughout the app for managing vibrations.
 */

#include "Settings_fields.h"

struct Group;
struct Timer;

/**
 * Get the number of seconds until the next time the {@link Timer} can vibrate,
 *
 * @param  group {@link Group} the {@link Timer} belongs to. Used to get the
 *               {@link Vibrate_interval_t} for the {@link Timer}.
 * @param  timer {@link Timer} to calculate the next vibrate for.
 * @return       The number of seconds until the next time the {@link Timer} can
 *               vibrate, or negative if the timer is not elapsed or can't
 *               vibrate.
 */
int seconds_until_next_vibrate(const struct Group* group,
  const struct Timer* timer);

/**
 * Calculate the number of times the {@link Timer} has vibrated since it
 * elapsed.
 *
 * @param  timer            {@link Timer} to get the vibrate count for.
 * @param  vibrate_interval {@link Vibrate_interval_t} for the
 *                          {@link Timer Timer's} {@link Group}.
 * @return                  The number of times the timer could have vibrated
 *                          since it elapsed, or 0 if it can't vibrate.
 */
int calculate_vibrate_count(const struct Timer* timer,
  Vibrate_interval_t vibrate_interval);

/**
 * Is the vibrate style one of the extended vibrations, such as
 * {@link VIBRATE_STYLE_EXTENDED_0}?
 *
 * @param  vibrate_style The {@link Vibrate_style_t} to check.
 * @return               Zero if the {@link Vibrate_style_t} is
 *                       an extended vibration, non-zero otherwise.
 */
int vibrate_style_extended(Vibrate_style_t vibrate_style);

/** @} // vibrate_utility */

#endif /*VIBRATE_UTILITY_H*/
