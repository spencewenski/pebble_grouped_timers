#ifndef SETTINGS_FIELDS_H
#define SETTINGS_FIELDS_H

/**
 * @addtogroup Settings_fields Settings fields
 * @{
 *
 * Enums to represent the settings fields and the options for each field.
 */

/**
 * The number of settings fields.
 */
#define NUM_SETTINGS_FIELDS 4

/**
 * The available settings fields.
 */
typedef enum {
  /**
   * Repeat styles.
   */
  SETTINGS_FIELD_REPEAT_STYLE,
  /**
   * Progress styles.
   */
  SETTINGS_FIELD_PROGRESS_STYLE,
  /**
   * Vibrate intervals.
   */
  SETTINGS_FIELD_VIBRATE_INTERVAL,
  /**
   * Vibrate styles.
   */
  SETTINGS_FIELD_VIBRATE_STYLE
} Settings_field_t;

/**
 * The available repeat styles.
 *
 * Values are assigned only to ensure consistency when saving and loading from
 * persistent storage.
 */
typedef enum {
  /**
   * Use the app settings.
   */
  REPEAT_STYLE_USE_APP_SETTINGS = 0,
  /**
   * Progress to the next timer, but don't repeat the group.
   */
  REPEAT_STYLE_NONE = 1,
  /**
   * Repeat the current timer.
   */
  REPEAT_STYLE_SINGLE = 2,
  /**
   * Progress to the next timer, repeating the group if at the last timer in the
   * group.
   */
  REPEAT_STYLE_GROUP = 3
} Repeat_style_t;

/**
 * The available progress styles.
 *
 * Values are assigned only to ensure consistency when saving and loading from
 * persistent storage.
 */
typedef enum {
  /**
   * Use the app settings.
   */
  PROGRESS_STYLE_USE_APP_SETTINGS = 0,
  /**
   * Wait for user to press the next button.
   */
  PROGRESS_STYLE_WAIT_FOR_USER = 1,
  /**
   * Auto-start next timer when the current one completes
   */
  PROGRESS_STYLE_AUTO = 2
} Progress_style_t;

/**
 * The available vibrate intervals.
 *
 * Values are assigned only to ensure consistency when saving and loading from
 * persistent storage.
 */
typedef enum {
  /**
   * Use the app settings.
   */
  VIBRATE_INTERVAL_USE_APP_SETTINGS = 0,
  /**
   * Vibrate every few seconds.
   */
  VIBRATE_INTERVAL_SHORT = 1,
  /**
   * Vibrate every minute.
   */
  VIBRATE_INTERVAL_MEDIUM = 2,
  /**
   * Vibrate every five minutes.
   */
  VIBRATE_INTERVAL_LONG = 3
} Vibrate_interval_t;

/**
 * The available vibrate styles.
 *
 * Values are assigned only to ensure consistency when saving and loading from
 * persistent storage.
 */
typedef enum {
  /**
   * Use the app settings.
   */
  VIBRATE_STYLE_USE_APP_SETTINGS = 0,
  /**
   * Single short pulse.
   */
  VIBRATE_STYLE_SHORT_PULSE = 1,
  /**
   * Single long pulse.
   */
  VIBRATE_STYLE_LONG_PULSE = 2,
  /**
   * Two short pulses.
   */
  VIBRATE_STYLE_DOUBLE_PULSE = 3,
  /**
   * Extended vibration, extra short.
   */
  VIBRATE_STYLE_EXTENDED_0 = 4,
  /**
   * Extended vibration, short.
   */
  VIBRATE_STYLE_EXTENDED_1 = 5,
  /**
   * Extended vibration, medium,
   */
  VIBRATE_STYLE_EXTENDED_2 = 6,
  /**
   * Extended vibration, long.
   */
  VIBRATE_STYLE_EXTENDED_3 = 7,
  /**
   * Extended vibration, extra long.
   */
  VIBRATE_STYLE_EXTENDED_4 = 8
} Vibrate_style_t;

/** @} // Settings_fields */

#endif /*SETTINGS_FIELDS_H*/
