#ifndef MAIN_VIEW_H
#define MAIN_VIEW_H

/**
 * @addtogroup Main_view Main view
 * @{
 *
 * The main view of the app.
 *
 * Lists all the {@link Group Groups} that have been created and the app
 * options.
 */

struct View;

/**
 * Create a {@link Main_view main view}.
 *
 * @return The abstract {@link View} handle for the {@link Main_view}.
 */
struct View* main_view_create();

/** @} // Main_view */

#endif /*MAIN_VIEW_H*/
