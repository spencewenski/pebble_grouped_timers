/**
 * @internal
 * @addtogroup Countdown_view
 * @{
 */

#include "Countdown_view.h"
#include "View.h"
#include "Utility.h"
#include "assert.h"
#include "Settings_fields.h"
#include "timer_utility.h"
#include "globals.h"
#include "draw_utility.h"
#include "Controller.h"
#include "Timer_state.h"
#include "View_interface.h"
#include "Settings_view_data.h"
#include "Vibrate_manager.h"

#include <pebble.h>

/**
 * The max height of the large countdown text.
 */
#define TIMER_TEXT_HEIGHT 45

/**
 * The max height of the small countdown text.
 */
#define TIMER_TEXT_HEIGHT_SM 22

/**
 * Number of pixels to leave on the sides of the countdown text.
 */
#define TIMER_TEXT_LEFT_RIGHT_SPACE 4

/**
 * @internal
 * @addtogroup Countdown_view_interface_impl
 * @{
 *
 * Implementations of the {@link View_interface} for the {@link Countdown_view}.
 */

/** {@link view_push_fp_t} */
static void countdown_view_push(void* data);
/** {@link view_destroy_fp_t} */
static void countdown_view_destroy(void* data);
/** {@link view_update_duration_fp_t} */
static int countdown_view_update_duration(void* data, int group_id,
  int timer_id, int duration);
/** {@link view_update_remaining_fp_t} */
static int countdown_view_update_remaining(void* data, int timer_id,
  int remaining);
/** {@link view_update_remove_timer_fp_t} */
static int countdown_view_update_remove_timer(void* data, int timer_id);
/** {@link view_update_timer_state_fp_t} */
static int countdown_view_update_timer_state(void* data, int timer_id,
  Timer_state_t timer_state);
/** {@link view_update_settings_fp_t} */
static int countdown_view_update_settings(void* data, int group_id,
  Repeat_style_t repeat_style, Progress_style_t progress_style,
  Vibrate_interval_t vibrate_interval, Vibrate_style_t vibrate_style);
/** {@link view_update_timer_tracking_fp_t} */
static int countdown_view_update_timer_tracking(void* data, int timer_id,
  int previous_timer_id);
/** {@link view_refresh_fp_t} */
static void countdown_view_refresh(void* data);

/** @} // Countdown_view_interface_impl */

/**
 * Data used by the {@link Countdown_view}.
 */
struct Countdown_view {
  /**
   * Pebble window object for this view.
   */
  Window* window;
  /**
   * Pebble text layer for the countdown text.
   */
  TextLayer* countdown_text_layer;
  /**
   * Pebble text layer for the {@link Timer Timer's} duration.
   */
  TextLayer* length_text_layer;
  /**
   * Pebble status bar layer to display the time at the top of the screen.
   */
  StatusBarLayer* status_bar_layer;
  /**
   * Pebble action bar layer to display the actions available for each button.
   */
  ActionBarLayer* action_bar_layer;
  /**
   * Pebble bitmap layer to display the {@link Repeat_style_t} icon.
   */
  BitmapLayer* repeat_style_bitmap_layer;
  /**
   * Pebble bitmap layer to display the {@link Progress_style_t} icon.
   */
  BitmapLayer* progress_style_bitmap_layer;
  /**
   * Pebble bitmap for the play icon.
   */
  GBitmap* play_icon;
  /**
   * Pebble bitmap for the pause icon.
   */
  GBitmap* pause_icon;
  /**
   * Pebble bitmap for the right arrow icon.
   */
  GBitmap* right_icon;
  /**
   * Pebble bitmap for the reset icon.
   */
  GBitmap* reset_icon;
  /**
   * Pebble bitmap for the ellipsis/more options icon.
   */
  GBitmap* ellipsis_icon;
  /**
   * Parent {@link View} object for this {@link Countdown_view}.
   */
  struct View* view;
  /**
   * Frame for the countdown text.
   */
  GRect timer_countdown_frame;
  /**
   * Frame for the {@link Timer Timer's} duration.
   */
  GRect timer_length_frame;
  /**
   * Id of the {@link Group} the {@link Timer} belongs to.
   */
  int group_id;
  /**
   * Id of the {@link Timer} the {@link Countdown_view} is currently displaying.
   */
  int timer_id;
  /**
   * Total duration of the {@link Timer} in seconds.
   */
  int duration;
  /**
   * Remaining time of the {@link Timer} in seconds.
   */
  int remaining;
  /**
   * Current state of the {@link Timer}.
   */
  Timer_state_t timer_state;
  /**
   * {@link Settings_view_data} for the default {@link Settings}
   */
  struct Settings_view_data* default_settings_data;
  /**
   * {@link Settings_view_data} for a group's {@link Settings}
   */
  struct Settings_view_data* group_settings_data;
  /**
   * {@link Settings_view_icons} for the {@link Countdown_view}.
   */
  struct Settings_view_icons* settings_view_icons;
  /**
   * Used to avoid updating the settings icons if the settings haven't changed.
   * Positive if the settings have changed since the last time the view was
   * refreshed, zero otherwise.
   */
  int settings_updated;
  /**
   * Buffer used to hold the {@link Timer Timer's} countdown text.
   */
  char countdown_text_buffer[TIMER_TEXT_BUFFER_LENGTH];
  /**
   * Buffer used to hold the {@link Timer Timer's} duration text.
   */
  char length_text_buffer[TIMER_TEXT_BUFFER_LENGTH];
};

/**
 * Update the settings icons layers.
 *
 * If the settings haven't been updated since the last time the view was
 * refreshed, do nothing.
 *
 * Note: This probably isn't really needed, because I'm pretty sure there's no
 * way for the {@link Settings_view} and the {@link Countdown_view} to be on the
 * window stack at the same time. However, I'm using it just in case this
 * changes or isn't true.
 *
 * @param countdown_view {@link Countdown_view} to update the settings icons
 *                       for.
 */
static void update_settings_icons(struct Countdown_view* countdown_view);

/**
 * Load the icon for the {@link Settings_field_t} and set it into the given
 * bitmap layer.
 *
 * @param countdown_view {@link Countdown_view} to load the icon for.
 * @param bitmap_layer   Bitmap layer to set the icon into.
 * @param settings_field The {@link Settings_field_t} to get the icon for.
 */
static void update_settings_field_icon(struct Countdown_view* countdown_view,
  BitmapLayer* bitmap_layer,
  Settings_field_t settings_field);

/**
 * Update the {@link Timer} countdown text layer with the latest {@link Timer}
 * time remaining.
 *
 * @param countdown_view {@link Countdown_view} to update the text layer of.
 */
static void update_timer_countdown_text_layer(
  struct Countdown_view* countdown_view);

/**
 * Update the {@link Timer} length text layer with the latest {@link Timer}
 * duration.
 *
 * @param countdown_view {@link Countdown_view} to update the text layer of.
 */
static void update_timer_length_text_layer(
  struct Countdown_view* countdown_view);

/**
 * Update the given text layer with the amount of time represented by the given
 * number of seconds.
 *
 * This is a helper method for {@link update_timer_countdown_text_layer} and
 * {@link update_timer_length_text_layer}.
 *
 * @param buffer              Char buffer to hold the time string.
 * @param buffer_length       Length of the char buffer.
 * @param text_layer          Text layer to update.
 * @param frame               Frame of the text layer.
 * @param seconds             Duration or remining time of the {@link Timer}
 *                            in seconds.
 * @param preferred_font_size {@link Preferred_font_size_t} for the text.
 */
static void update_timer_text_layer(char* buffer, int buffer_length,
  TextLayer* text_layer, GRect frame, int seconds,
  Preferred_font_size_t preferred_font_size);

/**
 * Set the icon for the select button.
 *
 * Uses the current {@link Timer_state} to determine which icon to use.
 *
 * @param countdown_view {@link Countdown_view} that contains the action bar
 *                       to update.
 */
static void action_bar_set_select_icon(struct Countdown_view* countdown_view);

/**
 * Create a Pebble text layer and setup with the common settings for this view.
 *
 * @param  frame GRect frame of the text layer.
 * @return       A new Pebble text layer setup with the common settings for this
 *               view.
 */
static TextLayer* create_and_setup_text_layer(GRect frame);

/**
 * Create a Pebble bitmap layer for the current settings and setup with the
 * common settings for this view.
 *
 * @param  countdown_view {@link Countdown_view} that contains the settings.
 * @param  frame          GRect frame of the bitmap layer.
 * @param  settings_field The {@link Settings_field_t} to create the layer for.
 * @return                A new Pebble bitmap layer for a settings icon.
 */
static BitmapLayer* create_and_setup_settings_bitmap_layer(
  struct Countdown_view* countdown_view, GRect frame);

// Pebble window handlers
static void window_load_handler(Window* window);
static void window_appear_handler(Window* window);
static void window_unload_handler(Window* window);

// Pebble click handlers
static void click_config_provider(void* context);
static void click_handler_up(ClickRecognizerRef recognizer, void* context);
static void click_handler_select(ClickRecognizerRef recognizer, void* context);
static void click_handler_down(ClickRecognizerRef recognizer, void* context);

/**
 * Cancel vibes if the group's vibrate style is one of the extended vibrate
 * styles.
 *
 * @param countdown_view The {@link Countdown_view} to cancel long vibes for.
 */
static void cancel_long_vibes(const struct Countdown_view* countdown_view);

struct View* countdown_view_create(int group_id, int timer_id)
{
  struct Countdown_view* countdown_view =
    safe_alloc(sizeof(struct Countdown_view));
  countdown_view->group_id = group_id;
  countdown_view->timer_id = timer_id;
  countdown_view->duration = 0;
  countdown_view->remaining = 0;
  countdown_view->timer_state = TIMER_STATE_NOT_STARTED;
  countdown_view->countdown_text_buffer[0] = '\0';
  countdown_view->length_text_buffer[0] = '\0';
  countdown_view->window = NULL;
  countdown_view->countdown_text_layer = NULL;
  countdown_view->length_text_layer = NULL;
  countdown_view->status_bar_layer = NULL;
  countdown_view->action_bar_layer = NULL;
  countdown_view->repeat_style_bitmap_layer = NULL;
  countdown_view->progress_style_bitmap_layer = NULL;
  countdown_view->play_icon = NULL;
  countdown_view->pause_icon = NULL;
  countdown_view->right_icon = NULL;
  countdown_view->reset_icon = NULL;
  countdown_view->ellipsis_icon = NULL;
  countdown_view->default_settings_data = settings_view_data_create();
  countdown_view->group_settings_data = settings_view_data_create();
  countdown_view->settings_view_icons = settings_view_icons_create();
  countdown_view->settings_updated = 0;
  struct View_interface view_interface = (struct View_interface) {
    .push = countdown_view_push,
    .update_duration = countdown_view_update_duration,
    .update_remaining = countdown_view_update_remaining,
    .update_remove_timer = countdown_view_update_remove_timer,
    .update_timer_state = countdown_view_update_timer_state,
    .update_group_settings = countdown_view_update_settings,
    .update_timer_tracking = countdown_view_update_timer_tracking,
    .refresh = countdown_view_refresh,
    .destroy = countdown_view_destroy
  };
  countdown_view->view = view_create(countdown_view, &view_interface);
  return countdown_view->view;
}

static void countdown_view_destroy(void* data)
{
  assert(data);
  struct Countdown_view* countdown_view = data;
  settings_view_data_destroy(countdown_view->default_settings_data);
  countdown_view->default_settings_data = NULL;
  settings_view_data_destroy(countdown_view->group_settings_data);
  countdown_view->group_settings_data = NULL;
  settings_view_icons_destroy(countdown_view->settings_view_icons);
  countdown_view->settings_view_icons = NULL;
  free(countdown_view);
}

void countdown_view_push(void* data)
{
  assert(data);
  struct Countdown_view* countdown_view = (struct Countdown_view*) data;

  countdown_view->window = window_create();

  assert(countdown_view->window);

  window_set_user_data(countdown_view->window, countdown_view);
  window_set_window_handlers(countdown_view->window, (WindowHandlers) {
    .load = window_load_handler,
    .appear = window_appear_handler,
    .unload = window_unload_handler
  });
  window_stack_push(countdown_view->window, false);
}

static int countdown_view_update_duration(void* data, int group_id,
  int timer_id, int duration)
{
  assert(data);
  struct Countdown_view* countdown_view = (struct Countdown_view*) data;

  if (timer_id != countdown_view->timer_id) {
    return 0;
  }
  // If the duration is being updated, then the remaining time should also be
  // updated
  countdown_view->duration = duration;
  countdown_view->remaining = duration;
  return 1;
}

static int countdown_view_update_remaining(void* data, int timer_id,
  int remaining)
{
  assert(data);
  struct Countdown_view* countdown_view = (struct Countdown_view*) data;

  if (timer_id != countdown_view->timer_id) {
    return 0;
  }
  countdown_view->remaining = remaining;
  return 1;
}

static int countdown_view_update_remove_timer(void* data, int timer_id)
{
  assert(data);
  struct Countdown_view* countdown_view = (struct Countdown_view*) data;

  if (timer_id != countdown_view->timer_id) {
    return 0;
  }
  // The timer this view is tracking was deleted, so pop this view
  window_stack_remove(countdown_view->window, false);
  return 0;
}

static int countdown_view_update_timer_state(void* data, int timer_id,
  Timer_state_t timer_state)
{
  assert(data);
  struct Countdown_view* countdown_view = (struct Countdown_view*) data;

  if (timer_id != countdown_view->timer_id) {
    return 0;
  }
  countdown_view->timer_state = timer_state;
  return 1;
}

static int countdown_view_update_settings(void* data, int group_id,
  Repeat_style_t repeat_style, Progress_style_t progress_style,
  Vibrate_interval_t vibrate_interval, Vibrate_style_t vibrate_style)
{
  // TODO: this code is nearly identical to
  // {@link settings_view_update_settings}, reduce this code duplication.
  assert(data);
  struct Countdown_view* countdown_view = (struct Countdown_view*) data;

  countdown_view->settings_updated++;

  int modified = 0;
  if (group_id < 0) {
    ++modified;
    settings_view_data_set_values(countdown_view->default_settings_data,
      repeat_style, progress_style, vibrate_interval, vibrate_style);
  }

  if (group_id != countdown_view->group_id) {
    return modified;
  }
  settings_view_data_set_values(countdown_view->group_settings_data,
    repeat_style, progress_style, vibrate_interval, vibrate_style);
  return ++modified;
}

static int countdown_view_update_timer_tracking(void* data, int timer_id,
  int previous_timer_id)
{
  assert(data);
  struct Countdown_view* countdown_view = (struct Countdown_view*) data;

  if (previous_timer_id >= 0 && previous_timer_id != countdown_view->timer_id) {
    return 0;
  }
  countdown_view->timer_id = timer_id;
  return 1;
}

static void countdown_view_refresh(void* data)
{
  assert(data);
  struct Countdown_view* countdown_view = (struct Countdown_view*) data;

  update_settings_icons(countdown_view);
  update_timer_countdown_text_layer(countdown_view);
  update_timer_length_text_layer(countdown_view);
  action_bar_set_select_icon(countdown_view);
}

static void window_load_handler(Window* window)
{
  struct Countdown_view* countdown_view = window_get_user_data(window);
  assert(countdown_view);

  Layer* window_layer = window_get_root_layer(window);

  // Setup the status bar
  countdown_view->status_bar_layer = status_bar_layer_create();
  layer_add_child(window_layer,
    status_bar_layer_get_layer(countdown_view->status_bar_layer));

  // Load action bar icons
  countdown_view->play_icon =
    gbitmap_create_with_resource(RESOURCE_ID_ACTION_BAR_ICON_PLAY);
  countdown_view->pause_icon =
    gbitmap_create_with_resource(RESOURCE_ID_ACTION_BAR_ICON_PAUSE);
  countdown_view->right_icon =
    gbitmap_create_with_resource(RESOURCE_ID_ACTION_BAR_ICON_RIGHT);
  countdown_view->reset_icon =
    gbitmap_create_with_resource(RESOURCE_ID_ACTION_BAR_ICON_RESET);
  countdown_view->ellipsis_icon =
    gbitmap_create_with_resource(RESOURCE_ID_MUSIC_ICON_ELLIPSIS);

  // Setup the action bar
  countdown_view->action_bar_layer = action_bar_layer_create();
  action_bar_layer_add_to_window(countdown_view->action_bar_layer, window);
  action_bar_layer_set_context(countdown_view->action_bar_layer,
    countdown_view);
  action_bar_layer_set_click_config_provider(countdown_view->action_bar_layer,
    click_config_provider);
  action_bar_layer_set_icon(countdown_view->action_bar_layer, BUTTON_ID_UP,
    countdown_view->reset_icon);
  action_bar_layer_set_icon(countdown_view->action_bar_layer, BUTTON_ID_DOWN,
    countdown_view->ellipsis_icon);
  action_bar_layer_set_icon_press_animation(countdown_view->action_bar_layer,
    BUTTON_ID_DOWN, ActionBarLayerIconPressAnimationNone);

  // Setup the countdown layer
  GRect window_bounds = layer_get_bounds(window_layer);
  window_bounds = action_bar_adjust_window_bounds(window_bounds);
  countdown_view->timer_countdown_frame = window_bounds;
  countdown_view->timer_countdown_frame.size.h = TIMER_TEXT_HEIGHT;
  grect_align(&countdown_view->timer_countdown_frame, &window_bounds,
    GAlignCenter, true);
  countdown_view->countdown_text_layer =
    create_and_setup_text_layer(countdown_view->timer_countdown_frame);
  layer_add_child(window_layer,
    text_layer_get_layer(countdown_view->countdown_text_layer));
  update_timer_countdown_text_layer(countdown_view);

  // Setup the timer length layer
  countdown_view->timer_length_frame = countdown_view->timer_countdown_frame;
  countdown_view->timer_length_frame.size.h = TIMER_TEXT_HEIGHT_SM;
  countdown_view->timer_length_frame.origin.y =
    countdown_view->timer_countdown_frame.origin.y +
    countdown_view->timer_countdown_frame.size.h;
  countdown_view->length_text_layer =
    create_and_setup_text_layer(countdown_view->timer_length_frame);
  layer_add_child(window_layer,
    text_layer_get_layer(countdown_view->length_text_layer));
  update_timer_length_text_layer(countdown_view);

  // Setup settings bitmap layers
  window_bounds = layer_get_bounds(window_layer);
  window_bounds = action_bar_adjust_window_bounds(window_bounds);
  window_bounds = status_bar_adjust_window_bounds(window_bounds);
  GRect settings_icons_bounds = window_bounds;
  settings_icons_bounds.size.h =
    countdown_view->timer_countdown_frame.origin.y - window_bounds.origin.y;
  GRect repeat_style_frame = settings_icons_bounds;
  GRect progress_style_frame = settings_icons_bounds;
  // Setup the frames for the settings
  // TODO: clean up this section of code; it's a bit hacky inserting this
  // macro check in the middle of the function
#ifndef PBL_ROUND
  // Non-round settings frames
  int width = settings_icons_bounds.size.w / 2;
  repeat_style_frame.size.w = width;
  progress_style_frame.size.w = width;
  progress_style_frame.origin.x += width;
#else
  // Round settings frames
  int width = settings_icons_bounds.size.w / 3;
  repeat_style_frame.size.w = width;
  repeat_style_frame.origin.x += width / 2;
  progress_style_frame.size.w = width;
  progress_style_frame.origin.x =
    (settings_icons_bounds.origin.x + settings_icons_bounds.size.w) -
    (1.5 * width);
#endif
  // Create and setup the settings bitmap layers
  countdown_view->repeat_style_bitmap_layer =
    create_and_setup_settings_bitmap_layer(countdown_view, repeat_style_frame);
  countdown_view->progress_style_bitmap_layer =
    create_and_setup_settings_bitmap_layer(countdown_view,
      progress_style_frame);
  layer_add_child(window_layer,
    bitmap_layer_get_layer(countdown_view->repeat_style_bitmap_layer));
  layer_add_child(window_layer,
    bitmap_layer_get_layer(countdown_view->progress_style_bitmap_layer));
  countdown_view->settings_updated = max(1, countdown_view->settings_updated);
  update_settings_icons(countdown_view);
}

static TextLayer* create_and_setup_text_layer(GRect frame)
{
  TextLayer* text_layer = text_layer_create(frame);
  assert(text_layer);
  text_layer_set_text_color(text_layer, GColorBlack);
  text_layer_set_background_color(text_layer, GColorWhite);
  text_layer_set_text_alignment(text_layer, GTextAlignmentCenter);
  return text_layer;
}

static BitmapLayer* create_and_setup_settings_bitmap_layer(
  struct Countdown_view* countdown_view, GRect frame)
{
  BitmapLayer* bitmap_layer = bitmap_layer_create(frame);
  bitmap_layer_set_alignment(bitmap_layer,
    PBL_IF_ROUND_ELSE(GAlignRight, GAlignCenter));
  bitmap_layer_set_compositing_mode(bitmap_layer, GCompOpSet);
  return bitmap_layer;
}

static void window_appear_handler(Window* window)
{
  struct Countdown_view* countdown_view = window_get_user_data(window);
  assert(countdown_view);

  update_timer_countdown_text_layer(countdown_view);
  update_timer_length_text_layer(countdown_view);
}

static void window_unload_handler(Window* window)
{
  struct Countdown_view* countdown_view = window_get_user_data(window);
  assert(countdown_view);

  cancel_long_vibes(countdown_view);

  settings_view_icons_unload(countdown_view->settings_view_icons);
  bitmap_layer_destroy(countdown_view->repeat_style_bitmap_layer);
  countdown_view->repeat_style_bitmap_layer = NULL;
  bitmap_layer_destroy(countdown_view->progress_style_bitmap_layer);
  countdown_view->progress_style_bitmap_layer = NULL;

  gbitmap_destroy(countdown_view->play_icon);
  countdown_view->play_icon = NULL;
  gbitmap_destroy(countdown_view->pause_icon);
  countdown_view->pause_icon = NULL;
  gbitmap_destroy(countdown_view->right_icon);
  countdown_view->right_icon = NULL;
  gbitmap_destroy(countdown_view->reset_icon);
  countdown_view->reset_icon = NULL;
  gbitmap_destroy(countdown_view->ellipsis_icon);
  countdown_view->ellipsis_icon = NULL;

  text_layer_destroy(countdown_view->length_text_layer);
  countdown_view->length_text_layer = NULL;

  text_layer_destroy(countdown_view->countdown_text_layer);
  countdown_view->countdown_text_layer = NULL;

  status_bar_layer_destroy(countdown_view->status_bar_layer);
  countdown_view->status_bar_layer = NULL;

  action_bar_layer_remove_from_window(countdown_view->action_bar_layer);
  action_bar_layer_destroy(countdown_view->action_bar_layer);
  countdown_view->action_bar_layer = NULL;

  window_destroy(countdown_view->window);
  countdown_view->window = NULL;

  controller_on_view_closed(countdown_view->view);
}

static void click_config_provider(void* context)
{
  window_single_click_subscribe(BUTTON_ID_UP, click_handler_up);
  window_single_click_subscribe(BUTTON_ID_SELECT, click_handler_select);
  window_single_click_subscribe(BUTTON_ID_DOWN, click_handler_down);
}

static void click_handler_up(ClickRecognizerRef recognizer, void* context)
{
  assert(context);
  struct Countdown_view* countdown_view = (struct Countdown_view*) context;

  cancel_long_vibes(countdown_view);

  controller_reset_timer(countdown_view->timer_id);
}

static void click_handler_select(ClickRecognizerRef recognizer, void* context)
{
  assert(context);
  struct Countdown_view* countdown_view = (struct Countdown_view*) context;

  cancel_long_vibes(countdown_view);

  switch (countdown_view->timer_state) {
    case TIMER_STATE_NOT_STARTED: // Intentional fall through
    case TIMER_STATE_PAUSED:
      controller_start_timer(countdown_view->timer_id);
      return;
    case TIMER_STATE_STARTED:
      controller_pause_timer(countdown_view->timer_id);
      return;
    case TIMER_STATE_ELAPSED:
      controller_next_timer(countdown_view->group_id, countdown_view->timer_id);
      return;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid timer state.");
      return;
 }
}

static void click_handler_down(ClickRecognizerRef recognizer, void* context)
{
  assert(context);
  struct Countdown_view* countdown_view = (struct Countdown_view*) context;

  cancel_long_vibes(countdown_view);

  controller_open_timer_options_view(countdown_view->timer_id);
}

static void cancel_long_vibes(const struct Countdown_view* countdown_view)
{
  assert(countdown_view);
  Vibrate_style_t vibrate_style =
    settings_view_data_get_group_vibrate_style(countdown_view->group_id,
      countdown_view->default_settings_data,
      countdown_view->group_settings_data);
  vibrate_manager_cancel_long_vibes(vibrate_style);
}

static void update_settings_icons(struct Countdown_view* countdown_view)
{
  assert(countdown_view);
  if (!countdown_view->settings_updated) {
    return;
  }
  countdown_view->settings_updated = 0;
  update_settings_field_icon(countdown_view,
    countdown_view->repeat_style_bitmap_layer, SETTINGS_FIELD_REPEAT_STYLE);
  update_settings_field_icon(countdown_view,
    countdown_view->progress_style_bitmap_layer, SETTINGS_FIELD_PROGRESS_STYLE);
}

static void update_settings_field_icon(struct Countdown_view* countdown_view,
  BitmapLayer* bitmap_layer,
  Settings_field_t settings_field)
{
  GBitmap* bitmap = settings_view_icons_get_for_field(
      countdown_view->settings_view_icons, countdown_view->group_id,
      countdown_view->default_settings_data,
      countdown_view->group_settings_data, settings_field);
  set_palette(bitmap, 0);
  bitmap_layer_set_bitmap(bitmap_layer, bitmap);
}

static void update_timer_countdown_text_layer(
  struct Countdown_view* countdown_view)
{
  assert(countdown_view);
  update_timer_text_layer(countdown_view->countdown_text_buffer,
    TIMER_TEXT_BUFFER_LENGTH, countdown_view->countdown_text_layer,
    countdown_view->timer_countdown_frame,
    countdown_view->remaining, PREFERRED_FONT_SIZE_5);
}

static void update_timer_length_text_layer(
  struct Countdown_view* countdown_view)
{
  assert(countdown_view);
  update_timer_text_layer(countdown_view->length_text_buffer,
    TIMER_TEXT_BUFFER_LENGTH, countdown_view->length_text_layer,
    countdown_view->timer_length_frame,
    countdown_view->duration, PREFERRED_FONT_SIZE_0);
}

static void update_timer_text_layer(char* buffer, int buffer_length,
  TextLayer* text_layer, GRect frame, int seconds,
  Preferred_font_size_t preferred_font_size)
{
  assert(buffer);
  assert(text_layer);
  get_timer_text_duration(buffer, buffer_length, seconds);
  GFont font = get_leco_font(preferred_font_size,
    buffer, frame.size.w - TIMER_TEXT_LEFT_RIGHT_SPACE, frame.size.h);
  text_layer_set_font(text_layer, font);
  layer_set_bounds(text_layer_get_layer(text_layer),
    vertical_center_text(buffer, font, frame.size.w, frame.size.h));
  text_layer_set_text(text_layer, buffer);
  layer_mark_dirty(text_layer_get_layer(text_layer));
}

static void action_bar_set_select_icon(struct Countdown_view* countdown_view)
{
  assert(countdown_view);
  switch (countdown_view->timer_state) {
    case TIMER_STATE_NOT_STARTED: // Intentional fall through
    case TIMER_STATE_PAUSED:
      action_bar_layer_set_icon(countdown_view->action_bar_layer,
        BUTTON_ID_SELECT, countdown_view->play_icon);
      return;
    case TIMER_STATE_STARTED:
      action_bar_layer_set_icon(countdown_view->action_bar_layer,
        BUTTON_ID_SELECT, countdown_view->pause_icon);
      return;
    case TIMER_STATE_ELAPSED:
      action_bar_layer_set_icon(countdown_view->action_bar_layer,
        BUTTON_ID_SELECT, countdown_view->right_icon);
      return;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid timer state");
      return;
  }
}

/** @} // Countdown_view */
