/**
 * @internal
 * @addtogroup Group_view
 * @{
 */

#include "Group_view.h"
#include "List.h"
#include "Utility.h"
#include "View.h"
#include "assert.h"
#include "Controller.h"
#include "draw_utility.h"
#include "globals.h"
#include "View_interface.h"
#include "locale_framework/src/localize.h"

#include <pebble.h>

/**
 * Number of menu sections in the {@link Group_view}.
 */
#define MENU_NUM_SECTIONS 2

/**
 * Number of rows in the settings section.
 */
#define SETTINGS_NUM_ROWS 3

/**
 * @internal
 * @addtogroup Group_view_interface_impl
 * @{
 *
 * Implementations of the {@link View_interface} for the {@link Group_view}.
 */

/** {@link view_push_fp_t} */
static void group_view_push(void* data);
/** {@link view_destroy_fp_t} */
static void group_view_destroy(void* data);
/** {@link view_update_duration_fp_t} */
static int group_view_update_duration(void* data, int group_id, int timer_id,
  int duration);
/** {@link view_update_remaining_fp_t} */
static int group_view_update_remaining(void* data, int timer_id, int remaining);
/** {@link view_update_remove_timer_fp_t} */
static int group_view_update_remove_timer(void* data, int timer_id);
/** {@link view_refresh_fp_t} */
static void group_view_refresh(void* data);

/** @} // Group_view_interface_impl */

/**
 * Data required for the {@link Group_view} to display a {@link Timer}.
 */
struct Timer_data {
  /**
   * Id of the {@link Timer}.
   */
  int timer_id;
  /**
   * Duration of the {@link Timer} in seconds.
   */
  int duration;
  /**
   * Amount of time remaining for the {@link Timer} in seconds.
   */
  int remaining;
};

/**
 * Create a {@link Timer_data} object.
 *
 * @param  timer_id Id of the {@link Timer}.
 * @return          A new {@link Timer_data} object.
 */
static struct Timer_data* timer_data_create(int timer_id);

/**
 * Destroy the {@link Timer_data} object.
 *
 * @param timer_data {@link Timer_data} object to destroy.
 */
static void timer_data_destroy(struct Timer_data* timer_data);

/**
 * Compare the given {@link Timer} id to the one in the given
 * {@link Timer_data}.
 *
 * @param  timer_id_ptr     {@link Timer} id to comapre to the id in the
 *                          {@link Timer_data}.
 * @param  timer_data_other {@link Timer_data} that contains the id to compare
 *                          to the given {@link Timer} id.
 * @return                  Zero if the ids are equal, positive if the given
 *                          {@link Timer} id is greater than the id in the
 *                          {@link Timer_data}, negative otherwise.
 */
static int timer_data_compare_id(const int* timer_id_ptr,
  const struct Timer_data* timer_data_other);

/**
 * Data used by the {@link Group_view}.
 */
struct Group_view {
  /**
   * Pebble window for this view.
   */
  Window* window;
  /**
   * Pebble menu layer to display the {@link Group Group's} data.
   */
  MenuLayer* menu_layer;
  /**
   * Pebble status bar layer to display the time at the top of the screen.
   */
  StatusBarLayer* status_bar_layer;
  /**
   * Parent {@link View} object for this {@link Countdown_view}.
   */
  struct View* view;
  /**
   * {@link Timer_data} for the {@link Timer Timers} in the {@link Group}.
   */
  struct List* timer_data;
  /**
   * Id of the {@link Group} the {@link Group_view} is displaying.
   */
  int group_id;
};

/**
 * Draw a menu row for a {@link Timer}.
 *
 * @param ctx        Pebble graphics context for the row.
 * @param cell_layer Pebble cell layer for the row.
 * @param row_index  Index of the row.
 * @param data       {@link Group_view} object used to draw the row.
 */
static void group_view_draw_row(GContext* ctx, const Layer* cell_layer,
  uint16_t row_index, void* data);

// WindowHandlers
static void window_load_handler(Window* window);
static void window_appear_handler(Window* window);
static void window_unload_handler(Window* window);

// MenuLayerCallbacks
static uint16_t menu_get_num_sections_callback(MenuLayer* menu_layer,
  void* data);
static uint16_t menu_get_num_rows_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data);
static void menu_draw_row_callback(GContext* ctx, const Layer* cell_layer,
  MenuIndex* cell_index, void* data);
static void menu_draw_header_callback(GContext* ctx, const Layer* cell_layer,
  uint16_t section_index, void* data);
static void menu_select_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data);
static void menu_select_long_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data);

struct View* group_view_create(int group_id)
{
  struct Group_view* group_view = safe_alloc(sizeof(struct Group_view));
  group_view->group_id = group_id;
  group_view->timer_data = list_create();
  struct View_interface view_interface = (struct View_interface) {
    .push = group_view_push,
    .destroy = group_view_destroy,
    .update_duration = group_view_update_duration,
    .update_remaining = group_view_update_remaining,
    .update_remove_timer = group_view_update_remove_timer,
    .refresh = group_view_refresh
  };
  group_view->view = view_create(group_view, &view_interface);
  return group_view->view;
}

static void group_view_destroy(void* data)
{
  assert(data);
  struct Group_view* group_view = (struct Group_view*) data;
  list_for_each(group_view->timer_data, (List_for_each_fp_t)timer_data_destroy);
  list_destroy(group_view->timer_data);
  free(data);
}

static int group_view_update_duration(void* data, int group_id, int timer_id,
  int duration)
{
  assert(data);
  assert(group_id >= 0);
  assert(timer_id >= 0);
  struct Group_view* group_view = (struct Group_view*) data;
  if (group_id != group_view->group_id) {
    return 0;
  }
  struct Timer_data* timer_data = list_find_arg(group_view->timer_data,
    &timer_id, (List_compare_arg_fp_t)timer_data_compare_id);
  // If the view doesn't have this timer yet, create the timer
  if (!timer_data) {
    timer_data = timer_data_create(timer_id);
    list_add(group_view->timer_data, timer_data);
  }
  timer_data->duration = duration;
  // If the duration is being updated, the remaining time should also be updated
  timer_data->remaining = duration;
  return 1;
}

static int group_view_update_remaining(void* data, int timer_id, int remaining)
{
  assert(data);
  assert(timer_id >= 0);
  struct Group_view* group_view = (struct Group_view*) data;
  struct Timer_data* timer_data = list_find_arg(group_view->timer_data,
    &timer_id, (List_compare_arg_fp_t)timer_data_compare_id);
  // If the timer isn't in the group this view is tracking, do nothing
  if (!timer_data) {
    return 0;
  }
  timer_data->remaining = remaining;
  return 1;
}

static int group_view_update_remove_timer(void* data, int timer_id)
{
  assert(data);
  assert(timer_id >= 0);
  struct Group_view* group_view = (struct Group_view*) data;
  struct Timer_data* timer_data = list_find_arg(group_view->timer_data,
    &timer_id, (List_compare_arg_fp_t)timer_data_compare_id);
  // If the timer isn't in the group this view is tracking, do nothing
  if (!timer_data) {
    return 0;
  }
  list_remove_ptr(group_view->timer_data, timer_data);
  timer_data_destroy(timer_data);
  return 1;
}

static void group_view_refresh(void* data)
{
  assert(data);
  struct Group_view* group_view = (struct Group_view*) data;
  assert(group_view->menu_layer);
  menu_layer_reload_data(group_view->menu_layer);
}

static void group_view_push(void* data)
{
  assert(data);
  struct Group_view* group_view = (struct Group_view*) data;

  group_view->window = window_create();

  assert(group_view->window);

  window_set_user_data(group_view->window, group_view);
  window_set_window_handlers(group_view->window, (WindowHandlers) {
    .load = window_load_handler,
    .appear = window_appear_handler,
    .unload = window_unload_handler
  });
  window_stack_push(group_view->window, false);
}

static struct Timer_data* timer_data_create(int timer_id)
{
  struct Timer_data* timer_data = safe_alloc(sizeof(struct Timer_data));
  timer_data->timer_id = timer_id;
  timer_data->duration = 0;
  timer_data->remaining = 0;
  return timer_data;
}

static void timer_data_destroy(struct Timer_data* timer_data)
{
  free(timer_data);
}

static int timer_data_compare_id(const int* timer_id_ptr,
 const struct Timer_data* timer_data_other)
{
  assert(timer_id_ptr);
  assert(timer_data_other);
  return *timer_id_ptr - timer_data_other->timer_id;
}

static void window_load_handler(Window* window)
{
  struct Group_view* group_view = window_get_user_data(window);
  assert(group_view);

  Layer* window_layer = window_get_root_layer(window);

  // Status bar layer
  group_view->status_bar_layer = status_bar_layer_create();
  layer_add_child(window_layer,
    status_bar_layer_get_layer(group_view->status_bar_layer));

  // Setup the menu layer
  GRect bounds = layer_get_bounds(window_layer);
  bounds = status_bar_adjust_window_bounds(bounds);
  group_view->menu_layer = menu_layer_create(bounds);
  assert(group_view->menu_layer);
  menu_layer_set_callbacks(group_view->menu_layer, group_view,
    (MenuLayerCallbacks) {
      .get_num_sections = menu_get_num_sections_callback,
      .get_num_rows = menu_get_num_rows_callback,
      .get_cell_height = PBL_IF_ROUND_ELSE(menu_cell_get_height_round, NULL),
      .get_header_height = menu_get_header_height_callback,
      .draw_header = menu_draw_header_callback,
      .draw_row = menu_draw_row_callback,
      .select_click = menu_select_click_callback,
      .select_long_click = menu_select_long_click_callback
    }
  );
  menu_layer_set_click_config_onto_window(group_view->menu_layer, window);
  layer_add_child(window_layer, menu_layer_get_layer(group_view->menu_layer));
}

static void window_appear_handler(Window* window)
{
  struct Group_view* group_view = window_get_user_data(window);
  assert(group_view);

  menu_layer_reload_data(group_view->menu_layer);
}

static void window_unload_handler(Window* window)
{
  struct Group_view* group_view = window_get_user_data(window);
  assert(group_view);

  menu_layer_destroy(group_view->menu_layer);
  group_view->menu_layer = NULL;

  status_bar_layer_destroy(group_view->status_bar_layer);
  group_view->status_bar_layer = NULL;

  window_destroy(group_view->window);
  group_view->window = NULL;

  controller_on_view_closed(group_view->view);
}

static uint16_t menu_get_num_sections_callback(MenuLayer* menu_layer,
 void* data)
{
  return MENU_NUM_SECTIONS;
}

static uint16_t menu_get_num_rows_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data)
{
  assert(data);
  struct Group_view* group_view = (struct Group_view*) data;

  switch (section_index) {
    case 0:
      // Timers
      return list_size(group_view->timer_data);
    case 1:
      // Group options
      return SETTINGS_NUM_ROWS;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid section index: %d", section_index);
      return 0;
  }
  return 0;
}

static void menu_draw_row_callback(GContext* ctx, const Layer* cell_layer,
  MenuIndex* cell_index, void* data)
{
  switch (cell_index->section) {
    case 0:
      group_view_draw_row(ctx, cell_layer, cell_index->row, data);
      return;
    case 1:
      // Group options
      if (cell_index->row == 0) {
        // New timer
        menu_cell_basic_draw(ctx, cell_layer, LOCALIZE("New timer"), NULL, NULL);
        return;
      }
      if (cell_index->row == 1) {
        // Settings
        menu_cell_basic_draw(ctx, cell_layer, LOCALIZE("Settings"), NULL, NULL);
        return;
      }
      if (cell_index->row == 2) {
        // Delete group
        menu_cell_basic_draw(ctx, cell_layer, LOCALIZE("Delete group"), NULL, NULL);
        return;
      }
      return;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid section index: %d",
        cell_index->section);
      return;
  }
}

static void group_view_draw_row(GContext* ctx, const Layer* cell_layer,
  uint16_t row_index, void* data)
{
  assert(ctx);
  assert(cell_layer);
  assert(data);

  struct Group_view* group_view = (struct Group_view*) data;

  struct Timer_data* timer_data = list_get(group_view->timer_data, row_index);
  assert(timer_data);

  // Get the main timer text. If the timer is not running, this is the timer
  // duration. If the timer is running, this is amount of time remaining for
  // the timer.
  char menu_text[MENU_TEXT_BUFFER_LENGTH];
  int menu_text_duration =  (timer_data->remaining != timer_data->duration) ?
    timer_data->remaining : timer_data->duration;
  get_timer_text_duration(menu_text, sizeof(menu_text), menu_text_duration);

  // Get the subtitle text. If the timer is not running, this is NULL. If
  // the time is running, this is the timer duration.
  char subtitle_text[MENU_TEXT_BUFFER_LENGTH];
  char* subtitle_text_ptr = NULL;
  if (timer_data->remaining != timer_data->duration) {
    get_timer_text_duration(subtitle_text, sizeof(subtitle_text),
      timer_data->duration);
    subtitle_text_ptr = subtitle_text;
  }
  menu_cell_basic_draw(ctx, cell_layer, menu_text, subtitle_text_ptr, NULL);
}

static void menu_draw_header_callback(GContext* ctx, const Layer* cell_layer,
  uint16_t section_index, void* data)
{
  switch (section_index) {
    case 0:
      // Timers
      menu_cell_draw_header(ctx, cell_layer, LOCALIZE("Timers"));
      return;
    case 1:
      // Group options
      menu_cell_draw_header(ctx, cell_layer, LOCALIZE("Group options"));
      return;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid section index: %d", section_index);
      return;
  }
}

static void menu_select_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data)
{
  assert(data);

  struct Group_view* group_view = (struct Group_view*) data;

  switch (cell_index->section) {
    case 0:
      // Open countdown view for the timer
      assert(in_range(cell_index->row, 0, list_size(group_view->timer_data)));
      struct Timer_data* timer_data =
        list_get(group_view->timer_data, cell_index->row);
      controller_open_countdown_view(group_view->group_id,
        timer_data->timer_id);
      break;
    case 1:
      if (cell_index->row == 0) {
        // Create timer and open edit window
        controller_open_edit_view(
          controller_create_timer(group_view->group_id));
      } else if (cell_index->row == 1) {
        // Group settings
        controller_open_settings_view(group_view->group_id);
      } else if (cell_index->row == 2) {
        // Delete group
        controller_delete_group(group_view->group_id);
        window_stack_remove(group_view->window, false);
        return;
      }
      break;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid section index: %d",
        cell_index->section);
      return;
  }
}

static void menu_select_long_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data)
{
  assert(data);

  struct Group_view* group_view = (struct Group_view*) data;

  switch (cell_index->section) {
    case 0:
      // Start the timer
      assert(in_range(cell_index->row, 0, list_size(group_view->timer_data)));
      struct Timer_data* timer_data =
        list_get(group_view->timer_data, cell_index->row);
      controller_start_timer(timer_data->timer_id);
      controller_open_countdown_view(group_view->group_id,
        timer_data->timer_id);
      break;
    default:
      APP_LOG(APP_LOG_LEVEL_DEBUG, "Only support long click on timers");
      return;
  }
}

/** @} // Group_view */
