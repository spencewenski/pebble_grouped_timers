/**
 * @internal
 * @addtogroup Advanced_settings
 * @{
 */

#include "Advanced_settings.h"
#include "persist_utility.h"
#include "Utility.h"
#include "assert.h"
#include "Model.h"

#include <pebble.h>

/**
 * The default {@link Extended_vibes_state_t}.
 */
#define DEFAULT_EXTENDED_VIBES_STATE EXTENDED_VIBES_DISABLED

/**
 * The number of {@link Advanced_settings} persist versions.
 */
#define NUM_PERSIST_VERSIONS 1

/**
 * The modified state of the persist version. Until the persist version is
 * loaded, assume it needs to be saved.
 */
static Modified_state_t s_persist_version_modified_state = MODIFIED_STATE_SINGLE;

/**
 * Type of function to load {@link Advanced_settings} from persistent storage.
 */
typedef struct Advanced_settings* (*Advanced_settings_load_fp_t)();

/**
 * Load the {@link Advanced_settings} from a
 * {@link Advanced_settings_save_data_v0}.
 *
 * @return The current {@link Advanced_settings} loaded from a
 *         {@link Advanced_settings_save_data_v0}.
 */
static struct Advanced_settings* advanced_settings_load_v0();

/**
 * Methods to load {@link Advanced_settings} from different persist versions.
 */
static const Advanced_settings_load_fp_t const
  advanced_settings_load_fp[NUM_PERSIST_VERSIONS] =
{
  (Advanced_settings_load_fp_t)advanced_settings_load_v0
};

/**
 * Get the current persist version.
 *
 * @return The current persist version.
 */
static int get_current_persist_version();

/**
 * Set the {@link Extended_vibes_state_t} of the {@link Advanced_settings}.
 *
 * @param settings             {@link Advanced_settings} to set the
 *                             {@link Extended_vibes_state_t} for.
 * @param extended_vibes_state {@link Extended_vibes_state_t} to set.
 */
static void advanced_settings_set_extended_vibes_state(
  struct Advanced_settings* settings,
  Extended_vibes_state_t extended_vibes_state);

/**
 * Get the {@link Extended_vibes_state_t} that comes after the given
 * {@link Extended_vibes_state_t}.
 *
 * @param  extended_vibes_state {@link Extended_vibes_state_t} to get the 'next'
 *                              of.
 * @return                      The {@link Extended_vibes_state_t} that comes
 *                              after the given {@link Extended_vibes_state_t}.
 */
static Extended_vibes_state_t get_next_extended_vibes_state(
  Extended_vibes_state_t extended_vibes_state);

/**
 * Data required for the {@link Advanced_settings}.
 */
struct Advanced_settings {
  /**
   * The modified state of these {@link Advanced_settings}.
   */
  Modified_state_t modified_state;
  /**
   * {@link Extended_vibes_state_t} for the {@link Advanced_settings}.
   */
  Extended_vibes_state_t extended_vibes_state;
};

/**
 * Persist version 0 of the {@link Advanced_settings}.
 */
struct Advanced_settings_save_data_v0 {
  Extended_vibes_state_t extended_vibes_state;
};

struct Advanced_settings* advanced_settings_create()
{
  struct Advanced_settings* settings =
    safe_alloc(sizeof(struct Advanced_settings));
  // The settings haven't been loaded, assume they need to be saved.
  settings->modified_state = MODIFIED_STATE_SINGLE;
  settings->extended_vibes_state = DEFAULT_EXTENDED_VIBES_STATE;
  return settings;
}

void advanced_settings_destroy(struct Advanced_settings* settings)
{
  free(settings);
}

struct Advanced_settings* advanced_settings_load()
{
  // Check that the advanced settings have been saved
  if (next_persist_exists() != 0) {
    APP_LOG(APP_LOG_LEVEL_INFO, "Advanced settings version hasn't been saved, "
      "returning default.");
    return advanced_settings_create();
  }
  // Get the persist version of the advanced settings
  int persist_version;
  persist_load(&persist_version, sizeof(persist_version));
  assert(persist_version >= 0);
  assert(persist_version <= get_current_persist_version());

  Modified_state_t modified_state =
    persist_version == get_current_persist_version() ?
      MODIFIED_STATE_NOT_MODIFIED : MODIFIED_STATE_SINGLE;
  s_persist_version_modified_state = modified_state;

  if (next_persist_exists() != 0) {
    APP_LOG(APP_LOG_LEVEL_INFO, "Advanced settings haven't been saved, "
      "returning default.");
    return advanced_settings_create();
  }
  // Load the advanced settings
  struct Advanced_settings* settings =
    (advanced_settings_load_fp[persist_version])();
  settings->modified_state = modified_state;
  return settings;
}

static struct Advanced_settings* advanced_settings_load_v0()
{
  struct Advanced_settings_save_data_v0 settings_save_data;
  persist_load(&settings_save_data,
    sizeof(struct Advanced_settings_save_data_v0));
  struct Advanced_settings* settings = advanced_settings_create();
  settings->extended_vibes_state = settings_save_data.extended_vibes_state;
  return settings;
}

void advanced_settings_save(const struct Advanced_settings* settings)
{
  assert(settings);
  assert(get_current_persist_version() >= 0);
  struct Advanced_settings_save_data_v0 settings_save_data;
  settings_save_data.extended_vibes_state = settings->extended_vibes_state;
  int persist_version = get_current_persist_version();
  persist_save(&persist_version, sizeof(persist_version),
    s_persist_version_modified_state);
  persist_save(&settings_save_data,
    sizeof(struct Advanced_settings_save_data_v0), settings->modified_state);
}

void advanced_settings_broadcast_state(const struct Advanced_settings* settings)
{
  assert(settings);
  model_notify_advanced_settings(settings->extended_vibes_state);
}

void advanced_settings_increment_field(struct Advanced_settings* settings,
  Advanced_settings_field_t settings_field)
{
  assert(settings);
  switch (settings_field) {
    case ADVANCED_SETTINGS_FIELD_EXTENDED_VIBES_STATE:
      advanced_settings_set_extended_vibes_state(settings,
        get_next_extended_vibes_state(settings->extended_vibes_state));
      break;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid advanced settings field.");
      break;
  }
}

Extended_vibes_state_t advanced_settings_get_extended_vibes_state(
  const struct Advanced_settings* settings)
{
  assert(settings);
  return settings->extended_vibes_state;
}

static void advanced_settings_set_extended_vibes_state(
  struct Advanced_settings* settings,
  Extended_vibes_state_t extended_vibes_state)
{
  assert(settings);
  settings->extended_vibes_state = extended_vibes_state;
  settings->modified_state = MODIFIED_STATE_SINGLE;
  model_notify_advanced_settings(settings->extended_vibes_state);
}

static Extended_vibes_state_t get_next_extended_vibes_state(
  Extended_vibes_state_t extended_vibes_state)
{
  switch (extended_vibes_state) {
    case EXTENDED_VIBES_DISABLED:
      return EXTENDED_VIBES_ENABLED;
    case EXTENDED_VIBES_ENABLED:
      return EXTENDED_VIBES_DISABLED;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid extended vibes state %d",
        extended_vibes_state);
      return EXTENDED_VIBES_DISABLED;
  }
}

static int get_current_persist_version()
{
  assert(NUM_PERSIST_VERSIONS > 0);
  return NUM_PERSIST_VERSIONS - 1;
}

/** @} // Advanced_settings */
