#ifndef VIEW_INTERFACE_H
#define VIEW_INTERFACE_H

#include "Settings_fields.h"
#include "Advanced_settings_fields.h"
#include "Timer_state.h"

/**
 * @addtogroup View_interface View interface
 * @{
 *
 * Methods that can be implemented to extend the abstract {@link View}.
 *
 * If a method returns int, the return value indicates whether the {@link View}
 * should be refreshed at the next refresh. A positive return value indicates
 * that the method made the {@link View} dirty and that it should be refreshed,
 * while a return value of zero indicates that the method didn't modify the
 * {@link View} and/or the {@link View} doesn't need to be refreshed.
 */

/**
 * Signature of the callback used to push the {@link View} implementation onto
 * the window stack.
 *
 * @param data  Pointer to the data that represents the {@link View}
 *              implementation.
 */
typedef void (*view_push_fp_t) (void* data);

/**
 * Signature of the callback used to destroy the {@link View} implementation.
 *
 * @param data  Pointer to the data that represents the {@link View}
 *              implementation.
 */
typedef void (*view_destroy_fp_t) (void* data);

/**
 * Signature of the callback used to notify this {@link View} implementation of
 * the {@link Timer Timer's} duration in seconds.
 *
 * @param  data     Pointer to the data that represents the {@link View}
 *                  implementation.
 * @param  group_id Id of the {@link Group} the {@link Timer} belongs to.
 * @param  timer_id Id of the {@link Timer} that is updating its duration.
 * @param  duration New duration for the {@link Timer} in seconds.
 * @return          Positive if this {@link View} should be refreshed, zero
 *                  otherwise.
 */
typedef int (*view_update_duration_fp_t) (void* data, int group_id,
  int timer_id, int duration);

/**
 * Signature of the callback used to notify this {@link View} implementation of
 * how many seconds are remaining for the {@link Timer}.
 *
 * @param  data       Pointer to the data that represents the {@link View}
 *                    implementation.
 * @param  timer_id   Id of the {@link Timer} that is updating its remaining
 *                    time.
 * @param  remaining  Remaining time for the {@link Timer} in seconds.
 * @return            Positive if this {@link View} should be refreshed, zero
 *                    otherwise.
 */
typedef int (*view_update_remaining_fp_t) (void* data, int timer_id,
  int remaining);

/**
 * Signature of the callback used to notify the {@link View} implementation
 * that the {@link Timer} was removed.
 *
 * @param  data     Pointer to the data that represents the {@link View}
 *                  implementation.
 * @param  timer_id Id of the {@link Timer} that was removed.
 * @return          Positive if this {@link View} should be refreshed, zero
 *                  otherwise.
 */
typedef int (*view_update_remove_timer_fp_t) (void* data, int timer_id);

/**
 * Signature of the callback used to notify the {@link View} implementation
 * of the {@link Timer Timer's} {@link Timer_state}.
 *
 * @param  data         Pointer to the data that represents the {@link View}
 *                      implementation.
 * @param  timer_id     Id of the {@link Timer} that is updating its
 *                      {@link Timer_state}.
 * @param  timer_state  {@link Timer_state} of the {@link Timer}.
 * @return              Positive if this {@link View} should be refreshed, zero
 *                      otherwise.
 */
typedef int (*view_update_timer_state_fp_t) (void* data, int timer_id,
  Timer_state_t timer_state);

/**
 * Signature of the callback used to notify the {@link View} implementation
 * that the {@link Group} exists.
 *
 * @param  data     Pointer to the data that represents the {@link View}
 *                  implementation.
 * @param  group_id Id of the {@link Group} that was added.
 * @return          Positive if this {@link View} should be refreshed, zero
 *                  otherwise.
 */
typedef int (*view_update_group_exists_fp_t) (void* data, int group_id);

/**
 * Signature of the callback used to notify the {@link View} implementation
 * that the {@link Group} was removed.
 *
 * @param  data     Pointer to the data that represents the {@link View}
 *                  implementation.
 * @param  group_id Id of the {@link Group} that was removed.
 * @return          Positive if this {@link View} should be refreshed, zero
 *                  otherwise.
 */
typedef int (*view_update_remove_group_fp_t) (void* data, int group_id);

/**
 * Signature of the callback used to notify the {@link View} implementation
 * that the {@link Settings} were updated.
 *
 * @param  data             Pointer to the data that represents the {@link View}
 *                          implementation.
 * @param  group_id         Id of the {@link Group} that the {@link Settings}
 *                          are for, or negative if these are the global app
 *                          {@link Settings}.
 * @param  repeat_style     The new {@link Repeat_style_t}.
 * @param  progress_style   The new {@link Progress_style_t}.
 * @param  vibrate_interval The new {@link Vibrate_interval_t}.
 * @param  vibrate_style    The new {@link Vibrate_style_t}.
 * @return                  Positive if this {@link View} should be refreshed,
 *                          zero otherwise.
 */
typedef int (*view_update_settings_fp_t) (void* data, int group_id,
  Repeat_style_t repeat_style, Progress_style_t progress_style,
  Vibrate_interval_t vibrate_interval, Vibrate_style_t vibrate_style);

/**
 * Signature of the callback used to notify the {@link View} implementation
 * that the {@link Advanced_settings} were updated.
 *
 * @param  data                Pointer to the data that represents the
 *                             {@link View} implementation.
 * @param extended_vibes_state The new {@link Extended_vibes_state_t}.
 * @return                     Positive if this {@link View} should be
 *                             refreshed, zero otherwise.
 */
typedef int (*view_update_advanced_settings_fp_t) (void* data,
  Extended_vibes_state_t extended_vibes_state);

// @param previous_timer_id: if negative, forces update to the new timer id
/**
 * Signature of the callback used to update the {@link View} implementation
 * of the {@link Timer} to track.
 *
 * @param  data               Pointer to the data that represents the
 *                            {@link View} implementation.
 * @param  timer_id           Id of the {@link Timer} to track.
 * @param  previous_timer_id  Id of the {@link Timer} that was previously being
 *                            tracked.
 * @return                    Positive if this {@link View} should be refreshed,
 *                            zero otherwise.
 */
typedef int (*view_update_timer_tracking_fp_t) (void* data, int timer_id,
  int previous_timer_id);

/**
 * Signature of the callback used to notify the {@link View} implementation
 * that an error occurred.
 *
 * @param  data Pointer to the data that represents the {@link View}
 *              implementation.
 * @param  msg  Error message.
 * @return      Positive if this {@link View} should be refreshed, zero
 *              otherwise.
 */
typedef int (*view_update_error_fp_t) (void* data, char* msg);

/**
 * Signature of the callback used to refresh the {@link View} implementation.
 *
 * @param data  Pointer to the data that represents the {@link View}
 *              implementation.
 */
typedef void (*view_refresh_fp_t) (void* data);

/**
 * Struct to hold all the view interface callbacks.
 */
struct View_interface {
  /**
   * Callback used to push the {@link View} implementation onto the window
   * stack (required).
   */
  view_push_fp_t push;
  /**
   * Callback used to destroy the {@link View} implementation (required).
   */
  view_destroy_fp_t destroy;
  /**
   * Callback used to update the {@link View} implementation of a
   * {@link Timer Timer's} duration.
   */
  view_update_duration_fp_t update_duration;
  /**
   * Callback used to update the {@link View} implementation of how much time
   * is remaining for a {@link Timer}.
   */
  view_update_remaining_fp_t update_remaining;
  /**
   * Callback used to notify the {@link View} implementation that the
   * {@link Timer} was removed.
   */
  view_update_remove_timer_fp_t update_remove_timer;
  /**
   * Callback used to notify the {@link View} implementation of the
   * {@link Timer Timer's} {@link Timer_state}.
   */
  view_update_timer_state_fp_t update_timer_state;
  /**
   * Callback used to notify the {@link View} implementation that the
   * {@link Group} exists.
   */
  view_update_group_exists_fp_t update_group_exists;
  /**
   * Callback used to notify the {@link View} implementation that the
   * {@link Group} was removed.
   */
  view_update_remove_group_fp_t update_remove_group;
  /**
   * Callback used to notify the {@link View} implementation that the
   * {@link Settings} were updated.
   */
  view_update_settings_fp_t update_group_settings;
  /**
   * Callback used to notify the {@link View} implementation that the
   * {@link Advanced_settings} were updated.
   */
  view_update_advanced_settings_fp_t update_advanced_settings;
  /**
   * Callback used to update the {@link View} implementation of the
   * {@link Timer} to track.
   */
  view_update_timer_tracking_fp_t update_timer_tracking;
  /**
   * Callback used to notify the {@link View} implementation that an error
   * occurred.
   */
  view_update_error_fp_t update_error;
  /**
   * Callback used to refresh the {@link View} implementation implementation.
   */
  view_refresh_fp_t refresh;
};

/** @} // View_interface */

#endif /*VIEW_INTERFACE_H*/
