#ifndef EDIT_VIEW_H
#define EDIT_VIEW_H

/**
 * @addtogroup Edit_view Edit view
 * @{
 *
 * {@link View} implementation used to edit a {@link Timer}.
 */

struct View;

/**
 * Create an {@link Edit_view edit view}.
 *
 * @param  timer_id Id of the {@link Timer} to edit.
 * @return          The abstract {@link View} handle for the
 *                  {@link Edit_view edit view}.
 */
struct View* edit_view_create(int timer_id);

/** @} // Edit_view */

#endif /*EDIT_VIEW_H*/
