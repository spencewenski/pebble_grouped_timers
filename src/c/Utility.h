#ifndef UTILITY_H
#define UTILITY_H

/**
 * @addtogroup utility Utility
 * @{
 *
 * Utility methods used throughout the app.
 */

#include <pebble.h>

/**
 * Wrapper around malloc that checks that memory was successfully allocated.
 *
 * @param  size Number of bytes to allocate.
 * @return      The pointer to the memory that was allocated.
 */
void* safe_alloc(int size);

/**
 * Check if the given value is in the range of [min, max).
 *
 * @param  value Value to check if it's in the range.
 * @param  min   Lower bound of the range.
 * @param  max   Upper bound of the range.
 * @return       Non-zero (true) if in range, zero (false) otherwise.
 */
int in_range(int value, int min, int max);

/**
 * Get the minimum value.
 *
 * @param  a First value to compare.
 * @param  b Second value to compare.
 * @return   The minimum of the given values.
 */
int min(int a, int b);

/**
 * Get the maximum value.
 *
 * @param  a First value to compare.
 * @param  b Second value to compare.
 * @return   The maximum of the given values.
 */
int max(int a, int b);

/** @} // utility */

#endif /*UTILITY_H*/
