/**
 * @internal
 * @addtogroup Settings_view_data
 * @{
 */

#include "Settings_view_data.h"
#include "Settings_fields.h"
#include "Utility.h"
#include "assert.h"

#include <pebble.h>

struct Icon_info;

/**
 * Choose either the default or group settings value.
 *
 * @param  group_id                   Id of the group the
 *                                    {@link Settings_view_data} is for, or
 *                                    negative if the {@link Settings_view_data}
 *                                    is the default {@link Settings}.
 * @param  use_default_settings_enum  The enum value of the
 *                                    {@link Settings_field_t} that indicates
 *                                    that the field should use the default
 *                                    settings, e.g.
 *                                    {@link REPEAT_STYLE_USE_APP_SETTINGS}.
 * @param  default_settings_value     The default value of a
 *                                    {@link Settings_field_t}.
 * @param  group_settings_value       The group's value of a
 *                                    {@link Settings_field_t}.
 * @return                            If the group_id is negative or the
 *                                    group_settings_value is equal to
 *                                    use_default_settings_enum, return
 *                                    default_settings_value. Otherwise,
 *                                    return group_settings_value.
 */
static int choose_group_or_default_settings_value(int group_id,
  int use_default_settings_enum, int default_settings_value,
  int group_settings_value);

/**
 * Signature of a function to get the resource id for the given settings enum
 * value.
 *
 * @param  settings_enum_value   The enum value of a {@link Settings_field_t}.
 * @return                       The resource id for the given
 *                               {@link Settings_field_t} value.
 */
typedef uint32_t (*Get_resource_id_fp_t) (int settings_enum_value);

/**
 * {@link Get_resource_id_fp_t} for {@link Repeat_style_t}.
 */
static uint32_t get_icon_resource_id_repeat_style(
  Repeat_style_t repeat_style);

/**
 * {@link Get_resource_id_fp_t} for {@link Progress_style_t}.
 */
static uint32_t get_icon_resource_id_progress_style(
  Progress_style_t progress_style);

/**
 * {@link Get_resource_id_fp_t} for {@link Vibrate_interval_t}.
 */
static uint32_t get_icon_resource_id_vibrate_interval(
  Vibrate_interval_t vibrate_interval);

/**
 * {@link Get_resource_id_fp_t} for {@link Vibrate_style_t}.
 */
static uint32_t get_icon_resource_id_vibrate_style(
  Vibrate_style_t vibrate_style);

/**
 * Swap the icon in the given {@link Icon_info} with the icon loaded from
 * the given resource id.
 *
 * If the icon is already in the given {@link Icon_info}, do nothing. If a
 * new icon is loaded, unload the icon currently in the {@link Icon_info}.
 *
 * @param icon_info       {@link Icon_info} to load the bitmap icon into.
 * @param new_resource_id Resource id of the bitmap to load into the
 *                        {@link Icon_info}.
 */
static void icon_info_swap_icon(struct Icon_info* icon_info,
  uint32_t new_resource_id);

/**
 * Unload the icon in the given {@link Icon_info}.
 *
 * @param icon_info {@link Icon_info} to unload the icon from.
 */
static void icon_info_unload_icon(struct Icon_info* icon_info);

/**
 * Settings data for the {@link Settings_view}
 */
struct Settings_view_data {
  /**
   * {@link Repeat_style_t} for the {@link Settings}.
   */
  Repeat_style_t repeat_style;
  /**
   * {@link Progress_style_t} for the {@link Settings}.
   */
  Progress_style_t progress_style;
  /**
   * {@link Vibrate_interval_t} for the {@link Settings}.
   */
  Vibrate_interval_t vibrate_interval;
  /**
   * {@link Vibrate_style_t} for the {@link Settings}.
   */
  Vibrate_style_t vibrate_style;
};

/**
 * Struct to hold data for an icon.
 */
struct Icon_info {
  /**
   * Resource id of the bitmap that is currently loaded in this
   * {@link Icon_info}.
   */
  uint32_t resource_id;
  /**
   * The bitmap that is currently loaded in this {@link Icon_info}.
   */
  GBitmap* bitmap;
};

/**
 * Icons for the {@link Settings}.
 */
struct Settings_view_icons {
  /**
   * {@Icon_info} for {@link Repeat_style_t}.
   */
  struct Icon_info repeat_style;
  /**
   * {@Icon_info} for {@link Progress_style_t}.
   */
  struct Icon_info progress_style;
  /**
   * {@Icon_info} for {@link Vibrate_interval_t}.
   */
  struct Icon_info vibrate_interval;
  /**
   * {@Icon_info} for {@link Vibrate_style_t}.
   */
  struct Icon_info vibrate_style;
};

struct Settings_view_data* settings_view_data_create()
{
  return safe_alloc(sizeof(struct Settings_view_data));
}

void settings_view_data_destroy(struct Settings_view_data* settings_view_data)
{
  free(settings_view_data);
}

void settings_view_data_set_values(
  struct Settings_view_data* settings_view_data, Repeat_style_t repeat_style,
  Progress_style_t progress_style, Vibrate_interval_t vibrate_interval,
  Vibrate_style_t vibrate_style)
{
  assert(settings_view_data);
  settings_view_data->repeat_style = repeat_style;
  settings_view_data->progress_style = progress_style;
  settings_view_data->vibrate_interval = vibrate_interval;
  settings_view_data->vibrate_style = vibrate_style;
}

Repeat_style_t settings_view_data_get_repeat_style(
  const struct Settings_view_data* settings_view_data)
{
  assert(settings_view_data);
  return settings_view_data->repeat_style;
}

Progress_style_t settings_view_data_get_progress_style(
  const struct Settings_view_data* settings_view_data)
{
  assert(settings_view_data);
  return settings_view_data->progress_style;
}

Vibrate_interval_t settings_view_data_get_vibrate_interval(
  const struct Settings_view_data* settings_view_data)
{
  assert(settings_view_data);
  return settings_view_data->vibrate_interval;
}

Vibrate_style_t settings_view_data_get_vibrate_style(
  const struct Settings_view_data* settings_view_data)
{
  assert(settings_view_data);
  return settings_view_data->vibrate_style;
}

Vibrate_style_t settings_view_data_get_group_vibrate_style(
  int group_id,
  const struct Settings_view_data* default_settings_data,
  const struct Settings_view_data* group_settings_data)
{
  return settings_view_data_get_group_vibrate_style_from_styles(group_id,
    settings_view_data_get_vibrate_style(default_settings_data),
    settings_view_data_get_vibrate_style(group_settings_data));
}

Vibrate_style_t settings_view_data_get_group_vibrate_style_from_styles(
  int group_id,
  Vibrate_style_t default_style,
  Vibrate_style_t group_style)
{
  if (group_id < 0 || group_style == VIBRATE_STYLE_USE_APP_SETTINGS) {
    return default_style;
  }
  return group_style;
}

struct Settings_view_icons* settings_view_icons_create()
{
  struct Settings_view_icons* settings_view_icons =
    safe_alloc(sizeof(struct Settings_view_icons));
  settings_view_icons->repeat_style.bitmap = NULL;
  settings_view_icons->progress_style.bitmap = NULL;
  settings_view_icons->vibrate_interval.bitmap = NULL;
  settings_view_icons->vibrate_style.bitmap = NULL;
  return settings_view_icons;
}

void settings_view_icons_destroy(
  struct Settings_view_icons* settings_view_icons)
{
  free(settings_view_icons);
}

void settings_view_icons_unload(
  struct Settings_view_icons* settings_view_icons)
{
  assert(settings_view_icons);
  icon_info_unload_icon(&(settings_view_icons->repeat_style));
  icon_info_unload_icon(&(settings_view_icons->progress_style));
  icon_info_unload_icon(&(settings_view_icons->vibrate_interval));
  icon_info_unload_icon(&(settings_view_icons->vibrate_style));
}

static void icon_info_unload_icon(struct Icon_info* icon_info)
{
  if (!icon_info->bitmap) {
    return;
  }
  gbitmap_destroy(icon_info->bitmap);
  icon_info->bitmap = NULL;
}

GBitmap* settings_view_icons_get_for_field(
  struct Settings_view_icons* settings_view_icons,
  int group_id,
  const struct Settings_view_data* default_settings_data,
  const struct Settings_view_data* group_settings_data,
  Settings_field_t settings_field)
{
  assert(settings_view_icons);
  assert(default_settings_data);
  assert(group_settings_data);

  int use_default_settings_enum = -1;
  int default_settings_value = -1;
  int group_settings_value = -1;
  Get_resource_id_fp_t id_func = NULL;
  struct Icon_info* icon_info = NULL;;
  switch (settings_field) {
    case SETTINGS_FIELD_REPEAT_STYLE:
      use_default_settings_enum = REPEAT_STYLE_USE_APP_SETTINGS;
      default_settings_value = default_settings_data->repeat_style;
      group_settings_value = group_settings_data->repeat_style;
      id_func = (Get_resource_id_fp_t)get_icon_resource_id_repeat_style;
      icon_info = &(settings_view_icons->repeat_style);
      break;
    case SETTINGS_FIELD_PROGRESS_STYLE:
      use_default_settings_enum = PROGRESS_STYLE_USE_APP_SETTINGS;
      default_settings_value = default_settings_data->progress_style;
      group_settings_value = group_settings_data->progress_style;
      id_func = (Get_resource_id_fp_t)get_icon_resource_id_progress_style;
      icon_info = &(settings_view_icons->progress_style);
      break;
    case SETTINGS_FIELD_VIBRATE_INTERVAL:
      use_default_settings_enum = VIBRATE_INTERVAL_USE_APP_SETTINGS;
      default_settings_value = default_settings_data->vibrate_interval;
      group_settings_value = group_settings_data->vibrate_interval;
      id_func = (Get_resource_id_fp_t)get_icon_resource_id_vibrate_interval;
      icon_info = &(settings_view_icons->vibrate_interval);
      break;
    case SETTINGS_FIELD_VIBRATE_STYLE:
      use_default_settings_enum = VIBRATE_STYLE_USE_APP_SETTINGS;
      default_settings_value = default_settings_data->vibrate_style;
      group_settings_value = group_settings_data->vibrate_style;
      id_func = (Get_resource_id_fp_t)get_icon_resource_id_vibrate_style;
      icon_info = &(settings_view_icons->vibrate_style);
      break;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid settings field: %d",
        settings_field);
      return NULL;
  }
  int enum_value = choose_group_or_default_settings_value(group_id,
    use_default_settings_enum, default_settings_value, group_settings_value);
  uint32_t resource_id = id_func(enum_value);
  icon_info_swap_icon(icon_info, resource_id);
  return icon_info->bitmap;
}

static int choose_group_or_default_settings_value(int group_id,
  int use_default_settings_enum, int default_settings_value,
  int group_settings_value)
{
  if (group_id < 0 || group_settings_value == use_default_settings_enum) {
    return default_settings_value;
  }
  return group_settings_value;
}

static uint32_t get_icon_resource_id_repeat_style(
  Repeat_style_t repeat_style)
{
  switch (repeat_style) {
    case REPEAT_STYLE_NONE:
      return RESOURCE_ID_REPEAT_STYLE_NONE_ICON;
    case REPEAT_STYLE_SINGLE:
      return RESOURCE_ID_REPEAT_STYLE_SINGLE_ICON;
    case REPEAT_STYLE_GROUP:
      return RESOURCE_ID_REPEAT_STYLE_GROUP_ICON;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid repeat style: %d", repeat_style);
      return -1;
  }
}

static uint32_t get_icon_resource_id_progress_style(
  Progress_style_t progress_style)
{
  switch (progress_style) {
    case PROGRESS_STYLE_AUTO:
      return RESOURCE_ID_PROGRESS_STYLE_AUTO_ICON;
    case PROGRESS_STYLE_WAIT_FOR_USER:
      return RESOURCE_ID_PROGRESS_STYLE_WAIT_ICON;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid progress style: %d",
        progress_style);
      return -1;
  }
}

static uint32_t get_icon_resource_id_vibrate_interval(
  Vibrate_interval_t vibrate_interval)
{
  switch (vibrate_interval) {
    case VIBRATE_INTERVAL_SHORT:
      return RESOURCE_ID_VIBRATE_INTERVAL_SHORT_ICON;
    case VIBRATE_INTERVAL_MEDIUM:
      return RESOURCE_ID_VIBRATE_INTERVAL_MEDIUM_ICON;
    case VIBRATE_INTERVAL_LONG:
      return RESOURCE_ID_VIBRATE_INTERVAL_LONG_ICON;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid vibrate interval: %d",
        vibrate_interval);
      return -1;
  }
}

static uint32_t get_icon_resource_id_vibrate_style(
  Vibrate_style_t vibrate_style)
{
  switch (vibrate_style) {
    case VIBRATE_STYLE_SHORT_PULSE:
      return RESOURCE_ID_VIBRATE_STYLE_SHORT_ICON;
    case VIBRATE_STYLE_LONG_PULSE:
      return RESOURCE_ID_VIBRATE_STYLE_LONG_ICON;
    case VIBRATE_STYLE_DOUBLE_PULSE:
      return RESOURCE_ID_VIBRATE_STYLE_DOUBLE_ICON;
    case VIBRATE_STYLE_EXTENDED_0:
      return RESOURCE_ID_VIBRATE_STYLE_EXTENDED_0_ICON;
    case VIBRATE_STYLE_EXTENDED_1:
      return RESOURCE_ID_VIBRATE_STYLE_EXTENDED_1_ICON;
    case VIBRATE_STYLE_EXTENDED_2:
      return RESOURCE_ID_VIBRATE_STYLE_EXTENDED_2_ICON;
    case VIBRATE_STYLE_EXTENDED_3:
      return RESOURCE_ID_VIBRATE_STYLE_EXTENDED_3_ICON;
    case VIBRATE_STYLE_EXTENDED_4:
      return RESOURCE_ID_VIBRATE_STYLE_EXTENDED_4_ICON;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid vibrate style: %d", vibrate_style);
      return -1;
  }
}

static void icon_info_swap_icon(struct Icon_info* icon_info,
  uint32_t new_resource_id)
{
  if (icon_info->resource_id == new_resource_id && icon_info->bitmap) {
    return;
  }
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Resource id is different and/or the icon "
    "hasn't been loaded, so load the new icon.");
  icon_info_unload_icon(icon_info);
  icon_info->resource_id = new_resource_id;
  icon_info->bitmap = gbitmap_create_with_resource(new_resource_id);
}

/** @} // Settings_view_data */
