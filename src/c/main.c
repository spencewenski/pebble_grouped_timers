#include "Controller.h"

#include <pebble.h>

int main()
{
  controller_app_open();
  app_event_loop();
  controller_app_close();
}
