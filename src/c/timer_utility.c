/**
 * @internal
 * @addtogroup timer_utility
 * @{
 */

#include "timer_utility.h"
#include "globals.h"

#include <pebble.h>

/**
 * Convert the given value from {@link Timer_field_t} into
 * {@link TIMER_FIELD_MS milliseconds}.
 *
 * This is the same as {@link timer_convert_field_to_seconds}, except for
 * {@link TIMER_FIELD_MS milliseconds}.
 *
 * @param  field {@link Timer_field_t} of the given value
 * @param  value Value of the {@link Timer_field_t} to get as milliseconds.
 * @return       The given value converted to
 *               {@link TIMER_FIELD_MS milliseconds}.
 */
static int timer_convert_field_to_ms(Timer_field_t field, int value);

/**
 * Get the {@link Timer_field_t} from the given number of
 * {@link TIMER_FIELD_MS milliseconds}.
 *
 * This is the same as {@link timer_get_field_from_seconds}, except for
 * {@link TIMER_FIELD_MS milliseconds}.
 *
 * @param  field  {@link Timer_field_t} to get from the given number of
 *                {@link TIMER_FIELD_MS milliseconds}.
 * @param  ms     The number of {@link TIMER_FIELD_MS milliseconds} to use to
 *                get the {@link Timer_field_t}.
 * @return        The {@link Timer_field_t} from the given number of
 *                {@link TIMER_FIELD_MS milliseconds}.
 */
static int timer_get_field_from_ms(Timer_field_t field, int ms);

int timer_convert_field_to_seconds(Timer_field_t field, int value)
{
  return timer_convert_field_to_ms(field, value) / MS_PER_SECOND;
}

static int timer_convert_field_to_ms(Timer_field_t field, int value)
{
  int conversion = 1;
  switch (field) {
    case TIMER_FIELD_HOURS:
      conversion = SECONDS_PER_HOUR * MS_PER_SECOND;
      break;
    case TIMER_FIELD_MINUTES:
      conversion = MS_PER_MINUTE;
      break;
    case TIMER_FIELD_SECONDS:
      conversion = MS_PER_SECOND;
      break;
    case TIMER_FIELD_MS:
      conversion = 1;
      break;
    default:
      APP_LOG(APP_LOG_LEVEL_DEBUG, "Invalid timer field.");
  }
  return value * conversion;
}

int timer_get_field_from_seconds(Timer_field_t field, int seconds)
{
  return timer_get_field_from_ms(field, seconds * MS_PER_SECOND);
}

static int timer_get_field_from_ms(Timer_field_t field, int ms)
{
  switch (field) {
    case TIMER_FIELD_HOURS:
      return ms / (SECONDS_PER_HOUR * MS_PER_SECOND);
    case TIMER_FIELD_MINUTES:
      return (ms % (SECONDS_PER_HOUR * MS_PER_SECOND)) / MS_PER_MINUTE;
    case TIMER_FIELD_SECONDS:
      return (ms % MS_PER_MINUTE) / MS_PER_SECOND;
    case TIMER_FIELD_MS:
      return ms % MS_PER_SECOND;
    default:
      APP_LOG(APP_LOG_LEVEL_DEBUG, "Invalid timer field.");
  }
  return 0;
}

/** @} // timer_utility */
