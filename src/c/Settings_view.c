/**
 * @internal
 * @addtogroup Settings_view
 * @{
 */

#include "Settings_view.h"
#include "Settings_fields.h"
#include "assert.h"
#include "draw_utility.h"
#include "View.h"
#include "Utility.h"
#include "Controller.h"
#include "View_interface.h"
#include "Settings_view_data.h"
#include "Vibrate_manager.h"
#include "locale_framework/src/localize.h"

#include <pebble.h>

/**
 * @internal
 * @addtogroup Settings_view_interface_impl
 * @{
 *
 * Implementations of the {@link View_interface} for the {@link Settings_view}.
 */

/** {@link view_push_fp_t} */
static void settings_view_push(void* data);
/** {@link view_destroy_fp_t} */
static void settings_view_destroy(void* data);
/** {@link view_update_settings_fp_t} */
static int settings_view_update_settings(void* data, int group_id,
  Repeat_style_t repeat_style, Progress_style_t progress_style,
  Vibrate_interval_t vibrate_interval, Vibrate_style_t vibrate_style);
/** {@link view_refresh_fp_t} */
static void settings_view_refresh(void* data);

/** @} // Settings_view_interface_impl */

/**
 * Data used by the {@link Settings_view}.
 */
struct Settings_view {
  /**
   * Pebble window for this view.
   */
  Window* window;
  /**
   * Pebble menu layer to display the {@link Settings} fields.
   */
  MenuLayer* menu_layer;
  /**
   * Pebble status bar layer to display the time at the top of the screen.
   */
  StatusBarLayer* status_bar_layer;
  /**
   * Parent {@link View} object for this {@link Settings_view}.
   */
  struct View* view;
  /**
   * Id of the {@link Group} the {@link Settings} are for, or negative if the
   * {@link Settings} are for the app.
   */
  int group_id;
  /**
   * {@link Settings_view_data} for the default {@link Settings}
   */
  struct Settings_view_data* default_settings_data;
  /**
   * {@link Settings_view_data} for a group's {@link Settings}
   */
  struct Settings_view_data* group_settings_data;
  /**
   * {@link Settings_view_icons} for the {@link Settings_view}.
   */
  struct Settings_view_icons* settings_view_icons;
};

/**
 * Translate the index into a {@link Settings_field_t}.
 *
 * The index should be a valid index of the {@link Settings_view} menu layer.
 *
 * @param  settings_field_index Index to translate into a
 *                              {@link Settings_field_t}.
 * @return                      {@link Settings_field_t} for the index.
 */
static Settings_field_t get_settings_field(int settings_field_index);

/**
 * Get the display text for the {@link Settings_field_t}.
 *
 * @param  settings_field The {@link Settings_field_t} to get the text for.
 * @return                The display text for the {@link Settings_field_t}.
 */
static const char* get_settings_field_text(Settings_field_t settings_field);

/**
 * Get the display text for the {@link Repeat_style_t}.
 *
 * @param  repeat_style The {@link Repeat_style_t} to get the text for.
 * @return              The display text for the {@link Repeat_style_t}.
 */
static const char* get_repeat_style_text(Repeat_style_t repeat_style);

/**
 * Get the display text for the {@link Progress_style_t}.
 *
 * @param  progress_style The {@link Progress_style_t} to get the text for.
 * @return                The display text for the {@link Progress_style_t}.
 */
static const char* get_progress_style_text(Progress_style_t progress_style);

/**
 * Get the display text for the {@link Vibrate_interval_t}.
 *
 * @param  vibrate_interval The {@link Vibrate_interval_t} to get the text for.
 * @return                  The display text for the {@link Vibrate_interval_t}.
 */
static const char* get_vibrate_interval_text(
  Vibrate_interval_t vibrate_interval);

/**
 * Get the display text for the {@link Vibrate_style_t}.
 *
 * @param  vibrate_style  The {@link Vibrate_style_t} to get the text for.
 * @return                The display text for the {@link Vibrate_style_t}.
 */
static const char* get_vibrate_style_text(Vibrate_style_t vibrate_style);

/**
 * Replacement of the static "Use default" string to allow the string to be
 * picked up by the localization framework.
 *
 * @return a localized version of the "Use default" text
 */
static const char* get_use_default_text();

/**
 * Cancel vibes if the group's vibrate style is one of the extended vibrate
 * styles.
 *
 * @param settings_view The {@link Settings_view} to cancel long vibes for.
 */
static void cancel_long_vibes(const struct Settings_view* settings_view);

// WindowHandlers
static void window_load_handler(Window* window);
static void window_unload_handler(Window* window);

// MenuLayerCallbacks
static uint16_t menu_get_num_sections_callback(MenuLayer* menu_layer,
  void* data);
static uint16_t menu_get_num_rows_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data);
static void menu_draw_row_callback(GContext* ctx, const Layer* cell_layer,
  MenuIndex* cell_index, void* data);
static void menu_draw_header_callback(GContext* ctx, const Layer* cell_layer,
  uint16_t section_index, void* data);
static void menu_select_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data);
static void menu_selection_changed_callback(MenuLayer* menu_layer,
  MenuIndex new_index, MenuIndex old_index, void* data);

struct View* settings_view_create(int group_id)
{
  struct Settings_view* settings_view =
    safe_alloc(sizeof(struct Settings_view));
  settings_view->group_id = group_id;
  settings_view->default_settings_data = settings_view_data_create();
  settings_view->group_settings_data = settings_view_data_create();
  settings_view->settings_view_icons = settings_view_icons_create();
  struct View_interface view_interface = (struct View_interface) {
    .push = settings_view_push,
    .update_group_settings = settings_view_update_settings,
    .refresh = settings_view_refresh,
    .destroy = settings_view_destroy
  };
  settings_view->view = view_create(settings_view, &view_interface);
  return settings_view->view;
}

static void settings_view_destroy(void* data)
{
  assert(data);
  struct Settings_view* settings_view = data;
  settings_view_data_destroy(settings_view->default_settings_data);
  settings_view->default_settings_data = NULL;
  settings_view_data_destroy(settings_view->group_settings_data);
  settings_view->group_settings_data = NULL;
  settings_view_icons_destroy(settings_view->settings_view_icons);
  settings_view->settings_view_icons = NULL;
  free(settings_view);
}

static void settings_view_push(void* data)
{
  assert(data);
  struct Settings_view* settings_view = (struct Settings_view*) data;

  settings_view->window = window_create();

  assert(settings_view->window);

  window_set_user_data(settings_view->window, settings_view);
  window_set_window_handlers(settings_view->window, (WindowHandlers) {
    .load = window_load_handler,
    .unload = window_unload_handler
  });
  window_stack_push(settings_view->window, false);
}

static int settings_view_update_settings(void* data, int group_id,
  Repeat_style_t repeat_style, Progress_style_t progress_style,
  Vibrate_interval_t vibrate_interval, Vibrate_style_t vibrate_style)
{
  assert(data);
  struct Settings_view* settings_view = (struct Settings_view*) data;

  int modified = 0;
  if (group_id < 0) {
    ++modified;
    settings_view_data_set_values(settings_view->default_settings_data,
      repeat_style, progress_style, vibrate_interval, vibrate_style);
  }

  if (group_id != settings_view->group_id) {
    return modified;
  }
  settings_view_data_set_values(settings_view->group_settings_data,
    repeat_style, progress_style, vibrate_interval, vibrate_style);
  return ++modified;
}

static void settings_view_refresh(void* data)
{
  assert(data);
  struct Settings_view* settings_view = (struct Settings_view*) data;
  menu_layer_reload_data(settings_view->menu_layer);
}

static void window_load_handler(Window* window)
{
  struct Settings_view* settings_view = window_get_user_data(window);
  assert(settings_view);

  Layer* window_layer = window_get_root_layer(settings_view->window);

  // Status bar layer
  settings_view->status_bar_layer = status_bar_layer_create();
  layer_add_child(window_layer,
    status_bar_layer_get_layer(settings_view->status_bar_layer));

  // Settings layer
  GRect bounds = layer_get_bounds(window_layer);
  bounds = status_bar_adjust_window_bounds(bounds);
  settings_view->menu_layer = menu_layer_create(bounds);
  assert(settings_view->menu_layer);

  menu_layer_set_callbacks(settings_view->menu_layer, settings_view,
    (MenuLayerCallbacks) {
      .get_num_sections = menu_get_num_sections_callback,
      .get_num_rows = menu_get_num_rows_callback,
      .get_header_height = menu_get_header_height_callback,
      .draw_header = menu_draw_header_callback,
      .get_cell_height = PBL_IF_ROUND_ELSE(menu_cell_get_height_round, NULL),
      .draw_row = menu_draw_row_callback,
      .select_click = menu_select_click_callback,
      .selection_changed = menu_selection_changed_callback
    }
  );
  menu_layer_set_click_config_onto_window(settings_view->menu_layer, window);
  layer_add_child(window_layer,
    menu_layer_get_layer(settings_view->menu_layer));
}

static void window_unload_handler(Window* window)
{
  struct Settings_view* settings_view = window_get_user_data(window);
  assert(settings_view);

  cancel_long_vibes(settings_view);

  settings_view_icons_unload(settings_view->settings_view_icons);

  menu_layer_destroy(settings_view->menu_layer);
  settings_view->menu_layer = NULL;

  status_bar_layer_destroy(settings_view->status_bar_layer);
  settings_view->status_bar_layer = NULL;

  window_destroy(settings_view->window);
  settings_view->window = NULL;

  controller_on_view_closed(settings_view->view);
}

static uint16_t menu_get_num_sections_callback(MenuLayer* menu_layer,
  void* data)
{
  return 1;
}

static uint16_t menu_get_num_rows_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data)
{
  return NUM_SETTINGS_FIELDS;
}

static void menu_draw_header_callback(GContext* ctx, const Layer* cell_layer,
  uint16_t section_index, void* data)
{
  assert(data);
  struct Settings_view* settings_view = (struct Settings_view*) data;

  switch (section_index) {
    case 0:
      if (settings_view->group_id < 0) {
        menu_cell_draw_header(ctx, cell_layer, LOCALIZE("Default settings"));
      } else {
        menu_cell_draw_header(ctx, cell_layer, LOCALIZE("Group settings"));
      }
      return;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid section index: %d", section_index);
      return;
  }
}

static void menu_draw_row_callback(GContext* ctx, const Layer* cell_layer,
  MenuIndex* cell_index, void* data)
{
  assert(data);
  struct Settings_view* settings_view = (struct Settings_view*) data;

  Settings_field_t settings_field = get_settings_field(cell_index->row);

  const char* subtitle_text;
  switch (settings_field) {
    case SETTINGS_FIELD_REPEAT_STYLE:
      subtitle_text = get_repeat_style_text(settings_view_data_get_repeat_style(
        settings_view->group_settings_data));
      break;
    case SETTINGS_FIELD_PROGRESS_STYLE:
      subtitle_text = get_progress_style_text(
        settings_view_data_get_progress_style(
          settings_view->group_settings_data));
      break;
    case SETTINGS_FIELD_VIBRATE_INTERVAL:
      subtitle_text = get_vibrate_interval_text(
        settings_view_data_get_vibrate_interval(
          settings_view->group_settings_data));
      break;
    case SETTINGS_FIELD_VIBRATE_STYLE:
      subtitle_text = get_vibrate_style_text(
        settings_view_data_get_vibrate_style(
          settings_view->group_settings_data));
      break;
    default:
      subtitle_text = "";
      break;
  }

  // Get the icon for this field
  GBitmap* icon = settings_view_icons_get_for_field(
    settings_view->settings_view_icons,
    settings_view->group_id, settings_view->default_settings_data,
    settings_view->group_settings_data, settings_field);
  set_palette(icon, menu_cell_layer_is_highlighted(cell_layer));

  menu_cell_basic_draw(ctx, cell_layer,
    get_settings_field_text(settings_field), subtitle_text, icon);
}

static Settings_field_t get_settings_field(int settings_field_index)
{
  switch (settings_field_index) {
    case 0:
      return SETTINGS_FIELD_REPEAT_STYLE;
    case 1:
      return SETTINGS_FIELD_PROGRESS_STYLE;
    case 2:
      return SETTINGS_FIELD_VIBRATE_INTERVAL;
    case 3:
      return SETTINGS_FIELD_VIBRATE_STYLE;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid settings field index: %d",
        settings_field_index);
      return SETTINGS_FIELD_VIBRATE_INTERVAL;
  }
}

static void menu_select_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data)
{
  assert(data);
  struct Settings_view* settings_view = (struct Settings_view*) data;

  Settings_field_t settings_field = get_settings_field(cell_index->row);

  // Cancel any vibrations if the current vibration style is one of the
  // extended vibrations.
  if (settings_field == SETTINGS_FIELD_VIBRATE_STYLE) {
    cancel_long_vibes(settings_view);
  }

  // Increment the setting
  controller_edit_settings(settings_view->group_id, settings_field);

  // Vibrate if the vibrate style was changed
  if (settings_field == SETTINGS_FIELD_VIBRATE_STYLE) {
    Vibrate_style_t vibrate_style =
      settings_view_data_get_group_vibrate_style(
        settings_view->group_id,
        settings_view->default_settings_data,
        settings_view->group_settings_data);

    vibrate_manager_vibrate_force(vibrate_style);
  }
}

static void menu_selection_changed_callback(MenuLayer* menu_layer,
  MenuIndex new_index, MenuIndex old_index, void* data)
{
  assert(data);
  struct Settings_view* settings_view = (struct Settings_view*) data;
  cancel_long_vibes(settings_view);
}

static void cancel_long_vibes(const struct Settings_view* settings_view)
{
  assert(settings_view);
  Vibrate_style_t vibrate_style =
    settings_view_data_get_group_vibrate_style(
      settings_view->group_id,
      settings_view->default_settings_data,
      settings_view->group_settings_data);
  vibrate_manager_cancel_long_vibes(vibrate_style);
}

static const char* get_settings_field_text(Settings_field_t settings_field)
{
  switch (settings_field) {
    case SETTINGS_FIELD_REPEAT_STYLE:
      return LOCALIZE("Repeat style");
    case SETTINGS_FIELD_PROGRESS_STYLE:
      return LOCALIZE("Progress style");
    case SETTINGS_FIELD_VIBRATE_INTERVAL:
      return LOCALIZE("Vibe interval");
    case SETTINGS_FIELD_VIBRATE_STYLE:
      return LOCALIZE("Vibe style");
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid settings field: %d",
        settings_field);
      return "";
  }
}

static const char* get_repeat_style_text(Repeat_style_t repeat_style)
{
  switch (repeat_style) {
    case REPEAT_STYLE_USE_APP_SETTINGS:
      return get_use_default_text();
    case REPEAT_STYLE_NONE:
      return LOCALIZE("Repeat none");
    case REPEAT_STYLE_SINGLE:
      return LOCALIZE("Repeat single");
    case REPEAT_STYLE_GROUP:
      return LOCALIZE("Repeat group");
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid repeat_style: %d", repeat_style);
      return "";
  }
}

static const char* get_progress_style_text(Progress_style_t progress_style)
{
  switch (progress_style) {
    case PROGRESS_STYLE_USE_APP_SETTINGS:
      return get_use_default_text();
    case PROGRESS_STYLE_AUTO:
      return LOCALIZE("Auto start next");
    case PROGRESS_STYLE_WAIT_FOR_USER:
      return LOCALIZE("Wait for user");
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid progress_style: %d",
        progress_style);
      return "";
  }
}

static const char* get_vibrate_interval_text(
  Vibrate_interval_t vibrate_interval)
{
  // TODO: Improve these strings so they dynamically use the actual interval
  // instead of hard-coding the values here.
  switch (vibrate_interval) {
    case VIBRATE_INTERVAL_USE_APP_SETTINGS:
      return get_use_default_text();
    case VIBRATE_INTERVAL_SHORT:
      return LOCALIZE("Short - 5s");
    case VIBRATE_INTERVAL_MEDIUM:
      return LOCALIZE("Medium - 1m");
    case VIBRATE_INTERVAL_LONG:
      return LOCALIZE("Long - 5m");
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid vibrate_interval: %d",
        vibrate_interval);
      return "";
  }
}

static const char* get_vibrate_style_text(Vibrate_style_t vibrate_style)
{
  switch (vibrate_style) {
    case VIBRATE_STYLE_USE_APP_SETTINGS:
      return get_use_default_text();
    case VIBRATE_STYLE_SHORT_PULSE:
      return LOCALIZE("Short pulse");
    case VIBRATE_STYLE_LONG_PULSE:
      return LOCALIZE("Long pulse");
    case VIBRATE_STYLE_DOUBLE_PULSE:
      return LOCALIZE("Double pulse");
    case VIBRATE_STYLE_EXTENDED_0:
      return LOCALIZE("5 seconds");
    case VIBRATE_STYLE_EXTENDED_1:
      return LOCALIZE("10 seconds");
    case VIBRATE_STYLE_EXTENDED_2:
      return LOCALIZE("20 seconds");
    case VIBRATE_STYLE_EXTENDED_3:
      return LOCALIZE("30 seconds");
    case VIBRATE_STYLE_EXTENDED_4:
      return LOCALIZE("1 minute");
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid vibrate style: %d", vibrate_style);
      return "";
  }
}

static const char* get_use_default_text() {
  return LOCALIZE("Use default");
}

/** @} // Settings_view */
