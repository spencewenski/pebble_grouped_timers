/**
 * @internal
 * @addtogroup Settings
 * @{
 */

#include "Settings.h"
#include "Utility.h"
#include "assert.h"
#include "Model.h"
#include "persist_utility.h"
#include "Settings_fields.h"
#include "Modified_state.h"
#include "Advanced_settings_fields.h"
#include "Advanced_settings.h"

#include <pebble.h>

/**
 * Default {@link Repeat_style_t} for the app {@link Settings}.
 */
#define DEFAULT_APP_REPEAT_STYLE REPEAT_STYLE_NONE

/**
 * Default {@link Progress_style_t} for the app {@link Settings}.
 */
#define DEFAULT_APP_PROGRESS_STYLE PROGRESS_STYLE_WAIT_FOR_USER

/**
 * Default {@link Vibrate_interval_t} for the app {@link Settings}.
 */
#define DEFAULT_APP_VIBRATE_INTERVAL VIBRATE_INTERVAL_SHORT

/**
 * Default {@link Vibrate_style_t} for the app {@link Settings}.
 */
#define DEFAULT_APP_VIBRATE_STYLE VIBRATE_STYLE_SHORT_PULSE

/**
 * Default {@link Repeat_style_t} for a {@link Group Group's} {@link Settings}.
 */
#define DEFAULT_GROUP_REPEAT_STYLE REPEAT_STYLE_USE_APP_SETTINGS

/**
 * Default {@link Progress_style_t} for a {@link Group Group's} {@link Settings}.
 */
#define DEFAULT_GROUP_PROGRESS_STYLE PROGRESS_STYLE_USE_APP_SETTINGS

/**
 * Default {@link Vibrate_interval_t} for a {@link Group Group's}
 * {@link Settings}.
 */
#define DEFAULT_GROUP_VIBRATE_INTERVAL VIBRATE_INTERVAL_USE_APP_SETTINGS

/**
 * Default {@link Vibrate_style_t} for a {@link Group Group's} {@link Settings}.
 */
#define DEFAULT_GROUP_VIBRATE_STYLE VIBRATE_STYLE_USE_APP_SETTINGS

/**
 * Set the {@link Repeat_style_t} for the {@link Settings}.
 *
 * @param settings     {@link Settings} to set the {@link Repeat_style_t} for.
 * @param repeat_style {@link Repeat_style_t} to set.
 */
static void settings_set_repeat_style(struct Settings* settings,
  Repeat_style_t repeat_style);

/**
 * Set the {@link Progress_style_t} for the {@link Settings}.
 *
 * @param settings        {@link Settings} to set the {@link Progress_style_t}
 *                        for.
 * @param progress_style  {@link Progress_style_t} to set.
 */
static void settings_set_progress_style(struct Settings* settings,
  Progress_style_t progress_style);

/**
 * Set the {@link Vibrate_interval_t} for the {@link Settings}.
 *
 * @param settings          {@link Settings} to set the {@link Vibrate_interval_t}
 *                          for.
 * @param vibrate_interval  {@link Vibrate_interval_t} to set.
 */
static void settings_set_vibrate_interval(struct Settings* settings,
  Vibrate_interval_t vibrate_interval);

/**
 * Set the {@link Vibrate_style_t} for the {@link Settings}.
 *
 * @param settings      {@link Settings} to set the {@link Vibrate_style_t} for.
 * @param vibrate_style {@link Vibrate_style_t} to set.
 */
static void settings_set_vibrate_style(struct Settings* settings,
  Vibrate_style_t vibrate_style);

/**
 * Get the next {@link Repeat_style_t} that comes after the given
 * {@link Repeat_style_t}.
 *
 * The next {@link Repeat_style_t} depends on if the {@link Settings} are for the
 * app or a {@link Group}.
 *
 * @param  group_id     Id of the {@link Group} the {@link Settings} are for,
 *                      or negative if the {@link Settings} are for the app.
 * @param  repeat_style {@link Repeat_style_t} to get the 'next' of.
 * @return              The next {@link Repeat_style_t} that comes after the given
 *                      {@link Repeat_style_t}.
 */
static Repeat_style_t get_next_repeat_style(int group_id,
  Repeat_style_t repeat_style);

/**
 * Get the next {@link Progress_style_t} that comes after the given
 * {@link Progress_style_t}.
 *
 * The next {@link Progress_style_t} depends on if the {@link Settings} are for
 * the app or a {@link Group}.
 *
 * @param  group_id       Id of the {@link Group} the {@link Settings} are for,
 *                        or negative if the {@link Settings} are for the app.
 * @param  progress_style {@link Progress_style_t} to get the 'next' of.
 * @return                The next {@link Progress_style_t} that comes after the
 *                        given {@link Progress_style_t}.
 */
static Progress_style_t get_next_progress_style(int group_id,
  Progress_style_t progress_style);

/**
 * Get the next {@link Vibrate_interval_t} that comes after the given
 * {@link Vibrate_interval_t}.
 *
 * The next {@link Vibrate_interval_t} depends on if the {@link Settings} are
 * for the app or a {@link Group}.
 *
 * @param  group_id         Id of the {@link Group} the {@link Settings} are
 *                          for, or negative if the {@link Settings} are for
 *                          the app.
 * @param  vibrate_interval {@link Vibrate_interval_t} to get the 'next' of.
 * @return                  The next {@link Vibrate_interval_t} that comes after
 *                          the given {@link Vibrate_interval_t}.
 */
static Vibrate_interval_t get_next_vibrate_interval(int group_id,
  Vibrate_interval_t vibrate_interval);

/**
 * Get the next {@link Vibrate_style_t} that comes after the given
 * {@link Vibrate_style_t}.
 *
 * The next {@link Vibrate_style_t} depends on if the {@link Settings} are for the
 * app or a {@link Group}.
 *
 * @param  group_id       Id of the {@link Group} the {@link Settings} are for,
 *                        or negative if the {@link Settings} are for the app.
 * @param  vibrate_style  {@link Vibrate_style_t} to get the 'next' of.
 * @return                The next {@link Vibrate_style_t} that comes after the
 *                        given {@link Vibrate_style_t}.
 */
static Vibrate_style_t get_next_vibrate_style(int group_id,
  Vibrate_style_t vibrate_style);

/**
 * Get the next {@link Vibrate_style_t} depending on whether or not extended
 * vibes are enabled.
 *
 * @param  group_id                 Id of the {@link Group} the {@link Settings}
 *                                  are for, or negative if the {@link Settings}
 *                                  are for the app.
 * @param next_extended_vibe_style  The extended {@link Vibrate_style_t} to use if
 *                                  extended vibes are enabled.
 * @return                          The given {@link Vibrate_style_t} if extended
 *                                  vibes are enabled, or the first
 *                                  {@link Vibrate_style_t} for this group
 *                                  otherwise.
 */
static Vibrate_style_t get_next_vibrate_style_extended(int group_id,
  Vibrate_style_t next_extended_vibe_style);

/**
 * Get the first vibrate style. Either {@link VIBRATE_STYLE_SHORT_PULSE} if
 * the group id is for the app settings, or
 * {@link VIBRATE_STYLE_USE_APP_SETTINGS} if the group id is for a group.
 *
 * @param  group_id Id of the {@link Group} the {@link Settings} are for,
 *                  or negative if the {@link Settings} are for the app.
 * @return          Either {@link VIBRATE_STYLE_SHORT_PULSE} or
 *                  {@link VIBRATE_STYLE_USE_APP_SETTINGS} depending on the
 *                  given group id.
 */
static Vibrate_style_t get_first_vibrate_style(int group_id);

/**
 * Data required for the {@link Settings}.
 */
struct Settings {
  /**
   * Id of the {@link Group} the {@link Settings} are for, or negative if the
   * {@link Settings} are for the app.
   */
  int group_id;
  /**
   * Persist key that was used to load the {@link Settings}, or
   * {@link INVALID_PERSIST_KEY} if the {@link Settings} has never been loaded.
   */
  int persist_key;
  /**
   * {@link Repeat_style_t} for the {@link Settings}.
   */
  Repeat_style_t repeat_style;
  /**
   * {@link Progress_style_t} for the {@link Settings}.
   */
  Progress_style_t progress_style;
  /**
   * {@link Vibrate_interval_t} for the {@link Settings}.
   */
  Vibrate_interval_t vibrate_interval;
  /**
   * {@link Vibrate_style_t} for the {@link Settings}.
   */
  Vibrate_style_t vibrate_style;
  /**
   * The modified state of these {@link Settings}.
   */
  Modified_state_t modified_state;
};

/**
 * Data required to save a {@link Settings} object to persistent storage.
 */
struct Settings_save_data {
  /**
   * {@link Repeat_style_t} for the {@link Settings}.
   */
  Repeat_style_t repeat_style;
  /**
   * {@link Progress_style_t} for the {@link Settings}.
   */
  Progress_style_t progress_style;
  /**
   * {@link Vibrate_interval_t} for the {@link Settings}.
   */
  Vibrate_interval_t vibrate_interval;
  /**
   * {@link Vibrate_style_t} for the {@link Settings}.
   */
  Vibrate_style_t vibrate_style;
};

struct Settings* settings_create(int group_id)
{
  struct Settings* settings = safe_alloc(sizeof(struct Settings));
  settings->group_id = group_id;
  settings->persist_key = INVALID_PERSIST_KEY;
  // The settings haven't been loaded, assume they need to be saved.
  settings->modified_state = MODIFIED_STATE_SINGLE;
  if (group_id < 0) {
    // Negative group id means these are app settings
    settings->repeat_style = DEFAULT_APP_REPEAT_STYLE;
    settings->progress_style = DEFAULT_APP_PROGRESS_STYLE;
    settings->vibrate_interval = DEFAULT_APP_VIBRATE_INTERVAL;
    settings->vibrate_style = DEFAULT_APP_VIBRATE_STYLE;
  } else {
    // Otherwise, these are group settings
    settings->repeat_style = DEFAULT_GROUP_REPEAT_STYLE;
    settings->progress_style = DEFAULT_GROUP_PROGRESS_STYLE;
    settings->vibrate_interval = DEFAULT_GROUP_VIBRATE_INTERVAL;
    settings->vibrate_style = DEFAULT_GROUP_VIBRATE_STYLE;
  }
  return settings;
}

void settings_destroy(struct Settings* settings)
{
  free(settings);
}

struct Settings* settings_load(int group_id)
{
  struct Settings_save_data settings_save_data;
  int persist_key = persist_load(&settings_save_data,
    sizeof(struct Settings_save_data));
  struct Settings* settings = settings_create(group_id);
  settings->persist_key = persist_key;
  settings->repeat_style = settings_save_data.repeat_style;
  settings->progress_style = settings_save_data.progress_style;
  settings->vibrate_interval = settings_save_data.vibrate_interval;
  settings->vibrate_style = settings_save_data.vibrate_style;
  // The settings have been loaded, they don't need to be saved until they
  // are modified.
  settings->modified_state = MODIFIED_STATE_NOT_MODIFIED;
  return settings;
}

void settings_save(const struct Settings* settings)
{
  assert(settings);
  struct Settings_save_data settings_save_data;
  settings_save_data.repeat_style = settings->repeat_style;
  settings_save_data.progress_style = settings->progress_style;
  settings_save_data.vibrate_interval = settings->vibrate_interval;
  settings_save_data.vibrate_style = settings->vibrate_style;
  persist_save(&settings_save_data, sizeof(struct Settings_save_data),
    settings->modified_state);
}

void settings_persist_delete(const struct Settings* settings)
{
  assert(settings);
  if (settings->persist_key < 0) {
    return;
  }
  persist_delete(settings->persist_key);
}

void settings_broadcast_state(const struct Settings* settings)
{
  assert(settings);
  model_notify_settings(settings->group_id, settings->repeat_style,
    settings->progress_style, settings->vibrate_interval,
    settings->vibrate_style);
}

static void settings_set_repeat_style(struct Settings* settings,
  Repeat_style_t repeat_style)
{
  assert(settings);
  settings->repeat_style = repeat_style;
  settings->modified_state = MODIFIED_STATE_SINGLE;
  model_notify_settings(settings->group_id, settings->repeat_style,
    settings->progress_style, settings->vibrate_interval,
    settings->vibrate_style);
}

Repeat_style_t settings_get_repeat_style(const struct Settings* settings)
{
  assert(settings);
  if (settings->repeat_style == REPEAT_STYLE_USE_APP_SETTINGS) {
    assert(settings->group_id >= 0);
    const struct Settings* app_settings = model_get_settings();
    assert(app_settings);
    return app_settings->repeat_style;
  }
  return settings->repeat_style;
}

static void settings_set_progress_style(struct Settings* settings,
  Progress_style_t progress_style)
{
  assert(settings);
  settings->progress_style = progress_style;
  settings->modified_state = MODIFIED_STATE_SINGLE;
  model_notify_settings(settings->group_id, settings->repeat_style,
    settings->progress_style, settings->vibrate_interval,
    settings->vibrate_style);
}

Progress_style_t settings_get_progress_style(const struct Settings* settings)
{
  assert(settings);
  if (settings->progress_style == PROGRESS_STYLE_USE_APP_SETTINGS) {
    assert(settings->group_id >= 0);
    const struct Settings* app_settings = model_get_settings();
    assert(app_settings);
    return app_settings->progress_style;
  }
  return settings->progress_style;
}

static void settings_set_vibrate_interval(struct Settings* settings,
  Vibrate_interval_t vibrate_interval)
{
  assert(settings);
  settings->vibrate_interval = vibrate_interval;
  settings->modified_state = MODIFIED_STATE_SINGLE;
  model_notify_settings(settings->group_id, settings->repeat_style,
    settings->progress_style, settings->vibrate_interval,
    settings->vibrate_style);
}

Vibrate_interval_t settings_get_vibrate_interval(
  const struct Settings* settings)
{
  assert(settings);
  if (settings->vibrate_interval == VIBRATE_INTERVAL_USE_APP_SETTINGS) {
    assert(settings->group_id >= 0);
    const struct Settings* app_settings = model_get_settings();
    assert(app_settings);
    return app_settings->vibrate_interval;
  }
  return settings->vibrate_interval;
}

static void settings_set_vibrate_style(struct Settings* settings,
  Vibrate_style_t vibrate_style)
{
  assert(settings);
  settings->vibrate_style = vibrate_style;
  settings->modified_state = MODIFIED_STATE_SINGLE;
  model_notify_settings(settings->group_id, settings->repeat_style,
    settings->progress_style, settings->vibrate_interval,
    settings->vibrate_style);
}

Vibrate_style_t settings_get_vibrate_style(const struct Settings* settings)
{
  assert(settings);
  if (settings->vibrate_style == VIBRATE_STYLE_USE_APP_SETTINGS) {
    assert(settings->group_id >= 0);
    const struct Settings* app_settings = model_get_settings();
    assert(app_settings);
    return app_settings->vibrate_style;
  }
  return settings->vibrate_style;
}

void settings_increment_field(struct Settings* settings,
  Settings_field_t settings_field)
{
  assert(settings);
  switch (settings_field) {
    case SETTINGS_FIELD_REPEAT_STYLE:
      settings_set_repeat_style(settings,
        get_next_repeat_style(settings->group_id, settings->repeat_style));
      break;
    case SETTINGS_FIELD_PROGRESS_STYLE:
      settings_set_progress_style(settings,
        get_next_progress_style(settings->group_id, settings->progress_style));
      break;
    case SETTINGS_FIELD_VIBRATE_INTERVAL:
      settings_set_vibrate_interval(settings,
        get_next_vibrate_interval(settings->group_id,
          settings->vibrate_interval));
      break;
    case SETTINGS_FIELD_VIBRATE_STYLE:
      settings_set_vibrate_style(settings,
        get_next_vibrate_style(settings->group_id, settings->vibrate_style));
      break;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid settings field.");
  }
}

static Repeat_style_t get_next_repeat_style(int group_id,
  Repeat_style_t repeat_style)
{
  switch (repeat_style) {
    case REPEAT_STYLE_USE_APP_SETTINGS:
      return REPEAT_STYLE_NONE;
    case REPEAT_STYLE_NONE:
      return REPEAT_STYLE_SINGLE;
    case REPEAT_STYLE_SINGLE:
      return REPEAT_STYLE_GROUP;
    case REPEAT_STYLE_GROUP:
      if (group_id < 0) {
        return REPEAT_STYLE_NONE;
      } else {
        return REPEAT_STYLE_USE_APP_SETTINGS;
      }
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid repeat style: %d", repeat_style);
      return REPEAT_STYLE_NONE;
  }
}

static Progress_style_t get_next_progress_style(int group_id,
  Progress_style_t progress_style)
{
  switch (progress_style) {
    case PROGRESS_STYLE_USE_APP_SETTINGS:
      return PROGRESS_STYLE_WAIT_FOR_USER;
    case PROGRESS_STYLE_WAIT_FOR_USER:
        return PROGRESS_STYLE_AUTO;
    case PROGRESS_STYLE_AUTO:
      if (group_id < 0) {
      return PROGRESS_STYLE_WAIT_FOR_USER;
      } else {
        return PROGRESS_STYLE_USE_APP_SETTINGS;
      }
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR,
        "Invalid progress style: %d", progress_style);
      return PROGRESS_STYLE_AUTO;
  }
}

static Vibrate_interval_t get_next_vibrate_interval(int group_id,
  Vibrate_interval_t vibrate_interval)
{
  switch (vibrate_interval) {
    case VIBRATE_INTERVAL_USE_APP_SETTINGS:
      return VIBRATE_INTERVAL_SHORT;
    case VIBRATE_INTERVAL_SHORT:
      return VIBRATE_INTERVAL_MEDIUM;
    case VIBRATE_INTERVAL_MEDIUM:
      return VIBRATE_INTERVAL_LONG;
    case VIBRATE_INTERVAL_LONG:
      if (group_id < 0) {
        return VIBRATE_INTERVAL_SHORT;
      } else {
        return VIBRATE_INTERVAL_USE_APP_SETTINGS;
      }
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR,
        "Invalid vibrate interval: %d", vibrate_interval);
      return VIBRATE_INTERVAL_SHORT;
  }
}

static Vibrate_style_t get_next_vibrate_style(int group_id,
  Vibrate_style_t vibrate_style)
{
  switch (vibrate_style) {
    case VIBRATE_STYLE_USE_APP_SETTINGS:
      return VIBRATE_STYLE_SHORT_PULSE;
    case VIBRATE_STYLE_SHORT_PULSE:
      return VIBRATE_STYLE_LONG_PULSE;
    case VIBRATE_STYLE_LONG_PULSE:
      return VIBRATE_STYLE_DOUBLE_PULSE;
    case VIBRATE_STYLE_DOUBLE_PULSE:
      return get_next_vibrate_style_extended(group_id,
        VIBRATE_STYLE_EXTENDED_0);
    case VIBRATE_STYLE_EXTENDED_0:
      return get_next_vibrate_style_extended(group_id,
        VIBRATE_STYLE_EXTENDED_1);
    case VIBRATE_STYLE_EXTENDED_1:
      return get_next_vibrate_style_extended(group_id,
        VIBRATE_STYLE_EXTENDED_2);
    case VIBRATE_STYLE_EXTENDED_2:
      return get_next_vibrate_style_extended(group_id,
        VIBRATE_STYLE_EXTENDED_3);
    case VIBRATE_STYLE_EXTENDED_3:
      return get_next_vibrate_style_extended(group_id,
        VIBRATE_STYLE_EXTENDED_4);
    case VIBRATE_STYLE_EXTENDED_4:
      // This is the last extended style, return the first vibrate style
      // regardles of whether extended vibes are enabled.
      return get_first_vibrate_style(group_id);
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid vibrate style: %d", vibrate_style);
      return VIBRATE_STYLE_SHORT_PULSE;
  }
}

static Vibrate_style_t get_next_vibrate_style_extended(int group_id,
  Vibrate_style_t next_extended_vibe_style)
{
  int extended_vibes_enabled = advanced_settings_get_extended_vibes_state(
    model_get_advanced_settings()) == EXTENDED_VIBES_ENABLED ? 0 : 1;
  if (extended_vibes_enabled == 0) {
    // Extended vibes enabled, return the next extended vibe style
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Extended vibes enabled, returning next extended style");
    return next_extended_vibe_style;
  }
  // Extended vibes disabled, return the first vibe style
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Extended vibes disabled, returning first vibe style");
  return get_first_vibrate_style(group_id);
}

static Vibrate_style_t get_first_vibrate_style(int group_id)
{
  if (group_id < 0) {
    return VIBRATE_STYLE_SHORT_PULSE;
  } else {
    return VIBRATE_STYLE_USE_APP_SETTINGS;
  }
}

/** @} // Settings */
