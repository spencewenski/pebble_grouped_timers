#ifndef TIMER_UTILITY_H
#define TIMER_UTILITY_H

/**
 * @addtogroup timer_utility Timer utility
 * @{
 *
 * Utility methods for the {@link Timer}.
 */

#include "Timer_field.h"

/**
 * Convert the given value from {@link Timer_field_t} into
 * {@link TIMER_FIELD_SECONDS seconds}.
 *
 * @param  field {@link Timer_field_t} of the given value
 * @param  value Value of the {@link Timer_field_t} to get as seconds.
 * @return       The given value converted to
 *               {@link TIMER_FIELD_SECONDS seconds}.
 */
int timer_convert_field_to_seconds(Timer_field_t field, int value);

/**
 * Get the {@link Timer_field_t} from the given number of
 * {@link TIMER_FIELD_SECONDS seconds}.
 *
 * E.g., if given 121 seconds, will return 2 for {@link TIMER_FIELD_MINUTES} and
 * 1 for {@link TIMER_FIELD_SECONDS}.
 *
 * @param  field   {@link Timer_field_t} to get from the given number of
 *                 {@link TIMER_FIELD_SECONDS seconds}.
 * @param  seconds The number of {@link TIMER_FIELD_SECONDS seconds} to use to
 *                 get the {@link Timer_field_t}.
 * @return         The {@link Timer_field_t} from the given number of
 *                 {@link TIMER_FIELD_SECONDS seconds}.
 */
int timer_get_field_from_seconds(Timer_field_t field, int seconds);

/** @} // timer_utility */

#endif /*TIMER_UTILITY_H*/
