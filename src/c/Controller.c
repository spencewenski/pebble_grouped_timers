/**
 * @internal
 * @addtogroup Controller
 * @{
 */

#include "Controller.h"
#include "List.h"
#include "Utility.h"
#include "assert.h"
#include "Model.h"
#include "view_factory.h"
#include "View.h"
#include "Group.h"
#include "Timer.h"
#include "timer_utility.h"
#include "Settings.h"
#include "Settings_fields.h"
#include "locale_framework/src/localize.h"
#include "Advanced_settings.h"

#include <pebble.h>

/**
 * Data required for the {@link Controller} to track the {@link View View's}
 * that have been opened.
 */
struct View_data {
  /**
   * {@link View} this {@link View_data} represents.
   */
  struct View* view;
  /**
   * The {@link View_type} of the {@link View}.
   */
  View_type_t view_type;
};

/**
 * Create a {@link View_data} object.
 *
 * @param  view      {@link View} the {@link View_data} represents.
 * @param  view_type The {@link View_type} of the {@link View}.
 * @return           A new {@link View_data} object.
 */
static struct View_data* view_data_create(struct View* view,
  View_type_t view_type);

/**
 * Destroy the {@link View_data}.
 *
 * @param view_data {@link View_data} to destroy.
 */
static void view_data_destroy(struct View_data* view_data);

/**
 * Compare a {@link View} object to a {@link View_data} object.
 *
 * Compares the {@link View} to the {@link View} pointer in {@link View_data}.
 *
 * @param  view      {@link View} to compare to the {@link View} in the
 *                   {@link View_data}.
 * @param  view_data {@link View_data} to use to compare to the {@link View}.
 * @return           Zero if the {@link View Views} are equal, positive if the
 *                   given {@link View} is 'greater' than the one in the
 *                   {@link View_data}, negative otherwise.
 */
static int view_data_compare_view_ptr(struct View* view,
  struct View_data* view_data);

/**
 * Compare a {@link View_type} to a {@link View_data} object.
 *
 * Compares the given {@link View_type} to the {@link View_type} in
 * {@link View_data}.
 *
 * @param  view_type {@link View_type} to compare to the {@link View_type} in
 *                   the {@link View_data}.
 * @param  view_data {@link View_data} to use to compare to the
 *                   {@link View_type}.
 * @return           Zero if the {@link View_type View_types} are equal,
 *                   positive if the given {@link View_type} is 'greater' than
 *                   the one in the {@link View_data}, negative otherwise.
 */
static int view_data_compare_type(View_type_t* view_type,
  struct View_data* view_data);

/**
 * Data used by the {@link Controller}.
 */
struct Controller {
  /**
   * {@link List} of {@link View_data} to track the {@link View Views} that
   * are currently open.
   */
  struct List* views;
};
/**
 * The singleton {@link Controller} instance.
 */
static struct Controller* s_controller = NULL;

/**
 * Create a {@link Controller} object.
 *
 * @return A new {@link Controller} object.
 */
static struct Controller* controller_create();

/**
 * Destroy the {@link Controller} object.
 *
 * @param controller {@link Controller} object to destroy.
 */
static void controller_destroy(struct Controller* controller);

/**
 * Helper method for opening {@link View Views} and preventing multiple of a
 * single {@link View_type} from being opened at the same time.
 *
 * @param view_type {@link View_type} of the {@link View} to open.
 * @param group_id  Id of the {@link Group} the {@link View} represents, or
 *                  negative if the {@link View} doesn't represent a specific
 *                  {@link Group}.
 * @param timer_id  If of the {@link Timer} the {@link View} represents, or
 *                  negative if the {@link View} doesn't represent a specific
 *                  {@link Timer}.
 */
static void controller_open_view(View_type_t view_type, int group_id,
  int timer_id);

void controller_app_open()
{
  assert(!s_controller);
  locale_init();
  s_controller = controller_create();
  model_start();
  controller_open_main_view();
}

void controller_app_close()
{
  assert(s_controller);
  window_stack_pop_all(false);
  model_stop();
  controller_destroy(s_controller);
  s_controller = NULL;
  local_deinit();
}

int controller_create_group()
{
  assert(s_controller);
  int group_id = model_get_next_group_id();
  struct Group* group = group_create(group_id);
  model_add_group(group);
  return group_id;
}

void controller_delete_group(int group_id)
{
  assert(s_controller);
  struct Group* group = model_get_group(group_id);
  model_remove_group(group);
  group_persist_delete(group);
  group_destroy(group);
}

int controller_create_timer(int group_id)
{
  assert(s_controller);
  int timer_id = model_get_next_timer_id();
  struct Timer* timer = timer_create(timer_id);
  model_add_timer(timer, group_id);
  return timer_id;
}

void controller_delete_timer(int timer_id)
{
  assert(s_controller);
  struct Timer* timer = model_get_timer(timer_id);
  model_remove_timer(timer);
  timer_persist_delete(timer);
  timer_destroy(timer);
}

void controller_edit_timer(int timer_id, Timer_field_t field, int amount)
{
  assert(s_controller);
  struct Timer* timer = model_get_timer(timer_id);
  timer_increment_field(timer, field, amount);
}

void controller_start_timer(int timer_id)
{
  assert(s_controller);
  struct Timer* timer = model_get_timer(timer_id);
  timer_start(timer);
}

void controller_pause_timer(int timer_id)
{
  assert(s_controller);
  struct Timer* timer = model_get_timer(timer_id);
  timer_pause(timer);
}

void controller_reset_timer(int timer_id)
{
  assert(s_controller);
  struct Timer* timer = model_get_timer(timer_id);
  timer_reset(timer);
}

void controller_next_timer(int group_id, int timer_id)
{
  assert(s_controller);
  controller_reset_timer(timer_id);
  struct Timer* next_timer = model_get_next_timer(timer_id);
  struct Timer* original_next_timer = next_timer;
  if (!next_timer) {
    // If the next timer is NULL, get the first timer in the group.
    struct Group* group = model_get_group(group_id);
    next_timer = group_get_timer(group, 0);
    if (!next_timer) {
      // If there are no timers in the group, just return.
      return;
    }
  }

  int next_timer_id = timer_get_id(next_timer);
  if (original_next_timer) {
    // Only start the next timer if it wasn't NULL before we got the first
    // timer in the group.
    controller_start_timer(next_timer_id);
  }
  model_notify_timer_tracking(next_timer_id, timer_id);
  // Make sure the views have the most up to date data for the timer
  timer_broadcast_state(next_timer);
}

void controller_edit_settings(int group_id, Settings_field_t settings_field)
{
  assert(s_controller);
  struct Settings* settings = NULL;
  // Get either the app settings or the group settings.
  if (group_id < 0) {
    settings = model_get_settings();
  } else {
    struct Group* group = model_get_group(group_id);
    settings = group_get_settings(group);
  }
  assert(settings);
  settings_increment_field(settings, settings_field);
}

void controller_edit_advanced_settings(
  Advanced_settings_field_t settings_field)
{
  assert(s_controller);
  struct Advanced_settings* advanced_settings = model_get_advanced_settings();
  assert(advanced_settings);
  advanced_settings_increment_field(advanced_settings, settings_field);
}

void controller_open_main_view()
{
  assert(s_controller);
  controller_open_view(VIEW_TYPE_MAIN_VIEW, -1, -1);
}

void controller_open_group_view(int group_id)
{
  assert(s_controller);
  controller_open_view(VIEW_TYPE_GROUP_VIEW, group_id, -1);
}

void controller_open_edit_view(int timer_id)
{
  assert(s_controller);
  controller_reset_timer(timer_id);
  controller_open_view(VIEW_TYPE_EDIT_VIEW, -1, timer_id);
}

void controller_open_countdown_view(int group_id, int timer_id)
{
  assert(s_controller);
  controller_open_view(VIEW_TYPE_COUNTDOWN_VIEW, group_id, timer_id);
  // Make sure the countdown view is tracking the timer and that the view
  // has the data for the timer
  model_notify_timer_tracking(timer_id, -1);
  timer_broadcast_state(model_get_timer(timer_id));
}

void controller_open_settings_view(int group_id)
{
  assert(s_controller);
  controller_open_view(VIEW_TYPE_SETTINGS_VIEW, group_id, -1);
}

void controller_open_app_info_view()
{
  assert(s_controller);
  controller_open_view(VIEW_TYPE_APP_INFO_VIEW, -1, -1);
}

void controller_open_advanced_settings_view()
{
  assert(s_controller);
  controller_open_view(VIEW_TYPE_ADVANCED_SETTINGS_VIEW, -1, -1);
}

void controller_open_timer_options_view(int timer_id)
{
  assert(s_controller);
  controller_open_view(VIEW_TYPE_TIMER_OPTIONS_VIEW, -1, timer_id);
}

static void controller_open_view(View_type_t view_type, int group_id,
  int timer_id)
{
  assert(s_controller);
  // Check that this type of view hasn't already been created.
  if (list_find(s_controller->views, &view_type,
      (List_compare_fp_t)view_data_compare_type)) {
    return;
  }
  // Make sure that the main view is opened before any other view
  if (view_type != VIEW_TYPE_MAIN_VIEW) {
    controller_open_main_view();
  }
  struct View* view = view_factory(view_type, group_id, timer_id);
  if (!view) {
    return;
  }
  APP_LOG(APP_LOG_LEVEL_DEBUG, "controller_open_view: %d", view_type);
  model_attach_view(view);
  struct View_data* view_data = view_data_create(view, view_type);
  list_add(s_controller->views, view_data);
  view_push(view);
}

void controller_on_view_closed(struct View* view)
{
  assert(s_controller);
  model_detach_view(view);
  struct View_data* view_data = list_find_arg(s_controller->views, view,
    (List_compare_arg_fp_t)view_data_compare_view_ptr);
  list_remove_ptr(s_controller->views, view_data);
  view_data_destroy(view_data);
  view_destroy(view);
}

static struct Controller* controller_create()
{
  struct Controller* controller =
    (struct Controller*) safe_alloc(sizeof(struct Controller));
  controller->views = list_create();
  return controller;
}

static void controller_destroy(struct Controller* controller)
{
  assert(controller);
  assert(!list_size(controller->views));
  list_destroy(controller->views);
  free(controller);
}

static struct View_data* view_data_create(struct View* view,
  View_type_t view_type)
{
  struct View_data* view_data =
    (struct View_data*) safe_alloc(sizeof(struct View_data));
  view_data->view = view;
  view_data->view_type = view_type;
  return view_data;
}

static void view_data_destroy(struct View_data* view_data)
{
  free(view_data);
}

static int view_data_compare_view_ptr(struct View* view,
  struct View_data* view_data)
{
  assert(view_data);
  assert(view);
  // There's no concept of 'greater than' or 'less than' for views, so only
  // return 0 or 1.
  return view_data->view == view ? 0 : 1;
}

static int view_data_compare_type(View_type_t* view_type,
  struct View_data* view_data)
{
  assert(view_type);
  assert(view_data);
  // There's no concept of 'greater than' or 'less than' for view types, so only
  // return 0 or 1.
  return *view_type == view_data->view_type ? 0 : 1;
}

/** @} // Controller */
