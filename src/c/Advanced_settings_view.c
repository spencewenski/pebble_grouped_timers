/**
 * @internal
 * @addtogroup Advanced_settings_view
 * @{
 */

#include "Advanced_settings_view.h"
#include "Advanced_settings_fields.h"
#include "View.h"
#include "Utility.h"
#include "Controller.h"
#include "View_interface.h"
#include "locale_framework/src/localize.h"
#include "assert.h"
#include "draw_utility.h"

#include <pebble.h>

/**
 * @internal
 * @addtogroup Advanced_settings_view_interface_impl
 * @{
 *
 * Implementations of the {@link View_interface} for the
 * {@link Advanced_settings_view}.
 */

/** {@link view_push_fp_t} */
static void advanced_settings_view_push(void* data);
/** {@link view_destroy_fp_t} */
static void advanced_settings_view_destroy(void* data);
/** {@link view_update_settings_fp_t} */
static int advanced_settings_view_update_advanced_settings(void* data,
  Extended_vibes_state_t extended_vibes_state);
/** {@link view_refresh_fp_t} */
static void advanced_settings_view_refresh(void* data);

/** @} // Advanced_settings_view_interface_impl */

/**
 * Data used by the {@link Advanced_settings_view}.
 */
struct Advanced_settings_view {
  /**
   * Pebble window for this view.
   */
  Window* window;
  /**
   * Pebble menu layer to display the {@link Advanced_settings} fields.
   */
  MenuLayer* menu_layer;
  /**
   * Pebble status bar layer to display the time at the top of the screen.
   */
  StatusBarLayer* status_bar_layer;
  /**
   * Parent {@link View} object for this {@link Advanced_settings_view}.
   */
  struct View* view;
  /**
   * The {@link Extended_vibes_state_t} setting.
   */
  Extended_vibes_state_t extended_vibes_state;
};

/**
 * Translate the index into a {@link Advanced_settings_field_t}.
 *
 * The index should be a valid index of the {@link Advanced_settings_view} menu
 * layer.
 *
 * @param  settings_field_index Index to translate into a
 *                              {@link Advanced_settings_field_t}.
 * @return                      {@link Advanced_settings_field_t} for the index.
 */
static Advanced_settings_field_t get_advanced_settings_field(
  int settings_field_index);

/**
 * Get the display text for the {@link Advanced_settings_field_t}.
 *
 * @param  settings_field The {@link Advanced_settings_field_t} to get the text
 *                        for.
 * @return                The display text for the
 *                        {@link Advanced_settings_field_t}.
 */
static const char* get_advanced_settings_field_text(
  Advanced_settings_field_t settings_field);

/**
 * Get the display text for the {@link Extended_vibes_state_t}.
 *
 * @param  extended_vibes_state The {@link Extended_vibes_state_t} to get the
 *                              text for.
 * @return                      The display text for the
 *                              {@link Extended_vibes_state_t}.
 */
static const char* get_extended_vibes_state_text(
  Extended_vibes_state_t extended_vibes_state);

// WindowHandlers
static void window_load_handler(Window* window);
static void window_unload_handler(Window* window);

// MenuLayerCallbacks
static uint16_t menu_get_num_sections_callback(MenuLayer* menu_layer,
  void* data);
static uint16_t menu_get_num_rows_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data);
static void menu_draw_row_callback(GContext* ctx, const Layer* cell_layer,
  MenuIndex* cell_index, void* data);
static void menu_draw_header_callback(GContext* ctx, const Layer* cell_layer,
  uint16_t section_index, void* data);
static void menu_select_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data);

struct View* advanced_settings_view_create()
{
  struct Advanced_settings_view* settings_view =
    safe_alloc(sizeof(struct Advanced_settings_view));
  struct View_interface view_interface = (struct View_interface) {
    .push = advanced_settings_view_push,
    .update_advanced_settings = advanced_settings_view_update_advanced_settings,
    .refresh = advanced_settings_view_refresh,
    .destroy = advanced_settings_view_destroy
  };
  settings_view->view = view_create(settings_view, &view_interface);
  return settings_view->view;
}

static void advanced_settings_view_destroy(void* data)
{
  free(data);
}

static void advanced_settings_view_push(void* data)
{
  assert(data);
  struct Advanced_settings_view* settings_view = (struct Advanced_settings_view*) data;

  settings_view->window = window_create();

  assert(settings_view->window);

  window_set_user_data(settings_view->window, settings_view);
  window_set_window_handlers(settings_view->window, (WindowHandlers) {
    .load = window_load_handler,
    .unload = window_unload_handler
  });
  window_stack_push(settings_view->window, false);
}

static int advanced_settings_view_update_advanced_settings(void* data,
  Extended_vibes_state_t extended_vibes_state)
{
  assert(data);
  struct Advanced_settings_view* settings_view = (struct Advanced_settings_view*) data;

  int modified = 0;
  if (settings_view->extended_vibes_state != extended_vibes_state) {
    ++modified;
    settings_view->extended_vibes_state = extended_vibes_state;
  }
  return modified;
}

static void advanced_settings_view_refresh(void* data)
{
  assert(data);
  struct Advanced_settings_view* settings_view = (struct Advanced_settings_view*) data;
  menu_layer_reload_data(settings_view->menu_layer);
}

static void window_load_handler(Window* window)
{
  struct Advanced_settings_view* settings_view = window_get_user_data(window);
  assert(settings_view);

  Layer* window_layer = window_get_root_layer(settings_view->window);

  // Status bar layer
  settings_view->status_bar_layer = status_bar_layer_create();
  layer_add_child(window_layer,
    status_bar_layer_get_layer(settings_view->status_bar_layer));

  // Settings layer
  GRect bounds = layer_get_bounds(window_layer);
  bounds = status_bar_adjust_window_bounds(bounds);
  settings_view->menu_layer = menu_layer_create(bounds);
  assert(settings_view->menu_layer);

  menu_layer_set_callbacks(settings_view->menu_layer, settings_view,
    (MenuLayerCallbacks) {
      .get_num_sections = menu_get_num_sections_callback,
      .get_num_rows = menu_get_num_rows_callback,
      .get_header_height = menu_get_header_height_callback,
      .draw_header = menu_draw_header_callback,
      .get_cell_height = PBL_IF_ROUND_ELSE(menu_cell_get_height_round, NULL),
      .draw_row = menu_draw_row_callback,
      .select_click = menu_select_click_callback
    }
  );
  menu_layer_set_click_config_onto_window(settings_view->menu_layer, window);
  layer_add_child(window_layer,
    menu_layer_get_layer(settings_view->menu_layer));
}

static void window_unload_handler(Window* window)
{
  struct Advanced_settings_view* settings_view = window_get_user_data(window);
  assert(settings_view);

  menu_layer_destroy(settings_view->menu_layer);
  settings_view->menu_layer = NULL;

  status_bar_layer_destroy(settings_view->status_bar_layer);
  settings_view->status_bar_layer = NULL;

  window_destroy(settings_view->window);
  settings_view->window = NULL;

  controller_on_view_closed(settings_view->view);
}

static uint16_t menu_get_num_sections_callback(MenuLayer* menu_layer,
  void* data)
{
  return 1;
}

static uint16_t menu_get_num_rows_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data)
{
  return NUM_ADVANCED_SETTINGS_FIELDS;
}

static void menu_draw_row_callback(GContext* ctx, const Layer* cell_layer,
  MenuIndex* cell_index, void* data)
{
  assert(data);
  struct Advanced_settings_view* settings_view = (struct Advanced_settings_view*) data;

  Advanced_settings_field_t settings_field =
    get_advanced_settings_field(cell_index->row);

  const char* subtitle_text;
  switch (settings_field) {
    case ADVANCED_SETTINGS_FIELD_EXTENDED_VIBES_STATE:
      subtitle_text = get_extended_vibes_state_text(
        settings_view->extended_vibes_state);
      break;
    default:
      subtitle_text = "";
      break;
  }

  menu_cell_basic_draw(ctx, cell_layer,
    get_advanced_settings_field_text(settings_field), subtitle_text, NULL);
}

static void menu_draw_header_callback(GContext* ctx, const Layer* cell_layer,
  uint16_t section_index, void* data)
{
  switch (section_index) {
    case 0:
      menu_cell_draw_header(ctx, cell_layer, LOCALIZE("Advanced"));
      return;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid section index: %d", section_index);
      return;
  }
}

static void menu_select_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data)
{
  Advanced_settings_field_t settings_field =
    get_advanced_settings_field(cell_index->row);
  controller_edit_advanced_settings(settings_field);
}

static Advanced_settings_field_t get_advanced_settings_field(
  int settings_field_index)
{
  switch (settings_field_index) {
    case 0:
      return ADVANCED_SETTINGS_FIELD_EXTENDED_VIBES_STATE;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid settings field index: %d",
        settings_field_index);
      return ADVANCED_SETTINGS_FIELD_EXTENDED_VIBES_STATE;
  }
}

static const char* get_advanced_settings_field_text(
  Advanced_settings_field_t settings_field)
{
  switch (settings_field) {
    case ADVANCED_SETTINGS_FIELD_EXTENDED_VIBES_STATE:
      return LOCALIZE("Extended vibes");
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid settings field: %d",
        settings_field);
      return "";
  }
}

static const char* get_extended_vibes_state_text(
  Extended_vibes_state_t extended_vibes_state)
{
  switch (extended_vibes_state) {
    case EXTENDED_VIBES_DISABLED:
      return LOCALIZE("Disabled");
    case EXTENDED_VIBES_ENABLED:
      return LOCALIZE("Enabled");
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid extended_vibes_state: %d",
        extended_vibes_state);
      return "";
  }
}

/** @} // Advanced_settings_view */
