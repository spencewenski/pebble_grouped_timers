#ifndef TIMER_STATE_H
#define TIMER_STATE_H

/**
 * @addtogroup Timer_state Timer state
 * @{
 *
 * Possible states of a {@link Timer}.
 */

/**
 * Possible states of a {@link Timer}.
 */
typedef enum {
  /**
   * The {@link Timer} has not been started.
   */
  TIMER_STATE_NOT_STARTED,
  /**
   * The {@link Timer} has been started and is currently running.
   */
  TIMER_STATE_STARTED,
  /**
   * The {@link Timer} has been started and is currently paused.
   */
  TIMER_STATE_PAUSED,
  /**
   * The {@link Timer} has completed but is still running.
   */
  TIMER_STATE_ELAPSED
} Timer_state_t;

/** @} // Timer_state */

#endif /*TIMER_STATE_H*/
