#ifndef VIBRATE_MANAGER_H
#define VIBRATE_MANAGER_H

/**
 * @addtogroup Vibrate_manager Vibrate manager
 * @{
 *
 * {@link Vibrate_manager} is a singleton that manages the logic for vibrating
 * and prevents the app from vibrating more than once per second.
 */

#include "Settings_fields.h"

struct Group;
struct Timer;

/**
 * Initialize the {@link Vibrate_manager}.
 *
 * Should be called once when the app is started and before any other
 * {@link Vibrate_manager} methods are called.
 */
void vibrate_manager_init();

/**
 * Tear down the {@link Vibrate_manager}.
 *
 * Should be called once when the app is closing. {@link Vibrate_manager}
 * methods should not be used after this is called.
 */
void vibrate_manager_uninit();

/**
 * Request that the app vibrate immediately.
 *
 * If the app has already vibrated this second, do nothing.
 *
 * @param vibrate_style {@link Vibrate_style_t} of the vibration.
 */
void vibrate_manager_vibrate_immediate(Vibrate_style_t vibrate_style);

/**
 * Force the app to vibrate immediately.
 *
 * Vibrate regardless of whether or not the app already vibrated this second.
 * This should be used sparingly; consider using
 * {@link vibrate_manager_vibrate_immediate} before using this.
 *
 * This will also register that a vibration occured this second, which will
 * affect {@link vibrate_manager_vibrate_immediate}.
 *
 * @param vibrate_style {@link Vibrate_style_t} of the vibration.
 */
void vibrate_manager_vibrate_force(Vibrate_style_t vibrate_style);

/**
 * Use the {@link Group Group's} {@link Settings} and {@link Timer} to determine
 * if the app should vibrate.
 *
 * If the app has already vibrated this second, do nothing. If the app vibrates,
 * open the {@link Countdown_view} for the {@link Timer}.
 *
 * @param group {@link Group} to use to determine if the app should vibrate.
 * @param timer {@link Timer} to use to determine if the app should vibrate.
 */
void vibrate_manager_vibrate(struct Group* group, struct Timer* timer);

/**
 * Cancel vibrations if the provided {@link Vibrate_style_t} is one of the
 * extended vibrate styles, such as {@link VIBRATE_STYLE_EXTENDED_0}.
 *
 * @param vibrate_style The {@link Vibrate_style_t} used to check if we should
 *                      cancel vibes.
 */
void vibrate_manager_cancel_long_vibes(Vibrate_style_t vibrate_style);

/** @} // Vibrate_manager */

#endif /*VIBRATE_MANAGER_H*/
