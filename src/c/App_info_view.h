#ifndef APP_INFO_VIEW_H
#define APP_INFO_VIEW_H

/**
 * @addtogroup App_info_view App info view
 * @{
 *
 * View to display app info.
 */

struct View;

/**
 * Create a {@link App_info_view main view}.
 *
 * @return The abstract {@link View} handle for the {@link App_info_view}.
 */
struct View* app_info_view_create();

/** @} // App_info_view */

#endif /*APP_INFO_VIEW_H*/
