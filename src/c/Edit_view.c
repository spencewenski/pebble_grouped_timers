/**
 * @internal
 * @addtogroup Edit_view
 * @{
 */

#include "Edit_view.h"
#include "View.h"
#include "globals.h"
#include "Controller.h"
#include "timer_utility.h"
#include "Utility.h"
#include "assert.h"
#include "draw_utility.h"
#include "View_interface.h"

#include <pebble.h>

/**
 * Number of ms between clicks of a button that is held down.
 */
#define CLICK_REPEAT_INTERVAL_MS 100

/**
 * Max height of the {@link Timer} edit text.
 */
#define TIMER_TEXT_HEIGHT 50

/**
 * Number of pixels to leave on the sides of the timer text.
 */
#define TIMER_TEXT_LEFT_RIGHT_SPACE 4

/**
 * Amount of space to put between the bottom of the field text and the field
 * indicator as a percentage of the height of the field text frame height
 */
#define INDICATOR_TOP_SPACING_PCT 0.2

/**
 * Percentage of the timer text frame height to use as the height of a
 * rectangular field indicator.
 */
#define RECT_HEIGHT_PCT 0.15

/**
 * Percentage of the timer text frame width to use as the width of a rectangular
 * field indicator.
 */
#define RECT_WIDTH_PCT 0.8

/**
 * Length of the buffer to use to hold the {@link Timer} field text.
 *
 * Really only need three characters, but use four just to have an extra
 * character of buffer just in case.
 */
#define TIMER_FIELD_TEXT_BUFFER_LENGTH 4

/**
 * Default text of a new {@link Timer}.
 *
 * Used to calculate the font to use for the edit text.
 */
static const char * const s_default_timer_str = "00:00:00";

/**
 * @internal
 * @addtogroup Edit_view_interface_impl
 * @{
 *
 * Implementations of the {@link View_interface} for the {@link Edit_view}.
 */

/** {@link view_push_fp_t} */
static void edit_view_push(void* data);
/** {@link view_destroy_fp_t} */
static void edit_view_destroy(void* data);
/** {@link view_update_duration_fp_t} */
static int edit_view_update_duration(void* data, int group_id, int timer_id,
  int duration);
/** {@link view_refresh_fp_t} */
static void edit_view_refresh(void* data);

/** @} // Edit_view_interface_impl */

/**
 * Data used by the {@link Edit_view}.
 */
struct Edit_view {
  /**
   * Pebble window object for this view.
   */
  Window* window;
  /**
   * Pebble status bar layer to display the time at the top of the screen.
   */
  StatusBarLayer* status_bar_layer;
  /**
   * Pebble text layer for the {@link TIMER_FIELD_HOURS}.
   */
  TextLayer* hours_text_layer;
  /**
   * Pebble text layer for the {@link TIMER_FIELD_MINUTES}.
   */
  TextLayer* minutes_text_layer;
  /**
   * Pebble text layer for the {@link TIMER_FIELD_SECONDS}.
   */
  TextLayer* seconds_text_layer;
  /**
   * Pebble text layer for the ":" between the hours and minutes fields.
   */
  TextLayer* colon_text_layer0;
  /**
   * Pebble text layer for the ":" between the minutes and seconds fields.
   */
  TextLayer* colon_text_layer1;
  /**
   * Pebble canvas layer to draw the field indicator.
   */
  Layer* canvas_layer;
  /**
   * Parent {@link View} object for this {@link Countdown_view}.
   */
  struct View* view;
  /**
   * Id of the {@link Timer} that is being edited.
   */
  int timer_id;
  /**
   * The duration of the {@link Timer} to display.
   */
  int duration;
  /**
   * The index of the {@link Timer_field_t} that is currently being edited.
   */
  int edit_field_num;
  /**
   * Buffer to hold the text for the {@link TIMER_FIELD_HOURS}.
   */
  char hours_text_buffer[TIMER_FIELD_TEXT_BUFFER_LENGTH];
  /**
   * Buffer to hold the text for the {@link TIMER_FIELD_MINUTES}.
   */
  char minutes_text_buffer[TIMER_FIELD_TEXT_BUFFER_LENGTH];
  /**
   * Buffer to hold the text for the {@link TIMER_FIELD_SECONDS}.
   */
  char seconds_text_buffer[TIMER_FIELD_TEXT_BUFFER_LENGTH];
};

/**
 * Data used by the pebble canvas layer used to draw the field indicator.
 */
struct Layer_data {
  struct Edit_view* edit_view;
};

/**
 * Update the {@link Edit_view Edit_view's} text to display the current duration
 * of the {@link Timer}.
 *
 * @param edit_view {@link Edit_view} to update the {@link Timer} text of.
 */
static void update_timer_text_layers(struct Edit_view* edit_view);

/**
 * Update the text of the given layer to display the given value.
 *
 * @param text_layer  Pebble text layer to update.
 * @param buffer      Buffer to hold the text for the {@link Timer_field_t}.
 * @param buffer_size Size of the buffer.
 * @param value       Value to display.
 */
static void update_field_text_layer(TextLayer* text_layer, char* buffer,
  int buffer_size, int value);

/**
 * Get the {@link Timer_field_t} that corresponds to the given index.
 *
 * E.g., index 0 is the {@link TIMER_FIELD_HOURS} and index 2 is
 * {@link TIMER_FIELD_SECONDS}.
 *
 * @param  timer_edit_index Index to get the {@link Timer_field_t} for.
 * @return                  The {@link Timer_field_t} that corresponds to the
 *                          given index.
 */
static Timer_field_t get_timer_field(int timer_edit_index);

/**
 * Create a Pebble text layer of the given width that is offset from the left
 * of the parent frame by the given amount.
 *
 * @param  parent_frame Parent frame of the text layer. The text layer uses
 *                      the parent's height and y-origin.
 * @param  width        Width of the text layer.
 * @param  left_offset  Offset from the left side of the parent frame
 * @param  font         Font to use for the text.
 * @return              Pebble text layer of the given width that is offset
 *                      from the left of the parent frame by the given amount.
 */
static TextLayer* create_text_layer(GRect parent_frame, int width,
  int left_offset, GFont font);

/**
 * Draw the icon to indicate which field is currently being edited.
 *
 * @param layer Pebble canvas layer that the indicator will be drawn on.
 * @param ctx   Graphics context to use to draw the indicator.
 */
static void draw_field_indicator(struct Layer* layer, GContext* ctx);

/**
 * Get the Pebble text layer for the given {@link Timer_field_t}.
 *
 * @param  edit_view   An {@link Edit_view} object.
 * @param  timer_field The {@link Timer_field_t} to get the text layer for.
 * @return             The Pebble text layer for the given {@link Timer_field_t}.
 */
static TextLayer* get_field_text_layer(const struct Edit_view* edit_view,
  const Timer_field_t timer_field);

// Pebble window handlers
static void window_load_handler(Window* window);
static void window_unload_handler(Window* window);
static void window_appear_handler(Window* window);

// Pebble click handlers
static void click_config_provider(void* context);
/**
 * Increment the current field by one.
 */
static void click_handler_up(ClickRecognizerRef recognizer, void* context);
/**
 * Move which field is being edited to the right by one, or exit the
 * {@link Edit_view} if currently editing the last field on the right.
 */
static void click_handler_select(ClickRecognizerRef recognizer, void* context);
/**
 * Decrement the current field by one.
 */
static void click_handler_down(ClickRecognizerRef recognizer, void* context);
/**
 * Move which field is being edited to the left by one, or exit the
 * {@link Edit_view} if currently editing the last field on the left.
 */
static void click_handler_back(ClickRecognizerRef recognizer, void* context);

struct View* edit_view_create(int timer_id) {
  struct Edit_view* edit_view = safe_alloc(sizeof(struct Edit_view));
  edit_view->timer_id = timer_id;
  edit_view->duration = 0;
  edit_view->edit_field_num = 0;
  edit_view->hours_text_buffer[0] = '\0';
  edit_view->minutes_text_buffer[0] = '\0';
  edit_view->seconds_text_buffer[0] = '\0';
  edit_view->window = NULL;
  edit_view->status_bar_layer = NULL;
  edit_view->hours_text_layer = NULL;
  edit_view->minutes_text_layer = NULL;
  edit_view->seconds_text_layer = NULL;
  edit_view->colon_text_layer0 = NULL;
  edit_view->colon_text_layer1 = NULL;
  edit_view->canvas_layer = NULL;
  struct View_interface view_interface = (struct View_interface) {
    .destroy = edit_view_destroy,
    .update_duration = edit_view_update_duration,
    .refresh = edit_view_refresh,
    .push = edit_view_push
  };
  edit_view->view = view_create(edit_view, &view_interface);
  return edit_view->view;
}

static void edit_view_destroy(void* data)
{
  free(data);
}

static void edit_view_push(void* data)
{
  assert(data);
  struct Edit_view* edit_view = (struct Edit_view*) data;

  edit_view->window = window_create();

  assert(edit_view->window);

  window_set_user_data(edit_view->window, edit_view);
  window_set_window_handlers(edit_view->window, (WindowHandlers) {
    .load = window_load_handler,
    .appear = window_appear_handler,
    .unload = window_unload_handler
  });
  window_stack_push(edit_view->window, false);
}

static int edit_view_update_duration(void* data, int group_id, int timer_id,
  int duration)
{
  assert(data);
  assert(timer_id >= 0);
  struct Edit_view* edit_view = (struct Edit_view*) data;
  if (timer_id != edit_view->timer_id) {
    return 0;
  }
  edit_view->duration = duration;
  return 1;
}

static void edit_view_refresh(void* data)
{
  assert(data);
  struct Edit_view* edit_view = (struct Edit_view*) data;

  update_timer_text_layers(edit_view);
}

static void window_load_handler(Window* window)
{
  struct Edit_view* edit_view = window_get_user_data(window);
  assert(edit_view);

  Layer* window_layer = window_get_root_layer(edit_view->window);
  window_set_click_config_provider_with_context(edit_view->window,
    click_config_provider, edit_view);

  // Status bar layer
  edit_view->status_bar_layer = status_bar_layer_create();
  layer_add_child(window_layer,
    status_bar_layer_get_layer(edit_view->status_bar_layer));

  GRect window_bounds = layer_get_bounds(window_layer);

  // Setup timer text layers
  GRect timer_bounds = window_bounds;
  timer_bounds.size.h = TIMER_TEXT_HEIGHT;
  grect_align(&timer_bounds, &window_bounds, GAlignCenter, true);
  // Get the font to use for the text
  GFont font = get_leco_font(PREFERRED_FONT_SIZE_5, s_default_timer_str,
    timer_bounds.size.w - TIMER_TEXT_LEFT_RIGHT_SPACE, timer_bounds.size.h);
  // Calculate the size for each part of the text
  GSize default_timer_string_size = get_min_size_for_text(s_default_timer_str,
    font);
  GSize timer_field_size = get_min_size_for_text("00", font);
  GSize colon_size = get_min_size_for_text(":", font);
  // Adjust the timer bounds so the text is centered vertically
  GRect centered_bounds = vertical_center_text(s_default_timer_str, font,
    timer_bounds.size.w, timer_bounds.size.h);
  timer_bounds.origin.y += centered_bounds.origin.y;
  timer_bounds.size.h = centered_bounds.size.h;
  // Get the initial left offset that will center the text horizontally
  int left_offset = (timer_bounds.size.w - default_timer_string_size.w) / 2;

  // Hours text layer
  edit_view->hours_text_layer = create_text_layer(timer_bounds,
    timer_field_size.w, left_offset, font);
  left_offset += timer_field_size.w;
  layer_add_child(window_layer,
    text_layer_get_layer(edit_view->hours_text_layer));
  // Colon text layer
  edit_view->colon_text_layer0 = create_text_layer(timer_bounds,
    colon_size.w, left_offset, font);
  left_offset += colon_size.w;
  text_layer_set_text(edit_view->colon_text_layer0, ":");
  layer_add_child(window_layer,
    text_layer_get_layer(edit_view->colon_text_layer0));
  // Minutes text layer
  edit_view->minutes_text_layer = create_text_layer(timer_bounds,
    timer_field_size.w, left_offset, font);
  left_offset += timer_field_size.w;
  layer_add_child(window_layer,
    text_layer_get_layer(edit_view->minutes_text_layer));
  // Colon text layer
  edit_view->colon_text_layer1 = create_text_layer(timer_bounds,
    colon_size.w, left_offset, font);
  left_offset += colon_size.w;
  text_layer_set_text(edit_view->colon_text_layer1, ":");
  layer_add_child(window_layer,
    text_layer_get_layer(edit_view->colon_text_layer1));
  // Seconds text layer
  edit_view->seconds_text_layer = create_text_layer(timer_bounds,
    timer_field_size.w, left_offset, font);
  layer_add_child(window_layer,
    text_layer_get_layer(edit_view->seconds_text_layer));
  update_timer_text_layers(edit_view);

  // Field indicator
  edit_view->canvas_layer = layer_create_with_data(window_bounds,
    sizeof(struct Layer_data));
  struct Layer_data* layer_data = layer_get_data(edit_view->canvas_layer);
  layer_data->edit_view = edit_view;
  layer_set_update_proc(edit_view->canvas_layer, draw_field_indicator);
  layer_add_child(window_layer, edit_view->canvas_layer);
}

static TextLayer* create_text_layer(GRect parent_frame, int width,
  int left_offset, GFont font)
{
  GRect frame = parent_frame;
  frame.size.w = width;
  frame.origin.x += left_offset;
  TextLayer* text_layer = text_layer_create(frame);
  assert(text_layer);
  text_layer_set_font(text_layer, font);
  text_layer_set_text_color(text_layer, GColorBlack);
  text_layer_set_background_color(text_layer, GColorWhite);
  text_layer_set_text_alignment(text_layer, GTextAlignmentCenter);
  return text_layer;
}

static void window_appear_handler(Window* window)
{
  struct Edit_view* edit_view = window_get_user_data(window);
  assert(edit_view);

  update_timer_text_layers(edit_view);
}

static void window_unload_handler(Window* window)
{
  struct Edit_view* edit_view = window_get_user_data(window);
  assert(edit_view);

  status_bar_layer_destroy(edit_view->status_bar_layer);
  edit_view->status_bar_layer = NULL;

  text_layer_destroy(edit_view->hours_text_layer);
  edit_view->hours_text_layer = NULL;
  text_layer_destroy(edit_view->minutes_text_layer);
  edit_view->minutes_text_layer = NULL;
  text_layer_destroy(edit_view->seconds_text_layer);
  edit_view->seconds_text_layer = NULL;
  text_layer_destroy(edit_view->colon_text_layer0);
  edit_view->colon_text_layer0 = NULL;
  text_layer_destroy(edit_view->colon_text_layer1);
  edit_view->colon_text_layer1 = NULL;

  layer_destroy(edit_view->canvas_layer);
  edit_view->canvas_layer = NULL;

  window_destroy(edit_view->window);
  edit_view->window = NULL;

  controller_on_view_closed(edit_view->view);
}

static void click_config_provider(void* context)
{
  window_single_repeating_click_subscribe(BUTTON_ID_UP,
    CLICK_REPEAT_INTERVAL_MS, click_handler_up);
  window_single_click_subscribe(BUTTON_ID_SELECT, click_handler_select);
  window_single_repeating_click_subscribe(BUTTON_ID_DOWN,
    CLICK_REPEAT_INTERVAL_MS, click_handler_down);
  window_single_click_subscribe(BUTTON_ID_BACK, click_handler_back);
}

static void click_handler_up(ClickRecognizerRef recognizer, void* context)
{
  assert(context);
  struct Edit_view* edit_view = (struct Edit_view*) context;

  controller_edit_timer(edit_view->timer_id,
    get_timer_field(edit_view->edit_field_num), 1);
}

static void click_handler_select(ClickRecognizerRef recognizer, void* context)
{
  assert(context);
  struct Edit_view* edit_view = (struct Edit_view*) context;

  ++edit_view->edit_field_num;
  if (edit_view->edit_field_num > 2) {
    window_stack_pop(false);
    return;
  }
  layer_mark_dirty(edit_view->canvas_layer);
}

static void click_handler_down(ClickRecognizerRef recognizer, void* context)
{
  assert(context);
  struct Edit_view* edit_view = (struct Edit_view*) context;

  controller_edit_timer(edit_view->timer_id,
    get_timer_field(edit_view->edit_field_num), -1);
}

static void click_handler_back(ClickRecognizerRef recognizer, void* context)
{
  assert(context);
  struct Edit_view* edit_view = (struct Edit_view*) context;

  --edit_view->edit_field_num;
  if (edit_view->edit_field_num < 0) {
    window_stack_pop(false);
    return;
  }
  layer_mark_dirty(edit_view->canvas_layer);
}

static void update_timer_text_layers(struct Edit_view* edit_view)
{
  assert(edit_view);
  // Hours
  update_field_text_layer(edit_view->hours_text_layer,
    edit_view->hours_text_buffer,
    TIMER_FIELD_TEXT_BUFFER_LENGTH,
    timer_get_field_from_seconds(TIMER_FIELD_HOURS, edit_view->duration));
  // Minutes
  update_field_text_layer(edit_view->minutes_text_layer,
    edit_view->minutes_text_buffer,
    TIMER_FIELD_TEXT_BUFFER_LENGTH,
    timer_get_field_from_seconds(TIMER_FIELD_MINUTES, edit_view->duration));
  // Seconds
  update_field_text_layer(edit_view->seconds_text_layer,
    edit_view->seconds_text_buffer,
    TIMER_FIELD_TEXT_BUFFER_LENGTH,
    timer_get_field_from_seconds(TIMER_FIELD_SECONDS, edit_view->duration));
}

static void update_field_text_layer(TextLayer* text_layer, char* buffer,
  int buffer_size, int value)
{
  // Hours
  snprintf(buffer, buffer_size, "%.2d", value);
  text_layer_set_text(text_layer, buffer);
  layer_mark_dirty(text_layer_get_layer(text_layer));
}

static void draw_field_indicator(struct Layer* layer, GContext* ctx)
{
  assert(layer);
  assert(ctx);

  struct Layer_data* layer_data = layer_get_data(layer);
  struct Edit_view* edit_view = layer_data->edit_view;
  assert(edit_view);

  Timer_field_t timer_field = get_timer_field(edit_view->edit_field_num);
  TextLayer* text_layer = get_field_text_layer(edit_view, timer_field);
  if (!text_layer) {
    APP_LOG(APP_LOG_LEVEL_ERROR,
      "Null text layer for field: %d. This should never happen.", timer_field);
    return;
  }
  // Calculate the bounds of the field indicator
  GRect frame = layer_get_frame(text_layer_get_layer(text_layer));
  GRect bounds;
  bounds.size.h = frame.size.h * RECT_HEIGHT_PCT;
  bounds.size.w = frame.size.w * RECT_WIDTH_PCT;
  bounds.origin.x = frame.origin.x + ((frame.size.w - bounds.size.w) / 2);
  bounds.origin.y =
    frame.origin.y +(frame.size.h * (1 + INDICATOR_TOP_SPACING_PCT));
  // Draw the field indicator
  graphics_context_set_fill_color(ctx, GColorBlack);
  graphics_fill_rect(ctx, bounds, bounds.size.h / 2, GCornersAll);
}

static Timer_field_t get_timer_field(int timer_edit_index)
{
  switch (timer_edit_index) {
    case 0:
      return TIMER_FIELD_HOURS;
    case 1:
      return TIMER_FIELD_MINUTES;
    case 2:
      return TIMER_FIELD_SECONDS;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid timer index: %d", timer_edit_index);
      return -1;
  }
}

static TextLayer* get_field_text_layer(const struct Edit_view* edit_view,
  const Timer_field_t timer_field)
{
  assert(edit_view);
  switch (timer_field) {
    case TIMER_FIELD_HOURS:
      return edit_view->hours_text_layer;
    case TIMER_FIELD_MINUTES:
      return edit_view->minutes_text_layer;
    case TIMER_FIELD_SECONDS:
      return edit_view->seconds_text_layer;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid timer index: %d", timer_field);
      return NULL;
  }
}

/** @} // Edit_view */
