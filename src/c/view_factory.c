/**
 * @internal
 * @addtogroup view_factory
 * @{
 */

#include "view_factory.h"
#include "Main_view.h"
#include "Group_view.h"
#include "Edit_view.h"
#include "Countdown_view.h"
#include "Settings_view.h"
#include "Timer_options_view.h"
#include "App_info_view.h"
#include "Advanced_settings_view.h"

#include <pebble.h>

struct View* view_factory(View_type_t view_type, int group_id, int timer_id)
{
  switch (view_type) {
    case VIEW_TYPE_MAIN_VIEW:
      return main_view_create();
    case VIEW_TYPE_GROUP_VIEW:
      return group_view_create(group_id);
    case VIEW_TYPE_EDIT_VIEW:
      return edit_view_create(timer_id);
    case VIEW_TYPE_COUNTDOWN_VIEW:
      return countdown_view_create(group_id, timer_id);
    case VIEW_TYPE_SETTINGS_VIEW:
      return settings_view_create(group_id);
    case VIEW_TYPE_TIMER_OPTIONS_VIEW:
      return timer_options_view_create(timer_id);
    case VIEW_TYPE_APP_INFO_VIEW:
      return app_info_view_create();
    case VIEW_TYPE_ADVANCED_SETTINGS_VIEW:
      return advanced_settings_view_create();
    default:
      APP_LOG(APP_LOG_LEVEL_DEBUG, "Unknown view type");
  }
  return NULL;
}

/** @} // view_factory */
