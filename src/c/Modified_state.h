#ifndef MODIFIED_STATE_H
#define MODIFIED_STATE_H

/**
 * @addtogroup Modified_state Modified state
 * @{
 *
 * Possible states of modification.
 *
 * Used to indicate whether an individual item needs to be saved, or if the
 * item was modified in such a way that we should resave everything.
 */

/**
 * Possible states of modification.
 */
typedef enum {
  /**
   * The item is not modified and does not need to be saved. This item will
   * only be resaved if a preceding item had state of
   * {@link MODIFIED_STATE_MULTIPLE}.
   */
  MODIFIED_STATE_NOT_MODIFIED,
  /**
   * In this modified state, the modification does not modify how other items
   * need to be saved.
   */
  MODIFIED_STATE_SINGLE,
  /**
   * In this modified state, the modification has external effects on how
   * other items need to be saved.
   */
  MODIFIED_STATE_MULTIPLE
} Modified_state_t;

/** @} // Modified_state */

#endif /*MODIFIED_STATE_H*/
