/**
 * @internal
 * @addtogroup vibrate_utility
 * @{
 */

#include "vibrate_utility.h"
#include "Group.h"
#include "Timer.h"
#include "Settings.h"
#include "Settings_fields.h"
#include "assert.h"
#include "Timer_state.h"
#include "Utility.h"

#include <pebble.h>

/**
 * The number of seconds between each vibration for the
 * {@link VIBRATE_INTERVAL_SHORT}.
 */
#define VIBRATE_INTERVAL_SHORT_SECONDS 5
/**
 * The number of seconds between each vibration for the
 * {@link VIBRATE_INTERVAL_MEDIUM}.
 */
#define VIBRATE_INTERVAL_MEDIUM_SECONDS SECONDS_PER_MINUTE
/**
 * The number of seconds between each vibration for the
 * {@link VIBRATE_INTERVAL_LONG}.
 */
#define VIBRATE_INTERVAL_LONG_SECONDS (SECONDS_PER_MINUTE * 5)

/**
 * Get the number of seconds between vibrations for the given
 * {@link Vibrate_interval_t}.
 *
 * @param  Vibrate_interval {@link Vibrate_interval_t} to get the number of
 *                          seconds for.
 * @return                  The number of seconds between vibrations for the
 *                          given {@link Vibrate_interval_t}. Value should always
 *                          be >= 0.
 */
static int get_vibrate_interval_seconds(Vibrate_interval_t Vibrate_interval);

int seconds_until_next_vibrate(const struct Group* group,
  const struct Timer* timer)
{
  assert(group);
  assert(timer);
  const Timer_state_t timer_state = timer_get_state(timer);
  if (timer_state != TIMER_STATE_ELAPSED) {
    return -1;
  }
  int elapsed_seconds = -timer_get_remaining_or_elapsed_seconds(timer);
  assert(elapsed_seconds >= 0);
  const struct Settings* settings = group_get_settings(group);
  int vibrate_interval = get_vibrate_interval_seconds(
    settings_get_vibrate_interval(settings));
  return vibrate_interval - (elapsed_seconds % vibrate_interval);
}

static int get_vibrate_interval_seconds(Vibrate_interval_t vibrate_interval)
{
  int interval = 0;
  switch (vibrate_interval) {
    case VIBRATE_INTERVAL_SHORT:
      interval = VIBRATE_INTERVAL_SHORT_SECONDS;
      break;
    case VIBRATE_INTERVAL_MEDIUM:
      interval = VIBRATE_INTERVAL_MEDIUM_SECONDS;
      break;
    case VIBRATE_INTERVAL_LONG:
      interval = VIBRATE_INTERVAL_LONG_SECONDS;
      break;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid vibrate style: %d",
        vibrate_interval);
      interval = VIBRATE_INTERVAL_SHORT_SECONDS;
      break;
  }
  return max(0, interval);
}

int calculate_vibrate_count(const struct Timer* timer,
  Vibrate_interval_t vibrate_interval)
{
  assert(timer);
  int vibrate_interval_seconds = get_vibrate_interval_seconds(vibrate_interval);
  int remaining = timer_get_remaining_or_elapsed_seconds(timer);
  if (remaining > 0) {
    // The timer hasn't elapsed.
    return 0;
  }
  return ((-remaining) / vibrate_interval_seconds) + 1;
}

int vibrate_style_extended(Vibrate_style_t vibrate_style)
{
  switch (vibrate_style) {
    case VIBRATE_STYLE_EXTENDED_0:
    case VIBRATE_STYLE_EXTENDED_1:
    case VIBRATE_STYLE_EXTENDED_2:
    case VIBRATE_STYLE_EXTENDED_3:
    case VIBRATE_STYLE_EXTENDED_4:
      return 0;
    default:
      return 1;
  }
}

/** @} // vibrate_utility */
