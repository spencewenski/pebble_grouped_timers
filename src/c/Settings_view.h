#ifndef SETTINGS_VIEW_H
#define SETTINGS_VIEW_H

/**
 * @addtogroup Settings_view Settings view
 * @{
 *
 * {@link View} implementation to display the {@link Settings} of the app or a
 * {@link Group}.
 */

struct View;

/**
 * Create a {@link Settings_view settings view}.
 *
 * @param  group_id Id of the {@link Group} the {@link Settings_view} is for, or
 *                  negative if these are the global app {@link Settings}.
 * @return          The abstract {@link View} handle for the
 *                  {@link Settings_view settings view}.
 */
struct View* settings_view_create(int group_id);

/** @} // Settings_view */

#endif /*SETTINGS_VIEW_H*/
