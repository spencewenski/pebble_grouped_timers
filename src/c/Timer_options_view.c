/**
 * @internal
 * @addtogroup Timer_options_view
 * @{
 */

#include "Timer_options_view.h"
#include "View.h"
#include "Utility.h"
#include "assert.h"
#include "Controller.h"
#include "draw_utility.h"
#include "View_interface.h"
#include "locale_framework/src/localize.h"

#include <pebble.h>

#define NUM_TIMER_OPTIONS 2

/**
 * @internal
 * @addtogroup Timer_options_view_interface_impl
 * @{
 *
 * Implementations of the {@link View_interface} for the
 * {@link Timer_options_view}.
 */

/** {@link view_push_fp_t} */
static void timer_options_view_push(void* data);
/** {@link view_destroy_fp_t} */
static void timer_options_view_destroy(void* data);

/** @} // Timer_options_view_interface_impl */

struct Timer_options_view {
  /**
   * Pebble window for this view.
   */
  Window* window;
  /**
   * Pebble menu layer to display the {@link Timer} options.
   */
  MenuLayer* menu_layer;
  /**
   * Pebble status bar layer to display the time at the top of the screen.
   */
  StatusBarLayer* status_bar_layer;
  /**
   * Parent {@link View} object for this {@link Settings_view}.
   */
  struct View* view;
  /**
   * Id of the {@link Timer} to display the options for.
   */
  int timer_id;
};

// Window Handlers
static void window_load_handler(Window* window);
static void window_unload_handler(Window* window);

// MenuLayerCallbacks
static uint16_t menu_get_num_rows_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data);
static void menu_draw_row_callback(GContext* ctx, const Layer* cell_layer,
  MenuIndex* cell_index, void* data);
static void menu_select_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data);
static void menu_draw_header_callback(GContext* ctx, const Layer* cell_layer,
  uint16_t section_index, void* data);

struct View* timer_options_view_create(int timer_id)
{
  struct Timer_options_view* timer_options_view =
    (struct Timer_options_view*) safe_alloc(sizeof(struct Timer_options_view));
  timer_options_view->timer_id = timer_id;
  struct View_interface view_interface = (struct View_interface) {
    .push = timer_options_view_push,
    .destroy = timer_options_view_destroy
  };
  timer_options_view->view = view_create(timer_options_view, &view_interface);
  return timer_options_view->view;
}

static void timer_options_view_destroy(void* data)
{
  free(data);
}

static void timer_options_view_push(void* data)
{
  assert(data);
  struct Timer_options_view* timer_options_view = data;

  timer_options_view->window = window_create();

  assert(timer_options_view->window);

  window_set_user_data(timer_options_view->window, timer_options_view);
  window_set_window_handlers(timer_options_view->window, (WindowHandlers) {
    .load = window_load_handler,
    .unload = window_unload_handler
  });
  window_stack_push(timer_options_view->window, false);
}

static void window_load_handler(Window* window)
{
  struct Timer_options_view* timer_options_view = window_get_user_data(window);
  assert(timer_options_view);

  Layer* window_layer = window_get_root_layer(window);

  // Status bar layer
  timer_options_view->status_bar_layer = status_bar_layer_create();
  layer_add_child(window_layer,
    status_bar_layer_get_layer(timer_options_view->status_bar_layer));

  // Setup the menu layer
  GRect bounds = layer_get_bounds(window_layer);
  bounds = status_bar_adjust_window_bounds(bounds);
  timer_options_view->menu_layer = menu_layer_create(bounds);
  assert(timer_options_view->menu_layer);
  menu_layer_set_callbacks(timer_options_view->menu_layer, timer_options_view,
    (MenuLayerCallbacks) {
      .get_num_rows = menu_get_num_rows_callback,
      .get_cell_height = PBL_IF_ROUND_ELSE(menu_cell_get_height_round, NULL),
      .draw_row = menu_draw_row_callback,
      .draw_header = menu_draw_header_callback,
      .get_header_height = menu_get_header_height_callback,
      .select_click = menu_select_click_callback
    }
  );
  menu_layer_set_click_config_onto_window(timer_options_view->menu_layer,
    window);
  layer_add_child(window_layer,
    menu_layer_get_layer(timer_options_view->menu_layer));
}

static void window_unload_handler(Window* window)
{
  struct Timer_options_view* timer_options_view = window_get_user_data(window);
  assert(timer_options_view);

  menu_layer_destroy(timer_options_view->menu_layer);
  timer_options_view->menu_layer = NULL;

  status_bar_layer_destroy(timer_options_view->status_bar_layer);
  timer_options_view->status_bar_layer = NULL;

  window_destroy(timer_options_view->window);
  timer_options_view->window = NULL;

  controller_on_view_closed(timer_options_view->view);
}

static uint16_t menu_get_num_rows_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data)
{
  return NUM_TIMER_OPTIONS;
}

static void menu_draw_row_callback(GContext* ctx, const Layer* cell_layer,
  MenuIndex* cell_index, void* data)
{
  switch (cell_index->row) {
    case 0:
      menu_cell_basic_draw(ctx, cell_layer, LOCALIZE("Edit timer"), NULL, NULL);
      return;
    case 1:
      menu_cell_basic_draw(ctx, cell_layer, LOCALIZE("Delete timer"), NULL, NULL);
      return;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid row index: %d", cell_index->row);
      return;
  }
}

static void menu_select_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data)
{
  assert(data);
  struct Timer_options_view* timer_options_view = data;

  switch (cell_index->row) {
    case 0:
      controller_open_edit_view(timer_options_view->timer_id);
      break;
    case 1:
      controller_delete_timer(timer_options_view->timer_id);
      break;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid row index: %d", cell_index->row);
      return;
  }
  window_stack_remove(timer_options_view->window, false);
}

static void menu_draw_header_callback(GContext* ctx, const Layer* cell_layer,
  uint16_t section_index, void* data)
{
  switch (section_index) {
    case 0:
      menu_cell_draw_header(ctx, cell_layer, LOCALIZE("Timer options"));
      return;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid section index: %d", section_index);
      return;
  }
}

/** @} // Timer_options_view */
