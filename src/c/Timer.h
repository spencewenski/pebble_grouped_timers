#ifndef TIMER_H
#define TIMER_H

/**
 * @addtogroup Timer
 * @{
 *
 * A countdown timer.
 */

#include "Timer_field.h"
#include "Timer_state.h"

struct Timer;

/**
 * Create a new {@link Timer} with the given id.
 *
 * @param  timer_id Id of the new {@link Timer}.
 * @return          A new {@link Timer} with the given id.
 */
struct Timer* timer_create(int timer_id);

/**
 * Destroy the given {@link Timer}.
 *
 * @param timer {@link Timer} to destroy.
 */
void timer_destroy(struct Timer* timer);

/**
 * Load a {@link Timer} from persistent storage.
 *
 * Should be used when the app is starting to load saved {@link Timer Timers}.
 *
 * @return  A {@link Timer} loaded from persistent storage.
 */
struct Timer* timer_load();

/**
 * Save the given {@link Timer} to persistent storage.
 *
 * @param timer {@link Timer} to save to persistent storage.
 */
void timer_save(const struct Timer* timer);

/**
 * Delete the given {@link Timer} from persistent storage.
 *
 * @param timer {@link Timer} to delete from persistent storage.
 */
void timer_persist_delete(const struct Timer* timer);

/**
 * Broadcast the {@link Timer Timer's} state to the {@link Model}.
 *
 * @param timer {@link Timer} to broadcast the state of.
 */
void timer_broadcast_state(const struct Timer* timer);

/**
 * Get the id of the given {@link Timer}.
 *
 * @param  timer  {@link Timer} to get the id of.
 * @return        The id of the given {@link Timer}.
 */
int timer_get_id(const struct Timer* timer);

/**
 * Get the duration of the {@link Timer} in seconds.
 *
 * @param  timer {@link Timer} to get the duration of.
 * @return       Duration of the {@link Timer} in seconds.
 */
int timer_get_length_seconds(const struct Timer* timer);

/**
 * Get the number of seconds remaining for the {@link Timer}.
 *
 * @param  timer {@link Timer} to get the remaining seconds for.
 * @return       The number of seconds remaining for the {@link Timer}. If the
 *               {@link Timer} has elapsed, return 0.
 */
int timer_get_remaining_seconds(const struct Timer* timer);

/**
 * Get the number of seconds remaining for the timer, or the negative of the
 * amount of time elapsed if the timer has elapsed.
 *
 * @param  timer  {@link Timer} to get the remaining seconds for.
 * @return       The number of seconds remaining for the {@link Timer}. If the
 *               {@link Timer} has elapsed, return the negative of the number of
 *               seconds that have elapsed.
 */
int timer_get_remaining_or_elapsed_seconds(const struct Timer* timer);

/**
 * Get the {@link Timer_state} of the {@link Timer}.
 *
 * @param  timer {@link Timer} to get the {@link Timer_state} of.
 * @return       The current {@link Timer_state} for the given {@link Timer}.
 */
Timer_state_t timer_get_state(const struct Timer* timer);

/**
 * Increment the {@link Timer_field_t} by the given amount.
 *
 * The provided amount is in the same units as the {@link Timer_field_t}. This
 * method bounds the value to a [min, max) for each field. If the field is at a
 * bound and an increment would take the field out of [min, max), the field is
 * 'looped' to the other bound. If the field is not at a bound and an increment
 * would take the field out of [min, max), the field is set to the respective
 * bound.
 *
 * @param timer  {@link Timer} to edit.
 * @param field  {@link Timer_field_t} to edit.
 * @param amount Amount to edit the {@link Timer_field_t} by. Should be in the
 *               same units as the {@link Timer_field_t}, e.g. 1 to increment the
 *               {@link TIMER_FIELD_HOURS} by one hour.
 */
void timer_increment_field(struct Timer* timer, Timer_field_t field,
  int amount);

/**
 * Set the number of times the {@link Timer} has vibrated since it elapsed.
 *
 * @param timer         {@link Timer} to set the vibrate count of.
 * @param vibrate_count The number of time the {@link Timer} has vibrated since
 *                      it elapsed.
 */
void timer_set_vibrate_count(struct Timer* timer, int vibrate_count);

/**
 * Get the number of times the {@link Timer} has vibrated since it elapsed.
 *
 * @param timer         {@link Timer} to get the vibrate count of.
 * @return              The number of time the {@link Timer} has vibrated since
 *                      it elapsed.
 */
int timer_get_vibrate_count(const struct Timer* timer);

/**
 * Start the {@link Timer}.
 *
 * If the {@link Timer} is already started, do nothing. If the {@link Timer} is
 * paused, resume the {@link Timer}.
 *
 * @param timer {@link Timer} to start.
 */
void timer_start(struct Timer* timer);

/**
 * Pause the {@link Timer}.
 *
 * If the {@link Timer} is not started or already paused, do nothing.
 *
 * @param timer {@link Timer} to puase.
 */
void timer_pause(struct Timer* timer);

/**
 * Stop the {@link Timer} if it is running and set its remaining seconds back
 * to zero.
 *
 * @param timer {@link Timer} to reset.
 */
void timer_reset(struct Timer* timer);

/**
 * Update the timer.
 *
 * This is called by the {@link Model} to update the {@link Timer}. If the
 * {@link Timer} is not running or is paused, do nothing.
 *
 * @param timer {@link Timer} to update.
 */
void timer_update(struct Timer* timer);

/** @} // Timer */

#endif /*TIMER_H*/
