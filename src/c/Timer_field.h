#ifndef TIMER_FIELD_H
#define TIMER_FIELD_H

/**
 * @addtogroup Timer_field Timer field
 * @{
 *
 * Hours, minutes, and seconds fields of a timer.
 */

/**
 * Timer fields.
 */
typedef enum {
  /**
   * Hours.
   */
  TIMER_FIELD_HOURS,
  /**
   * Minutes.
   */
  TIMER_FIELD_MINUTES,
  /**
   * Seconds.
   */
  TIMER_FIELD_SECONDS,
  /**
   * Milliseconds. Not really used anywhere.
   */
  TIMER_FIELD_MS
} Timer_field_t;

/** @} // Timer_field */

#endif /*TIMER_FIELD_H*/
