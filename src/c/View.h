#ifndef VIEW_H
#define VIEW_H

/**
 * @addtogroup View
 * @{
 *
 * An abstract view type that provides a fat interface for all the different
 * views.
 *
 * This allows the {@link Model} to interact with all the views without needing
 * to know the exact implementation of any specific view. This makes it very
 * easy to add new types of views without modifying the {@link Model}. Views can
 * set the fields of {@link View_interface} to point to the implementations of
 * the callbacks.
 */

#include "Settings_fields.h"
#include "Advanced_settings_fields.h"
#include "Timer_state.h"

struct View;
struct View_interface;

/**
 * Create a {@link View} object.
 *
 * This be used in the *_create methods for {@link View} implementations.
 *
 * @param  data           Data relevant to a {@link View} implementation. This
 *                        is used as the first argument to the
 *                        {@link View_interface} methods/.
 * @param  view_interface Pointer to a {@link View_interface}. The values of the
 *                        {@link View_interface} are copied into the
 *                        {@link View}, so the data pointed to by this argument
 *                        can and should be safely destroyed.
 * @return                A new {@link View} object.
 */
struct View* view_create(void* data, struct View_interface* view_interface);

/**
 * Destroy the given view.
 *
 * @param view {@link View} to destroy.
 */
void view_destroy(struct View* view);

/**
 * Push the {@link View} onto the window stack.
 *
 * @param view {@link View} to push onto the window stack.
 */
void view_push(struct View* view);

/**
 * Notify the {@link View} of the {@link Timer Timer's} duration.
 *
 * @param view     {@link View} to update.
 * @param group_id Id of the {@link Group} the {@link Timer} belongs to.
 * @param timer_id Id of the {@link Timer} that is updating its duration.
 * @param duration Duration of the {@link Timer}.
 */
void view_update_duration(struct View* view, int group_id, int timer_id,
  int duration);

/**
 * Notify the {@link View} of the time remaining for the {@link Timer}.
 *
 * @param view      {@link View} to update.
 * @param timer_id  Id of the {@link Timer} that is updating its remaining time.
 * @param remaining Remaining time for the {@link Timer} in seconds.
 */
void view_update_remaining(struct View* view, int timer_id, int remaining);

/**
 * Notify the {@link View} that the {@link Timer} was removed.
 *
 * @param view     {@link View} to update.
 * @param timer_id Id of the {@link Timer} that was removed.
 */
void view_update_remove_timer(struct View* view, int timer_id);

/**
 * Notify the {@link View} of the {@link Timer Timer's} {@link Timer_state}.
 *
 * @param view        {@link View} to update.
 * @param timer_id    Id of the {@link Timer} that is updating its
 *                    {@link Timer_state}.
 * @param timer_state New {@link Timer_state} of the {@link Timer}.
 */
void view_update_timer_state(struct View* view, int timer_id,
  Timer_state_t timer_state);

/**
 * Notify the {@link View} that the {@link Group} exists.
 *
 * @param view     {@link View} to update.
 * @param group_id Id of the {@link Group} that was created.
 */
void view_update_group_exists(struct View* view, int group_id);

/**
 * Notify the {@link View} that the {@link Group} was removed.
 *
 * @param view     {@link View} to update.
 * @param group_id Id of the {@link Group} to removed.
 */
void view_update_remove_group(struct View* view, int group_id);

/**
 * Notify the {@link View} that the {@link Settings} were updated.
 *
 * @param view              {@link View} to update.
 * @param group_id          Id of the {@link Group} the {@link Settings} were
 *                          updated for, or negative if the {@link Settings} are
 *                          the app {@link Settings}.
 * @param repeat_style      The new {@link Repeat_style_t}.
 * @param progress_style    The new {@link Progress_style_t}.
 * @param vibrate_interval  The new {@link Vibrate_interval_t}.
 * @param vibrate_style     The new {@link Vibrate_style_t}.
 */
void view_update_settings(struct View* view, int group_id,
  Repeat_style_t repeat_style, Progress_style_t progress_style,
  Vibrate_interval_t vibrate_interval, Vibrate_style_t vibrate_style);

/**
 * Notify the {@link View} that the {@link Advanced_settings} were updated.
 *
 * @param view                 {@link View} to update.
 * @param extended_vibes_state The new {@link Extended_vibes_state_t}.
 */
void view_update_advanced_settings(struct View* view,
  Extended_vibes_state_t extended_vibes_state);

/**
 * Update the {@link View} of the {@link Timer} to track.
 *
 * @param view              {@link View} to update.
 * @param timer_id          Id of the {@link Timer} to track.
 * @param previous_timer_id Id of the {@link Timer} that was previously being
 *                          tracked.
 */
void view_update_timer_tracking(struct View* view, int timer_id,
  int previous_timer_id);

/**
 * Notify the {@link View} that an error occurred.
 *
 * @param view {@link View} to update.
 * @param msg  Error message.
 */
void view_update_error(struct View* view, char* msg);

/**
 * Start a batched update to the {@link View}.
 *
 * @param view {@link View} to start a batched update for.
 */
void view_start_update(struct View* view);

/**
 * Finish a batched update to the {@link View}.
 *
 * The {@link View} will be refreshed after this call.
 *
 * @param view {@link View} to finish a batched update for.
 */
void view_finish_update(struct View* view);

/**
 * Finish a batched update to the {@link View}.
 *
 * The {@link View} will not be refreshed after this call.
 *
 * @param view {@link View} to finish a batched update for.
 */
void view_finish_update_no_refresh(struct View* view);

/** @} // View */

#endif /*VIEW_H*/
