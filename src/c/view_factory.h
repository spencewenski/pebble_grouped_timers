#ifndef VIEW_FACTORY_H
#define VIEW_FACTORY_H

/**
 * @addtogroup view_factory View factory
 * @{
 *
 * Factory to create {@link View Views}.
 */

/**
 * The types of {@link View Views} that can be created by the
 * {@link view_factory}.
 */
typedef enum {
  /**
   * {@link Main_view}.
   */
  VIEW_TYPE_MAIN_VIEW,
  /**
   * {@link Group_view}.
   */
  VIEW_TYPE_GROUP_VIEW,
  /**
   * {@link Countdown_view}.
   */
  VIEW_TYPE_COUNTDOWN_VIEW,
  /**
   * {@link Edit_view}.
   */
  VIEW_TYPE_EDIT_VIEW,
  /**
   * {@link Settings_view}.
   */
  VIEW_TYPE_SETTINGS_VIEW,
  /**
   * {@link Timer_options_view}.
   */
  VIEW_TYPE_TIMER_OPTIONS_VIEW,
  /**
   * {@link App_info_view}.
   */
  VIEW_TYPE_APP_INFO_VIEW,
  /**
   * {@link Advanced_settings_view}.
   */
  VIEW_TYPE_ADVANCED_SETTINGS_VIEW
} View_type_t;

struct View;

/**
 * Create {@link View} for the given {@link View_type}.
 *
 * @param  view_type  {@link View_type} to create.
 * @param  group_id   Id of the {@link Group} to track, or negative if no
 *                    {@link Group} is tracked.
 * @param  timer_id   Id of the {@link Timer} to track, or negative if no
 *                    {@link Timer} is tracked.
 * @return            A {@link View} of the given {@link View_type}, or NULL if
 *                    the {@link View_type} isn't supported.
 */
struct View* view_factory(View_type_t view_type, int group_id, int timer_id);

/** @} // view_factory */

#endif /*VIEW_FACTORY_H*/
