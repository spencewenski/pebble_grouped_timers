#ifndef GLOBALS_H
#define GLOBALS_H

/**
 * @addtogroup globals Global constants
 * @{
 *
 * Global constants used in various places throughout the app.
 */

/**
 * Length of buffers used for storing menu text strings.
 */
#define MENU_TEXT_BUFFER_LENGTH 30

/**
 * Length of buffers used for storing {@link Timer} durations strings.
 *
 * Really only need nine characters, but add a few extra just in case.
 */
#define TIMER_TEXT_BUFFER_LENGTH 12

/**
 * The number of milliseconds in a second.
 */
#define MS_PER_SECOND 1000

/**
 * The number of millisconds in a minute.
 */
#define MS_PER_MINUTE 60000

/** @} // globals */

#endif /*GLOBALS_H*/
