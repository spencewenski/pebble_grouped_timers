#ifndef CONTROLLER_H
#define CONTROLLER_H

/**
 * @addtogroup Controller
 * @{
 *
 * The {@link Controller} is a singleton for controlling the app.
 *
 * The {@link Controller} manages opening {@link View Views} and manipulating
 * {@link Timer Timers}, {@link Group Groups}, and {@link Settings}.
 */

#include "Settings_fields.h"
#include "Advanced_settings_fields.h"
#include "Timer_field.h"

struct View;

/**
 * Tell the {@link Controller} that the app was opened.
 *
 * Should be called once when the app starts.
 */
void controller_app_open();

/**
 * Tell the {@link Controller} that the app is closing.
 *
 * Should be called once when the app is closed.
 */
void controller_app_close();

/**
 * Create a new {@link Group}.
 *
 * @return  The id of the new {@link Group}.
 */
int controller_create_group();

/**
 * Delete the given {@link Group}.
 *
 * @param group_id  Id of the {@link Group} to delete.
 */
void controller_delete_group(int group_id);

/**
 * Create a new {@link Timer} and add it to the given {@link Group}.
 *
 * @param  group_id Id of the {@link Group} the new {@link Timer} will be added
 *                  to.
 * @return          The id of the new {@link Timer}.
 */
int controller_create_timer(int group_id);

/**
 * Delete the given {@link Timer}.
 *
 * @param timer_id  Id of the {@link Timer} to delete.
 */
void controller_delete_timer(int timer_id);

/**
 * Increment the given field of the {@link Timer} by the given amount.
 *
 * @param timer_id  Id of the {@link Timer} to edit.
 * @param field     The field of the {@link Timer} to edit.
 * @param amount    The amount to increment the field by.
 */
void controller_edit_timer(int timer_id, Timer_field_t field, int amount);

/**
 * Start the given {@link Timer}.
 *
 * @param timer_id  Id of the {@link Timer} to start.
 */
void controller_start_timer(int timer_id);

/**
 * Pause the given {@link Timer}.
 *
 * @param timer_id  Id of the {@link Timer} to pause.
 */
void controller_pause_timer(int timer_id);

/**
 * Reset the given {@link Timer}.
 *
 * @param timer_id  Id of the {@link Timer} to reset.
 */
void controller_reset_timer(int timer_id);

/**
 * Reset the given {@link Timer} and start the next {@link Timer}.
 *
 * The next {@link Timer} is determined by the {@link Repeat_style_t} of the
 * {@link Timer}'s {@link Group}.
 *
 * @param group_id  Id of the {@link Group} the {@link Timer} is in.
 * @param timer_id  Id of the current {@link Timer}. This method will start the
 *                  {@link Timer} that comes after the given timer in the
 *                  {@link Group}.
 */
void controller_next_timer(int group_id, int timer_id);

/**
 * Increment the given {@link Settings_field_t} by one.
 *
 * @param group_id        Id of the {@link Group} to edit {@link Settings} for,
 *                        or negative if editing the global {@link Settings}.
 * @param settings_field  {@link Settings_field_t} to edit.
 */
void controller_edit_settings(int group_id, Settings_field_t settings_field);

/**
 * Increment the given {@link Advanced_settings_field_t} by one.
 *
 * @param settings_field  {@link Advanced_settings_field_t} to edit.
 */
void controller_edit_advanced_settings(
  Advanced_settings_field_t settings_field);

/**
 * Open the {@link Main_view}.
 */
void controller_open_main_view();

/**
 * Open the {@link Group_view}.
 *
 * @param group_id  Id of the {@link Group} to display in the
 *                  {@link Group_view}.
 */
void controller_open_group_view(int group_id);

/**
 * Open the {@link Edit_view}.
 *
 * @param timer_id  Id of the {@link Timer} to edit in the {@link Edit_view}.
 */
void controller_open_edit_view(int timer_id);

/**
 * Open the {@link Countdown_view}.
 *
 * @param group_id  Id of the {@link Group} that the {@link Timer} belongs to.
 * @param timer_id  Id of the {@link Timer} to display in the
 *                  {@link Countdown_view}.
 */
void controller_open_countdown_view(int group_id, int timer_id);

/**
 * Open the {@link Timer_options_view}.
 *
 * @param timer_id  Id of the {@link Timer} to open the
 *                  {@link Timer_options_view} for.
 */
void controller_open_timer_options_view(int timer_id);

/**
 * Open the {@link Settings_view}.
 *
 * @param group_id  Id of the {@link Group} to open the {@link Settings_view}
 *                  for, or negative if editing the global app {@link Settings}.
 */
void controller_open_settings_view(int group_id);

/**
 * Open the {@link App_info_view}.
 */
void controller_open_app_info_view();

/**
 * Open the {@link Advanced_settings_view}.
 */
void controller_open_advanced_settings_view();

/**
 * Callback to let the {@link Controller} know that the given {@link View} was
 * closed.
 *
 * Should be called in the {@link View View's} window unload handler.
 *
 * @param view  {@link View} that was closed.
 */
void controller_on_view_closed(struct View* view);

/** @} // Controller */

#endif /*CONTROLLER_H*/
