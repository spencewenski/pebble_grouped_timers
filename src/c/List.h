#ifndef LIST_H
#define LIST_H

/**
 * @addtogroup List
 * @{
 *
 * A dynamic array that holds pointers to void.
 */

struct List;

/**
 * Create a new, empty {@link List}.
 *
 * @returns A new {@link List}.
 */
struct List* list_create();

/**
 * Destroy the {@link List} and its items.
 *
 * Caller is responsible for deleting all pointed-to data before calling this
 * function. After this call, the {@link List} pointer value must not be used
 * again.
 *
 * @param list  {@link List} to destroy.
 */
void list_destroy(struct List* list);

/**
 * Clear the {@link List}.
 *
 * Caller is responsible for deleting pointed-to data first.
 *
 * @param list  {@link List} to clear.
 */
void list_clear(struct List* list);

/**
 * Add the given item to the back of the {@link List}.
 *
 * @param list  {@link List} to add the item to.
 * @param item  Item to add to the {@link List}.
 */
void list_add(struct List* list, void* item);

/**
 * Get the size of the {@link List}.
 *
 * @param  list {@link List} to get the size of.
 * @return      The size of the list.
 */
int list_size(const struct List* list);

/**
 * Is the {@link List} empty?
 *
 * @return  Non-zero (true) if the {@link List} is empty, zero (false) if the
 *          {@link List} is non-empty.
 */
int list_empty(const struct List* list);

/**
 * Get the item at the given index.
 *
 * @param  list   {@link List} to get the item from.
 * @param  index  Index of the item to get from the {@link List}.
 * @return        The item at the given index, or NULL if the index is invalid.
 */
void* list_get(const struct List* list, int index);

/**
 * Remove the item at the given index.
 *
 * Caller is responsible for deleting the pointed-to data.
 *
 * @param list  {@link List} to remove the item from.
 * @param index Index of the item to remove from the {@link List}.
 */
void list_remove(struct List* list, int index);

/**
 * Remove the given data pointer from the {@link List}.
 *
 * If the pointer is not in the {@link List}, do nothing. Caller is responsible
 * for deleting pointed-to data.
 *
 * @param list      {@link List} to remove the item from.
 * @param data_ptr  Item to remove from the {@link List}.
 */
void list_remove_ptr(struct List* list, void* data_ptr);

/**
 * Type of function used by {@link list_for_each}.
 *
 * @param data  Pointer to data to do something with.
 */
typedef void (*List_for_each_fp_t) (void* data);

/**
 * Apply the supplied function to the data pointer in each item of the
 * container.
 *
 * @param list      {@link List} to apply the function to.
 * @param func_ptr  {@link List_for_each_fp_t} implementation to apply to each
 *                  element of the {@link List}.
 */
void list_for_each(const struct List* list, List_for_each_fp_t func_ptr);

/**
 * Type of function that compares two data pointers.
 *
 * @param  data_ptr0  First pointer to compare.
 * @param  data_ptr1  Second pointer to compare.
 * @return            0 when data_ptr0 is equal to data_ptr1, negative when
 *                    data_ptr0 is less than data_ptr1, and positive when
 *                    data_ptr0 is greater than data_ptr1.
 */
typedef int (*List_compare_fp_t) (const void* data_ptr0, const void* data_ptr1);

/**
 * Find an item in the {@link List} for which func_ptr returns 0.
 *
 * Uses data_ptr as the first argument to func_ptr, and the {@link List} element
 * as the second argument.
 *
 * @param  list     {@link List} to get the item from.
 * @param  data_ptr Pointer to data to compare to elements of the {@link List}.
 * @param  func_ptr {@link List_compare_fp_t} implementation to compare data_ptr
 *                  to elements of the {@link List}.
 * @return          The first element in the {@link List} for which func_ptr
 *                  returns 0, or NULL if no such element exists.
 */
void* list_find(const struct List* list, const void* data_ptr,
  List_compare_fp_t func_ptr);

/**
 * Find the pointer in the {@link List}.
 *
 * @param  list     {@link List} to find the pointer in.
 * @param  data_ptr Pointer to find in the {@link List}.
 * @return          The same pointer if found, or NULL if not found.
 */
void* list_find_ptr(const struct List* list, const void* data_ptr);

/**
 * Find the index of the pointer in the list.
 *
 * @param  list     {@link List} to find the pointer in.
 * @param  data_ptr Pointer to find in the list.
 * @return          The index of the pointer in the {@link List}.
 */
int list_find_ptr_index(const struct List* list, const void* data_ptr);

/**
 * Type of function that compares the data pointed to by arg_ptr and data_ptr.
 *
 * The actual types pointed to by arg_ptr and data_ptr don't need to be the
 * same, which allows you to compare two objects without creating an extra
 * instance of the object. For example, arg_ptr could point to a datum with the
 * same value as a member of the sought-for data object.
 *
 * @param  arg_ptr  Value to compare to the data_ptr.
 * @param  data_ptr Data to compare to the arg_ptr.
 * @return          0 if the data in arg_ptr is equal to the data in data_ptr,
 *                  negative if arg_ptr is less than data_ptr, and positive if
 *                  arg_ptr is greater than data_ptr.
 */
typedef int (*List_compare_arg_fp_t) (const void* arg_ptr,
  const void* data_ptr);

/**
 * Find an item in the list for which func_ptr returns 0.
 *
 * Uses arg_ptr as the first argument to func_ptr, and the list element as the
 * second argument.
 *
 * @param  list     {@link List} to find the value in.
 * @param  arg_ptr  Pointer to data to compare to each element of the
 *                  {@link List}.
 * @param  func_ptr {@link List_compare_arg_fp_t} implementation to compare
 *                  arg_ptr to elements of the {@link List}.
 * @return          The first element in the {@link List} for which func_ptr
 *                  returns 0, or NULL if no such element exists.
 */
void* list_find_arg(const struct List* list, const void* arg_ptr,
  List_compare_arg_fp_t func_ptr);

/** @} // List */

#endif /*LIST_H*/
