/**
 * @internal
 * @addtogroup App_info_view
 * @{
 */


#include "App_info_view.h"
#include "Utility.h"
#include "View_interface.h"
#include "View.h"
#include "assert.h"
#include "draw_utility.h"
#include "Controller.h"
#include "locale_framework/src/localize.h"

#include <pebble.h>

// For reading info from the package.json
#include "pebble_process_info.h"
extern const PebbleProcessInfo __pbl_app_info;

/**
 * Number of menu sections in the {@link App_info_view}.
 */
#define APP_INFO_VIEW_NUM_SECTIONS 1

/**
 * Number of rows in the {@link App_info_view}.
 */
#define APP_INFO_VIEW_NUM_ROWS 2

#define SUBTITLE_TEXT_BUFFER_SIZE 30

const char * const beta_string_c = "Beta";

/**
 * @internal
 * @addtogroup App_info_view_interface_impl
 * @{
 */

/** {@link view_push_fp_t} */
static void app_info_view_push(void* data);
/** {@link view_destroy_fp_t} */
static void app_info_view_destroy(void* data);

/** @} // App_info_view_interface_impl */

/**
 * Data required for the {@link App_info_view}.
 */
struct App_info_view {
  /**
   * Pebble window for this view.
   */
  Window* window;
  /**
   * Pebble menu layer to display the app info.
   */
  MenuLayer* menu_layer;
  /**
   * Pebble status bar layer to display the time at the top of the screen.
   */
  StatusBarLayer* status_bar_layer;
  /**
   * Parent {@link View} object for this {@link App_info_view}.
   */
  struct View* view;
};

/**
 * Get text containing the app version.
 *
 * @param buf      Buffer to write the app version text to.
 * @param buf_size Size of the buffer.
 */
static void get_app_version_subtitle_text(char* buf, int buf_size);

// WindowHandlers
static void window_load_handler(Window* window);
static void window_unload_handler(Window* window);

// MenuLayerCallbacks
static uint16_t menu_get_num_sections_callback(MenuLayer* menu_layer,
  void* data);
static uint16_t menu_get_num_rows_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data);
static void menu_draw_header_callback(GContext* ctx, const Layer* cell_layer,
  uint16_t section_index, void* data);
static void menu_draw_row_callback(GContext* ctx, const Layer* cell_layer,
  MenuIndex* cell_index, void* data);

struct View* app_info_view_create()
{
  struct App_info_view* app_info_view =
    safe_alloc(sizeof(struct App_info_view));
  struct View_interface view_interface = (struct View_interface) {
    .push = app_info_view_push,
    .destroy = app_info_view_destroy
  };
  app_info_view->view = view_create(app_info_view, &view_interface);
  return app_info_view->view;
}

static void app_info_view_destroy(void* data)
{
  free(data);
}

static void app_info_view_push(void* data)
{
  assert(data);
  struct App_info_view* app_info_view = (struct App_info_view*) data;

  app_info_view->window = window_create();

  assert(app_info_view->window);

  window_set_user_data(app_info_view->window, app_info_view);
  window_set_window_handlers(app_info_view->window, (WindowHandlers) {
    .load = window_load_handler,
    .unload = window_unload_handler
  });
  window_stack_push(app_info_view->window, false);
}

static void window_load_handler(Window* window)
{
  struct App_info_view* app_info_view = window_get_user_data(window);
  assert(app_info_view);

  Layer* window_layer = window_get_root_layer(app_info_view->window);

  // Status bar layer
  app_info_view->status_bar_layer = status_bar_layer_create();
  layer_add_child(window_layer,
    status_bar_layer_get_layer(app_info_view->status_bar_layer));

  // App info layer
  GRect bounds = layer_get_bounds(window_layer);
  bounds = status_bar_adjust_window_bounds(bounds);
  app_info_view->menu_layer = menu_layer_create(bounds);
  assert(app_info_view->menu_layer);

  menu_layer_set_callbacks(app_info_view->menu_layer, app_info_view,
    (MenuLayerCallbacks) {
      .get_num_sections = menu_get_num_sections_callback,
      .get_num_rows = menu_get_num_rows_callback,
      .get_header_height = menu_get_header_height_callback,
      .draw_header = menu_draw_header_callback,
      .get_cell_height = PBL_IF_ROUND_ELSE(menu_cell_get_height_round, NULL),
      .draw_row = menu_draw_row_callback,
    }
  );
  menu_layer_set_click_config_onto_window(app_info_view->menu_layer, window);
  layer_add_child(window_layer,
    menu_layer_get_layer(app_info_view->menu_layer));
}

static void window_unload_handler(Window* window)
{
  struct App_info_view* app_info_view = window_get_user_data(window);
  assert(app_info_view);

  menu_layer_destroy(app_info_view->menu_layer);
  app_info_view->menu_layer = NULL;

  status_bar_layer_destroy(app_info_view->status_bar_layer);
  app_info_view->status_bar_layer = NULL;

  window_destroy(app_info_view->window);
  app_info_view->window = NULL;

  controller_on_view_closed(app_info_view->view);
}

static uint16_t menu_get_num_sections_callback(MenuLayer* menu_layer,
  void* data)
{
  return APP_INFO_VIEW_NUM_SECTIONS;
}

static uint16_t menu_get_num_rows_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data)
{
  return APP_INFO_VIEW_NUM_ROWS;
}

static void menu_draw_header_callback(GContext* ctx, const Layer* cell_layer,
  uint16_t section_index, void* data)
{
  switch (section_index) {
    case 0:
      menu_cell_draw_header(ctx, cell_layer, LOCALIZE("App info"));
      break;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid section index: %d", section_index);
      return;
  }
}

static void menu_draw_row_callback(GContext* ctx, const Layer* cell_layer,
  MenuIndex* cell_index, void* data)
{
  char subtitle_text_buf[SUBTITLE_TEXT_BUFFER_SIZE];
  subtitle_text_buf[0] = '\0';
  const char* title_text = NULL;
  const char* subtitle_text = NULL;
  switch (cell_index->row) {
    case 0:
      title_text = LOCALIZE("Developer");
      subtitle_text = __pbl_app_info.company;
      break;
    case 1:
      title_text = LOCALIZE("Version");
      get_app_version_subtitle_text(subtitle_text_buf, SUBTITLE_TEXT_BUFFER_SIZE);
      subtitle_text = subtitle_text_buf;
      break;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid row index: %d", cell_index->row);
      break;
  }
  menu_cell_basic_draw(ctx, cell_layer, title_text, subtitle_text, NULL);
}

static void get_app_version_subtitle_text(char* buf, int buf_size)
{
  assert(buf);
  const char* beta_string = strstr(__pbl_app_info.name, beta_string_c) ?
    beta_string_c : NULL;
  snprintf(buf, buf_size, "%s%s%d.%d",
    beta_string != NULL ? beta_string : "",
    beta_string != NULL ? " " : "",
    __pbl_app_info.process_version.major,
    __pbl_app_info.process_version.minor);
}

/** @} // App_info_view */
