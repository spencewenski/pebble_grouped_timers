/**
 * @internal
 * @addtogroup Group
 * @{
 */

#include "Group.h"
#include "List.h"
#include "Utility.h"
#include "Timer.h"
#include "assert.h"
#include "Settings.h"
#include "Settings_fields.h"
#include "Model.h"
#include "persist_utility.h"
#include "vibrate_utility.h"
#include "Modified_state.h"

#include <pebble.h>

/**
 * Get the index of the {@link Timer} with the given id.
 *
 * @param  group    {@link Group} to get the {@link Timer} index from.
 * @param  timer_id Id of the {@link Timer} to get the index of.
 * @return          The index of the {@link Timer} with the given id, or
 *                  negative if no {@link Timer} has the given id.
 */
static int get_timer_index(const struct Group* group, int timer_id);

/**
 * Data required for a {@link Group}.
 */
struct Group {
  /**
   * Id of the {@link Group}.
   */
  int id;
  /**
   * Persist key that was used to load the {@link Group}, or
   * {@link INVALID_PERSIST_KEY} if the {@link Group} has never been loaded.
   */
  int persist_key;
  /**
   * {@link List} of {@link Timer Timers} in this {@link Group}.
   */
  struct List* timers;
  /**
   * The {@link Settings} for this {@link Group}.
   */
  struct Settings* settings;
  /**
   * The modified state of this {@link Group}.
   */
  Modified_state_t modified_state;
};

/**
 * Data required to save a {@link Group} to persistent storage.
 */
struct Group_save_data {
  /**
   * Id of the {@link Group}.
   */
  int group_id;
  /**
   * Number of {@link Timer Timers} in the {@link Group}.
   */
  int num_timers;
};

struct Group* group_create(int group_id)
{
  struct Group* group = safe_alloc(sizeof(struct Group));
  group->id = group_id;
  group->timers = list_create();
  group->settings = settings_create(group->id);
  group->persist_key = INVALID_PERSIST_KEY;
  // The group hasn't been loaded, assume it needs to be saved.
  group->modified_state = MODIFIED_STATE_SINGLE;
  return group;
}

void group_destroy(struct Group* group)
{
  assert(group);
  list_for_each(group->timers, (List_for_each_fp_t)timer_destroy);
  list_destroy(group->timers);
  settings_destroy(group->settings);
  free(group);
}

struct Group* group_load()
{
  struct Group_save_data group_save_data;
  // Load the group date from persistent storage
  int persist_key = persist_load(&group_save_data,
    sizeof(struct Group_save_data));
  struct Group* group = group_create(group_save_data.group_id);
  group->persist_key = persist_key;
  // Load settings
  settings_destroy(group->settings);
  group->settings = settings_load(group->id);
  // Load timers
  for (int i = 0; i < group_save_data.num_timers; ++i) {
    struct Timer* timer = timer_load();
    list_add(group->timers, timer);
    // Calculate how many times the timer has vibrated since it elapsed
    int vibrate_count = calculate_vibrate_count(timer,
      settings_get_vibrate_interval(group->settings));
    timer_set_vibrate_count(timer, vibrate_count);
  }
  // The group has been loaded, it doesn't need to be saved until it is
  // modified.
  group->modified_state = MODIFIED_STATE_NOT_MODIFIED;
  return group;
}

void group_save(const struct Group* group)
{
  assert(group);
  struct Group_save_data group_save_data;
  // Save the group's data
  group_save_data.group_id = group->id;
  group_save_data.num_timers = list_size(group->timers);
  persist_save(&group_save_data, sizeof(struct Group_save_data),
    group->modified_state);
  // Save the settings
  settings_save(group->settings);
  // Save the timers
  for (int i = 0; i < list_size(group->timers); ++i) {
    timer_save(list_get(group->timers, i));
  }
}

void group_persist_delete(const struct Group* group)
{
  assert(group);
  if (group->persist_key < 0) {
    return;
  }
  list_for_each(group->timers, (List_for_each_fp_t)timer_persist_delete);
  settings_persist_delete(group->settings);
  persist_delete(group->persist_key);
}

void group_broadcast_state(const struct Group* group)
{
  assert(group);
  model_notify_group_exists(group->id);
  list_for_each(group->timers, (List_for_each_fp_t)timer_broadcast_state);
  settings_broadcast_state(group->settings);
}

int group_get_id(const struct Group* group)
{
  assert(group);

  return group->id;
}

int group_compare_id(int* group_id, const struct Group* group)
{
  assert(group);
  assert(*group_id >= 0);

  return *group_id - group->id;
}

struct Settings* group_get_settings(const struct Group* group)
{
  assert(group);

  return group->settings;
}

void group_add_timer(struct Group* group, struct Timer* timer)
{
  assert(group);
  assert(timer);

  list_add(group->timers, timer);
  group->modified_state = MODIFIED_STATE_MULTIPLE;
}

void group_remove_timer(struct Group* group, struct Timer* timer)
{
  assert(group);
  assert(timer);

  list_remove_ptr(group->timers, timer);
  group->modified_state = MODIFIED_STATE_MULTIPLE;
}

int group_size(const struct Group* group)
{
  assert(group);

  return list_size(group->timers);
}

struct Timer* group_get_timer(const struct Group* group, int index)
{
  assert(group);
  if (!in_range(index, 0, list_size(group->timers))) {
    return NULL;
  }
  return list_get(group->timers, index);
}

struct Timer* group_get_next_timer(const struct Group* group, int timer_id)
{
  assert(group);
  int current_timer_index = get_timer_index(group, timer_id);
  if (!in_range(current_timer_index, 0, list_size(group->timers))) {
    return NULL;
  }
  int next_timer_index = current_timer_index + 1;
  switch (settings_get_repeat_style(group->settings)) {
    case REPEAT_STYLE_SINGLE:
      // Repeat the current timer.
      return group_get_timer(group, current_timer_index);
    case REPEAT_STYLE_GROUP:
      // Get the next timer, looping to the first timer if the current timer
      // is the last timer in the group.
      return group_get_timer(group, (next_timer_index % list_size(group->timers)));
    case REPEAT_STYLE_NONE:
      // Get the next timer.
      return group_get_timer(group, next_timer_index);
    default:
      return NULL;
  }
}

struct Timer* group_get_timer_by_id(const struct Group* group, int timer_id)
{
  assert(group);
  assert(timer_id >= 0);
  int index = get_timer_index(group, timer_id);
  return index >= 0 ? list_get(group->timers, index) : NULL;
}

static int get_timer_index(const struct Group* group, int timer_id)
{
  assert(group);
  assert(timer_id >= 0);
  for (int i = 0; i < list_size(group->timers); ++i) {
    struct Timer* timer = list_get(group->timers, i);
    if (timer_get_id(timer) == timer_id) {
      return i;
    }
  }
  return -1;
}

/** @} // Group */
