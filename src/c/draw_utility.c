/**
 * @internal
 * @addtogroup draw_utility
 * @{
 */

#include "draw_utility.h"
#include "assert.h"
#include "globals.h"
#include "timer_utility.h"
#include "Utility.h"

#include <pebble.h>

/**
 * The number of {@link Preferred_font_size_t font sizes}.
 */
#define NUM_FONT_SIZES 6

/**
 * Size of the palettes.
 */
#define PALETTE_SIZE 2

/**
 * Data for the Leco fonts.
 */
struct Leco_font_data {
  /**
   * Name of the Leco font.
   */
  const char * const key;
  /**
   * The difference between the width of the '1' character and all the other
   * number characters in the Leco font.
   *
   * This value is used to prevent jumping between different font sizes when
   * calculating the max font size for a frame.
   */
  int extra_space_for_one_char;
};

/**
 * {@link Leco_font_data} for each Leco font.
 */
static struct Leco_font_data leco_font_data[] = {
  {FONT_KEY_LECO_20_BOLD_NUMBERS, -1},        // PREFERRED_FONT_SIZE_0
  {FONT_KEY_LECO_26_BOLD_NUMBERS_AM_PM, -1},  // PREFERRED_FONT_SIZE_1
  {FONT_KEY_LECO_32_BOLD_NUMBERS, -1},        // PREFERRED_FONT_SIZE_2
  {FONT_KEY_LECO_36_BOLD_NUMBERS, -1},        // PREFERRED_FONT_SIZE_3
  {FONT_KEY_LECO_38_BOLD_NUMBERS, -1},        // PREFERRED_FONT_SIZE_4
  {FONT_KEY_LECO_42_NUMBERS, -1}              // PREFERRED_FONT_SIZE_5
};

/**
 * Palette for an icon when its row is hightlighted.
 */
static GColor s_highlighted_palette[PALETTE_SIZE];

/**
 * Palette for an icon when its row is not hightlighted.
 */
static GColor s_unhighlighted_palette[PALETTE_SIZE];

#ifdef PBL_ROUND
/**
 * Draw a header for a menu layer section with centered text.
 *
 * This is an implementation of the Pebble MenuLayerDrawHeaderCallback.
 *
 * @param ctx         Graphics context of the header.
 * @param cell_layer  Layer of the menu cell.
 * @param text        Text of the header.
 */
static void menu_cell_draw_header_centered(GContext* ctx,
  const Layer* cell_layer, const char* text);
#endif

/**
 * Convert the {@link Preferred_font_size_t} into an index.
 *
 * @param  preferred_font_size The {@link Preferred_font_size_t} to convert.
 * @return                     Index that corresponds to the given
 *                             {@link Preferred_font_size_t}/
 */
static int get_preferred_font_size_index(
  const Preferred_font_size_t preferred_font_size);

/**
 * Get the leco font data.
 *
 * Calculate and set the {@link Leco_font_data#extra_space_for_one_char} for
 * each Leco font.
 *
 * @return The {@link Leco_font_data} for each leco font.
 */
static const struct Leco_font_data* get_leco_font_data();

/**
 * Get the number of '1' characters in the given text.
 *
 * @param  text Text to count the number of '1' characters in.
 * @return      The number of '1' charaters in the given text.
 */
static int get_number_of_ones(const char* text);

/**
 * Initialize the app's palettes.
 */
static void init_palettes();

void menu_cell_draw_header(GContext* ctx, const Layer* cell_layer,
  const char* text)
{
  assert(ctx);
  assert(cell_layer);
  assert(text);

  PBL_IF_ROUND_ELSE(
    menu_cell_draw_header_centered(ctx, cell_layer, text),
    menu_cell_basic_header_draw(ctx, cell_layer, text)
    );
}

#ifdef PBL_ROUND
static void menu_cell_draw_header_centered(GContext* ctx,
  const Layer* cell_layer, const char* text)
{
  GSize size = layer_get_frame(cell_layer).size;
  graphics_draw_text(ctx,
                     text,
                     fonts_get_system_font(FONT_KEY_GOTHIC_14_BOLD),
                     GRect(0, 0, size.w, size.h),
                     GTextOverflowModeTrailingEllipsis,
                     GTextAlignmentCenter,
                     NULL);
}
#endif

#ifdef PBL_ROUND
int16_t menu_cell_get_height_round(MenuLayer* menu_layer, MenuIndex* cell_index,
  void* data)
{
  if (cell_index->section == menu_layer_get_selected_index(menu_layer).section &&
      cell_index->row == menu_layer_get_selected_index(menu_layer).row) {
    return MENU_CELL_ROUND_FOCUSED_SHORT_CELL_HEIGHT;
  }
  return MENU_CELL_ROUND_UNFOCUSED_SHORT_CELL_HEIGHT;
}
#endif

void get_timer_text(char* buf, int buf_size, int hours, int minutes,
  int seconds)
{
  assert(buf);
  if (hours > 0) {
    snprintf(buf, buf_size, "%d:%.2d:%.2d", hours, minutes, seconds);
  } else if (minutes > 0) {
    snprintf(buf, buf_size, "%d:%.2d", minutes, seconds);
  } else {
    snprintf(buf, buf_size, ":%.2d", seconds);
  }
}

void get_timer_text_duration(char* buf, int buf_size, int duration)
{
  assert(buf);
  get_timer_text(buf, buf_size,
    timer_get_field_from_seconds(TIMER_FIELD_HOURS, duration),
    timer_get_field_from_seconds(TIMER_FIELD_MINUTES, duration),
    timer_get_field_from_seconds(TIMER_FIELD_SECONDS, duration));
}

GRect status_bar_adjust_window_bounds(GRect bounds)
{
  bounds.origin.y += STATUS_BAR_LAYER_HEIGHT;
  bounds.size.h -= STATUS_BAR_LAYER_HEIGHT;
  return bounds;
}

GRect action_bar_adjust_window_bounds(GRect bounds)
{
  bounds.size.w -= ACTION_BAR_WIDTH;
  return bounds;
}

int16_t menu_get_header_height_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data)
{
  return MENU_CELL_BASIC_HEADER_HEIGHT;
}

GSize get_min_size_for_text(const char* text, const GFont font)
{
  GRect large_rect;
  large_rect.origin.x = large_rect.origin.y = 0;
  large_rect.size.w = PBL_DISPLAY_WIDTH;
  large_rect.size.h = PBL_DISPLAY_HEIGHT;
  return graphics_text_layout_get_content_size(text, font,
      large_rect, GTextOverflowModeWordWrap, GTextAlignmentCenter);
}

static const struct Leco_font_data* get_leco_font_data()
{
  if (leco_font_data[0].extra_space_for_one_char >= 0) {
    return leco_font_data;
  }
  // I'm pretty sure that '1' is the only number that has a different width
  // in the Leco font
  const char * const one_string = "1";
  const char * const other_string = "2";
  for (int i = 0; i < NUM_FONT_SIZES; ++i) {
    GFont font = fonts_get_system_font(leco_font_data[i].key);
    GSize one_size = get_min_size_for_text(one_string, font);
    GSize other_size = get_min_size_for_text(other_string, font);
    leco_font_data[i].extra_space_for_one_char = other_size.w - one_size.w;
  }
  return leco_font_data;
}

static int get_number_of_ones(const char* text)
{
  int count = 0;
  while (*text) {
    if (*text++ == '1') {
      ++count;
    }
  }
  return count;
}

GFont get_leco_font(const Preferred_font_size_t preferred_font_size,
  const char* text, const int frame_width, const int frame_height)
{
  const struct Leco_font_data* leco_fonts = get_leco_font_data();
  // Make sure we don't return NULL
  GFont font = fonts_get_system_font(leco_fonts[0].key);
  // Start at 1 because we already loaded the smallest font
  for (int i = 1; i <= get_preferred_font_size_index(preferred_font_size); ++i)
  {
    GFont tmp_font = fonts_get_system_font(leco_fonts[i].key);
    GSize size = get_min_size_for_text(text, tmp_font);
    int num_ones = get_number_of_ones(text);
    if (size.w > (max(0,
          frame_width - (num_ones * leco_fonts[i].extra_space_for_one_char))) ||
        size.h > frame_height) {
      // If the font doesn't fit in the bounds, return the largest font we have
      // so far
      return font;
    } else {
      font = tmp_font;
    }
  }
  return font;
}

static int get_preferred_font_size_index(
  const Preferred_font_size_t preferred_font_size)
{
  switch (preferred_font_size) {
    case PREFERRED_FONT_SIZE_0:
      return 0;
    case PREFERRED_FONT_SIZE_1:
      return 1;
    case PREFERRED_FONT_SIZE_2:
      return 2;
    case PREFERRED_FONT_SIZE_3:
      return 3;
    case PREFERRED_FONT_SIZE_4:
      return 4;
    case PREFERRED_FONT_SIZE_5:
      return 5;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid Preferred_font_size: %d",
        preferred_font_size);
      return -1;
  }
}

GRect vertical_center_text(const char* text, const GFont font,
  int frame_width, int frame_height)
{
  GSize size = get_min_size_for_text(text, font);
  GRect bounds;
  bounds.size.w = frame_width;
  bounds.size.h = size.h;
  bounds.origin.x = 0;
  bounds.origin.y = max(0, (frame_height - bounds.size.h) / 4);
  return bounds;
}

void set_palette(GBitmap* bitmap, int highlighted)
{
  if (!bitmap) {
    return;
  }
  init_palettes();
  GColor* palette = highlighted ? s_highlighted_palette :
    s_unhighlighted_palette;
  gbitmap_set_palette(bitmap, palette, false);
}

static void init_palettes()
{
  static int init_count = 0;
  if (init_count > 0) {
    return;
  }
  ++init_count;
  // Highlighted
  s_highlighted_palette[0] = GColorClear;
  s_highlighted_palette[1] = GColorWhite;
  // Unhighlighted
  s_unhighlighted_palette[0] = GColorClear;
  s_unhighlighted_palette[1] = GColorBlack;
}


/** @} // draw_utility */
