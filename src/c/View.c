/**
 * @internal
 * @addtogroup View
 * @{
 */

#include "View.h"
#include "assert.h"
#include "Utility.h"
#include "Settings_fields.h"
#include "Timer_state.h"
#include "View_interface.h"

/**
 * Refresh this {@link View}.
 *
 * Do nothing if the {@link View} is not dirty or is updating.
 *
 * @param view {@link View} to refresh.
 */
static void view_refresh(struct View* view);

/**
 * Data used by the {@link View}.
 */
struct View {
  /**
   * The {@link View_interface} for this {@link View}.
   */
  struct View_interface view_interface;
  /**
   * Pointer to the {@link View} implementation this {@link View} represents.
   *
   * This is passed as the first argument to the {@link View_interface}
   * callbacks.
   */
  void* data;
  /**
   * Positive if the {@link View} is being updated in a batched update, zero
   * otherwise.
   */
  int updating;
  /**
   * Positive if the {@link View} has been modified since the last time the
   * {@link View} was refreshed, zero otherwise
   */
  int dirty;
};

// Todo: view_interface should probably be a value type instead of a pointer.
struct View* view_create(void* data, struct View_interface* view_interface)
{
  struct View* view = safe_alloc(sizeof(struct View));
  view->view_interface = *view_interface;
  view->data = data;
  view->updating = 0;
  view->dirty = 0;
  return view;
}

void view_destroy(struct View* view)
{
  assert(view);
  if (!view->view_interface.destroy) {
    APP_LOG(APP_LOG_LEVEL_WARNING, "No view_destroy_fp_t defined for view");
    return;
  }
  view->view_interface.destroy(view->data);
  free(view);
}

void view_push(struct View* view)
{
  assert(view);
  if (!view->view_interface.push) {
    APP_LOG(APP_LOG_LEVEL_WARNING, "No view_push_fp_t defined for view");
    return;
  }
  view->view_interface.push(view->data);
  view_refresh(view);
}

void view_update_duration(struct View* view, int group_id, int timer_id,
  int duration)
{
  assert(view);
  if (!view->view_interface.update_duration) {
    return;
  }
  view->dirty += max(0, view->view_interface.update_duration(view->data,
    group_id, timer_id, duration));
  view_refresh(view);
}

void view_update_remaining(struct View* view, int timer_id, int remaining)
{
  assert(view);
  if (!view->view_interface.update_remaining) {
    return;
  }
  view->dirty += max(0, view->view_interface.update_remaining(view->data,
    timer_id, remaining));
  view_refresh(view);
}

void view_update_remove_timer(struct View* view, int timer_id)
{
  assert(view);
  if (!view->view_interface.update_remove_timer) {
    return;
  }
  view->dirty += max(0, view->view_interface.update_remove_timer(view->data,
    timer_id));
  view_refresh(view);
}

void view_update_timer_state(struct View* view, int timer_id,
  Timer_state_t timer_state)
{
  assert(view);
  if (!view->view_interface.update_timer_state) {
    return;
  }
  view->dirty += max(0, view->view_interface.update_timer_state(view->data,
    timer_id, timer_state));
  view_refresh(view);
}

void view_update_group_exists(struct View* view, int group_id)
{
  assert(view);
  if (!view->view_interface.update_group_exists) {
    return;
  }
  view->dirty += max(0, view->view_interface.update_group_exists(view->data,
    group_id));
  view_refresh(view);
}

void view_update_remove_group(struct View* view, int group_id)
{
  assert(view);
  if (!view->view_interface.update_remove_group) {
    return;
  }
  view->dirty += max(0, view->view_interface.update_remove_group(view->data,
    group_id));
  view_refresh(view);
}

void view_update_settings(struct View* view, int group_id,
  Repeat_style_t repeat_style, Progress_style_t progress_style,
  Vibrate_interval_t vibrate_interval, Vibrate_style_t vibrate_style)
{
  assert(view);
  if (!view->view_interface.update_group_settings) {
    return;
  }
  view->dirty += max(0, view->view_interface.update_group_settings(view->data,
    group_id, repeat_style, progress_style, vibrate_interval, vibrate_style));
  view_refresh(view);
}

void view_update_advanced_settings(struct View* view,
  Extended_vibes_state_t extended_vibes_state)
{
  assert(view);
  if (!view->view_interface.update_advanced_settings) {
    return;
  }
  view->dirty += max(0, view->view_interface
    .update_advanced_settings(view->data, extended_vibes_state));
  view_refresh(view);
}

// Update which timer this view is tracking
void view_update_timer_tracking(struct View* view, int timer_id,
  int previous_timer_id)
{
  assert(view);
  if (!view->view_interface.update_timer_tracking) {
    return;
  }
  view->dirty += max(0, view->view_interface.update_timer_tracking(view->data,
    timer_id, previous_timer_id));
  view_refresh(view);
}

void view_update_error(struct View* view, char* msg)
{
  assert(view);
  if (!view->view_interface.update_error) {
    return;
  }
  view->dirty += max(0, view->view_interface.update_error(view->data, msg));
  view_refresh(view);
}

// Update batching
void view_start_update(struct View* view)
{
  assert(view);
  view->updating = 1;
}

void view_finish_update(struct View* view)
{
  view->updating = 0;
  view_refresh(view);
}

void view_finish_update_no_refresh(struct View* view)
{
  view->updating = 0;
  view->dirty = 0;
}

static void view_refresh(struct View* view)
{
  assert(view);
  if (view->updating || !view->dirty) {
    return;
  }
  if (!view->view_interface.refresh) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "no view_refresh_fp_t defined for view");
    return;
  }
  view->view_interface.refresh(view->data);
  view->dirty = 0;
}

/** @} // View */
