/**
 * @internal
 * @addtogroup Vibrate_manager
 * @{
 */

#include "Vibrate_manager.h"
#include "Utility.h"
#include "assert.h"
#include "Controller.h"
#include "Timer.h"
#include "Group.h"
#include "Settings.h"
#include "Settings_fields.h"
#include "Timer_state.h"
#include "vibrate_utility.h"
#include "Model.h"

#include <pebble.h>

/**
 * Custom vibration pattern for {@link VIBRATE_STYLE_EXTENDED_0}.
 */
static const uint32_t const extended_vibration_0_segments[] = { 5000 };

/**
 * Number of segments from {@link extended_vibration_0_segments} to use for
 * {@link VIBRATE_STYLE_EXTENDED_0}
 */
#define EXTENDED_VIBRATION_0_SEGMENT_COUNT 1

/**
 * Custom vibration pattern for {@link VIBRATE_STYLE_EXTENDED_1} through
 * {@link VIBRATE_STYLE_EXTENDED_4}.
 */
static const uint32_t const extended_vibration_segments[] = {
  10000,  // 10 seconds total (extended_vibration_1)
  0,      // space
  10000,  // 20 seconds total (extended_vibration_2)
  0,      // space
  10000,  // 30 seconds total (extended_vibration_3)
  0,      // space
  10000,  // 40 seconds total
  0,      // space
  10000,  // 50 seconds total
  0,      // space
  10000   // 60 seconds total (extended_vibration_4)
};

/**
 * Number of segments from {@link extended_vibration_segments} to use for
 * {@link VIBRATE_STYLE_EXTENDED_1}
 */
#define EXTENDED_VIBRATION_1_SEGMENT_COUNT 1
/**
 * Number of segments from {@link extended_vibration_segments} to use for
 * {@link VIBRATE_STYLE_EXTENDED_2}
 */
#define EXTENDED_VIBRATION_2_SEGMENT_COUNT 3
/**
 * Number of segments from {@link extended_vibration_segments} to use for
 * {@link VIBRATE_STYLE_EXTENDED_3}
 */
#define EXTENDED_VIBRATION_3_SEGMENT_COUNT 5
/**
 * Number of segments from {@link extended_vibration_segments} to use for
 * {@link VIBRATE_STYLE_EXTENDED_4}
 */
#define EXTENDED_VIBRATION_4_SEGMENT_COUNT 11

/**
 * Data used by the {@link Vibrate_manager}.
 */
struct Vibrate_manager {
  /**
   * The time in seconds of the last vibration, or negative if the app has
   * never vibrated.
   */
  int last_vibrate_time;
};
/**
 * The singleton {@link Vibrate_manager} instance.
 */
static struct Vibrate_manager* s_vibrate_manager = NULL;

/**
 * Create a new {@link Vibrate_manager} object.
 *
 * @return a new {@link Vibrate_manager} object.
 */
static struct Vibrate_manager* vibrate_manager_create();

/**
 * Destroy the {@link Vibrate_manager} object.
 *
 * @param vibrate_manager {@link Vibrate_manager} object to destroy
 */
static void vibrate_manager_destroy(struct Vibrate_manager* vibrate_manager);

/**
 * Call the Pebble vibration method that corresponds to the given
 * {@link Vibrate_style_t}.
 *
 * @param vibrate_style {@link Vibrate_style_t} of the vibration to do.
 */
static void do_vibrate(Vibrate_style_t vibrate_style);

/**
 * Perform a custom vibration.
 *
 * @param segments The custom vibration segments.
 * @param size     The number of vibration segments to use for this vibration.
 */
static void custom_vibration(const uint32_t * const segments, uint32_t size);

void vibrate_manager_init()
{
  if (s_vibrate_manager) {
    return;
  }
  s_vibrate_manager = vibrate_manager_create();
}

void vibrate_manager_uninit()
{
  assert(s_vibrate_manager);
  vibrate_manager_destroy(s_vibrate_manager);
  s_vibrate_manager = NULL;
}

void vibrate_manager_vibrate_immediate(Vibrate_style_t vibrate_style)
{
  assert(s_vibrate_manager);
  // Bail if we've already vibrated this second and this is not an extended
  // vibration.
  if (s_vibrate_manager->last_vibrate_time >= time(NULL) &&
      vibrate_style_extended(vibrate_style) != 0) {
    return;
  }
  s_vibrate_manager->last_vibrate_time = time(NULL);

  do_vibrate(vibrate_style);
}

void vibrate_manager_vibrate_force(Vibrate_style_t vibrate_style)
{
  assert(s_vibrate_manager);
  s_vibrate_manager->last_vibrate_time = time(NULL);
  do_vibrate(vibrate_style);
}

static void do_vibrate(Vibrate_style_t vibrate_style)
{
  switch (vibrate_style) {
    case VIBRATE_STYLE_SHORT_PULSE:
      return vibes_short_pulse();
    case VIBRATE_STYLE_LONG_PULSE:
      return vibes_long_pulse();
    case VIBRATE_STYLE_DOUBLE_PULSE:
      return vibes_double_pulse();
    case VIBRATE_STYLE_EXTENDED_0:
      return custom_vibration(extended_vibration_0_segments,
        EXTENDED_VIBRATION_0_SEGMENT_COUNT);
    case VIBRATE_STYLE_EXTENDED_1:
      return custom_vibration(extended_vibration_segments,
        EXTENDED_VIBRATION_1_SEGMENT_COUNT);
    case VIBRATE_STYLE_EXTENDED_2:
      return custom_vibration(extended_vibration_segments,
        EXTENDED_VIBRATION_2_SEGMENT_COUNT);
    case VIBRATE_STYLE_EXTENDED_3:
      return custom_vibration(extended_vibration_segments,
        EXTENDED_VIBRATION_3_SEGMENT_COUNT);
    case VIBRATE_STYLE_EXTENDED_4:
      return custom_vibration(extended_vibration_segments,
        EXTENDED_VIBRATION_4_SEGMENT_COUNT);
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid vibrate style: %d", vibrate_style);
      return;
  }
}

void vibrate_manager_vibrate(struct Group* group, struct Timer* timer)
{
  assert(s_vibrate_manager);
  assert(group);
  assert(timer);
  assert(timer_get_state(timer) == TIMER_STATE_ELAPSED);

  int count = calculate_vibrate_count(timer,
    settings_get_vibrate_interval(group_get_settings(group)));
  if (count <= timer_get_vibrate_count(timer)) {
    // Can't vibrate
    return;
  }
  timer_set_vibrate_count(timer, count);

  vibrate_manager_vibrate_immediate(
    settings_get_vibrate_style(group_get_settings(group)));

  // Make sure the countdown view is open
  controller_open_countdown_view(group_get_id(group), timer_get_id(timer));
}

void vibrate_manager_cancel_long_vibes(Vibrate_style_t vibrate_style)
{
  if (vibrate_style_extended(vibrate_style) == 0) {
    vibes_cancel();
  }
}

static struct Vibrate_manager* vibrate_manager_create()
{
  struct Vibrate_manager* vibrate_manager =
    (struct Vibrate_manager*) safe_alloc(sizeof(struct Vibrate_manager));
  vibrate_manager->last_vibrate_time = -1;
  return vibrate_manager;
}

static void vibrate_manager_destroy(struct Vibrate_manager* vibrate_manager)
{
  free(vibrate_manager);
}

static void custom_vibration(const uint32_t * const segments, uint32_t size)
{
  VibePattern pat = {
    .durations = segments,
    .num_segments = size,
  };
  vibes_enqueue_custom_pattern(pat);
}

/** @} // Vibrate_manager */
