#ifndef COUNTDOWN_VIEW_H
#define COUNTDOWN_VIEW_H

/**
 * @addtogroup Countdown_view Countdown view
 * @{
 *
 * {@link View} implementation used to display the countdown of a {@link Timer}.
 */

struct View;

/**
 * Create a {@link Countdown_view countdown view}.
 *
 * @param  group_id Id of the {@link Group} that the {@link Timer} belongs to.
 * @param  timer_id Id of the {@link Timer} to display in the
 *                  {@link Countdown_view}.
 * @return          The abstract {@link View} handle for the
 *                  {@link Countdown_view countdown view}.
 */
struct View* countdown_view_create(int group_id, int timer_id);

/** @} // Countdown_view */

#endif /*COUNTDOWN_VIEW_H*/
