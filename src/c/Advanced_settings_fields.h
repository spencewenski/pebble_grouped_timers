#ifndef ADVANCED_SETTINGS_FIELDS_H
#define ADVANCED_SETTINGS_FIELDS_H

/**
 * @addtogroup Advanced_settings_fields Advanced settings fields
 * @{
 *
 * Fields used in the {@link Advanced_settings}.
 */

#define NUM_ADVANCED_SETTINGS_FIELDS 1

/**
 * The available advanced settings fields.
 */
typedef enum {
  /**
   * Enabled/disabled state of the extended vibes.
   */
  ADVANCED_SETTINGS_FIELD_EXTENDED_VIBES_STATE
} Advanced_settings_field_t;

/**
 * The enabled state of extended vibes.
 *
 * Values are assigned only to ensure consistency when saving and loading from
 * persistent storage.
 */
typedef enum {
  /**
   * Extended vibes are disabled.
   */
  EXTENDED_VIBES_DISABLED = 0,
  /**
   * Extended vibes are enabled.
   */
  EXTENDED_VIBES_ENABLED = 1
} Extended_vibes_state_t;

/** @} // Advanced_settings_fields */

#endif /*ADVANCED_SETTINGS_FIELDS_H*/
