/**
 * @internal
 * @addtogroup Timer
 * @{
 */

#include "Timer.h"
#include "Utility.h"
#include "assert.h"
#include "Model.h"
#include "timer_utility.h"
#include "Timer_state.h"
#include "persist_utility.h"
#include "Modified_state.h"

#include <pebble.h>
#include <stdlib.h>
#include <math.h>

/**
 * Default values for the {@link Timer} fields.
 */
#define DEFAULT_VALUE 0

/**
 * Maximum value for hours, exclusive.
 */
#define MAX_HOURS 60

/**
 * Maximum value for minutes, exclusive.
 */
#define MAX_MINUTES 60

/**
 * Maximum value for seconds, exclusive.
 */
#define MAX_SECONDS 60

/**
 * Get the maximum value (exclusive) for the {@link Timer_field_t}, e.g.,
 * {@link MAX_HOURS} for {@link TIMER_FIELD_HOURS}.
 *
 * @param  field {@link Timer_field_t} to get the max value for
 * @return       The maximum value (exclusive) for the {@link Timer_field_t}.
 */
static int get_max_value(Timer_field_t field);

/**
 * Set the {@link Timer_field_t} of the {@link Timer} to zero.
 *
 * @param timer {@link Timer} to set the {@link Timer_field_t} for.
 * @param field {@link Timer_field_t} to set to zero.
 */
static void set_field_to_zero(struct Timer* timer, Timer_field_t field);

/**
 * Set the {@link Timer_field_t} of the {@link Timer} to the maximum available
 * for the {@link Timer_field_t}.
 *
 * @param timer {@link Timer} to set the {@link Timer_field_t} for.
 * @param field {@link Timer_field_t} to set to the max.
 */
static void set_field_to_max(struct Timer* timer, Timer_field_t field);

/**
 * Get the number of seconds that have elapsed for the {@link Timer}.
 *
 * @param  timer {@link Timer} to get the elapsed seconds for.
 * @return       The number of seconds that have elapsed for the {@link Timer}.
 */
static int get_elapsed_seconds(const struct Timer* timer);

/**
 * Is the {@link Timer} currently running?
 *
 * @param  timer  {@link Timer} to check if it's currently running.
 * @return        Non-zero if timer is running, zero otherwise
 */
static int timer_is_running(const struct Timer* timer);

/**
 * Is the {@link Timer} currently paused?
 *
 * @param  timer  {@link Timer} to check if it's currently running.
 * @return        Non-zero if timer is paused, zero otherwise
 */
static int timer_is_paused(const struct Timer* timer);

/**
 * Has the {@link Timer} elapsed?
 *
 * @param  timer  {@link Timer} to check if it's currently running.
 * @return        Non-zero if timer is elapsed, zero otherwise
 */
static int timer_is_elapsed(const struct Timer* timer);

/**
 * Data required for a {@link Timer}.
 */
struct Timer {
  /**
   * Id of the {@link Timer}.
   */
  int id;
  /**
   * Duration of the {@link Timer} in seconds.
   */
  int duration;
  /**
   * Persist key that was used to load the {@link Timer}, or
   * {@link INVALID_PERSIST_KEY} if the {@link Timer} has never been loaded.
   */
  int persist_key;
  /**
   * When the {@link Timer} is running, this is the start time minus the amount
   * of time elapsed. When the {@link Timer} is paused, this is the negative of
   * the amount of time elapsed. If the timer hasn't been started, this is zero.
   */
  int start_time_seconds;
  /**
   * The number of times the {@link Timer} has vibrated since it last elapsed.
   */
  int vibrate_count;
  /**
   * The modified state of this {@link Timer}'s duration.
   */
  Modified_state_t duration_modified_state;
  /**
   * The modified state of this {@link Timer}'s start time.
   */
  Modified_state_t start_time_modified_state;
  /**
   * Has this timer been saved since it was last started? True if the loaded
   * start time is not zero, false otherwise.
   */
  bool saved_since_started;
};

/**
 * Data required to save a {@link Timer} to persistent storage.
 */
struct Timer_save_data {
  /**
   * Id of the {@link Timer}.
   */
  int timer_id;
  /**
   * Duration of the {@link Timer} in seconds.
   */
  int duration;
  /**
   * Start time of the {@link Timer}.
   */
  int start_time;
};

struct Timer* timer_create(int timer_id) {
  struct Timer* timer = safe_alloc(sizeof(struct Timer));
  timer->id = timer_id;
  timer->duration = DEFAULT_VALUE;
  timer->persist_key = INVALID_PERSIST_KEY;
  timer->start_time_seconds = DEFAULT_VALUE;
  timer->vibrate_count = DEFAULT_VALUE;
  // The timer hasn't been loaded, assume it needs to be saved.
  timer->duration_modified_state = MODIFIED_STATE_SINGLE;
  timer->start_time_modified_state = MODIFIED_STATE_SINGLE;
  timer->saved_since_started = false;
  return timer;
}

void timer_destroy(struct Timer* timer)
{
  free(timer);
}

struct Timer* timer_load()
{
  struct Timer_save_data timer_save_data;
  int persist_key = persist_load(&timer_save_data,
    sizeof(struct Timer_save_data));
  struct Timer* timer = timer_create(timer_save_data.timer_id);
  timer->duration = timer_save_data.duration;
  timer->start_time_seconds = timer_save_data.start_time;
  timer->persist_key = persist_key;
  // The timer has been loaded, it doesn't need to be saved until it is
  // modified.
  timer->duration_modified_state = MODIFIED_STATE_NOT_MODIFIED;
  timer->start_time_modified_state = MODIFIED_STATE_NOT_MODIFIED;
  timer->saved_since_started = timer_save_data.start_time != 0;
  return timer;
}

void timer_save(const struct Timer* timer)
{
  assert(timer);
  struct Timer_save_data timer_save_data;
  timer_save_data.timer_id = timer->id;
  timer_save_data.duration = timer->duration;
  timer_save_data.start_time = timer->start_time_seconds;
  // Only save if either the duration or start time need to be saved.
  const Modified_state_t modified_state =
    (timer->duration_modified_state == MODIFIED_STATE_NOT_MODIFIED &&
    timer->start_time_modified_state == MODIFIED_STATE_NOT_MODIFIED) ?
      MODIFIED_STATE_NOT_MODIFIED : MODIFIED_STATE_SINGLE;
  persist_save(&timer_save_data, sizeof(struct Timer_save_data),
    modified_state);
}

void timer_persist_delete(const struct Timer* timer)
{
  assert(timer);
  if (timer->persist_key < 0) {
    return;
  }
  persist_delete(timer->persist_key);
}

void timer_broadcast_state(const struct Timer* timer)
{
  assert(timer);
  model_notify_duration(timer->id, timer_get_length_seconds(timer));
  model_notify_remaining(timer->id, timer_get_remaining_seconds(timer));
  model_notify_timer_state(timer->id, timer_get_state(timer));
}

int timer_get_id(const struct Timer* timer)
{
  assert(timer);
  return timer->id;
}

void timer_increment_field(struct Timer* timer, Timer_field_t field,
  int amount)
{
  assert(timer);
  if (amount == 0) {
    return;
  }
  int field_current_value = timer_get_field_from_seconds(field,
    timer->duration);
  if (field_current_value + amount >= get_max_value(field)) {
    set_field_to_zero(timer, field);
  } else if (field_current_value + amount < 0) {
    set_field_to_max(timer, field);
  } else {
    timer->duration += timer_convert_field_to_seconds(field, amount);
  }
  timer->duration_modified_state = MODIFIED_STATE_SINGLE;
  model_notify_duration(timer->id, timer_get_length_seconds(timer));
}

static int get_max_value(Timer_field_t field)
{
  switch (field) {
    case TIMER_FIELD_HOURS:
      return MAX_HOURS;
    case TIMER_FIELD_MINUTES:
      return MAX_MINUTES;
    case TIMER_FIELD_SECONDS:
      return MAX_SECONDS;
    default:
      APP_LOG(APP_LOG_LEVEL_WARNING, "Invalid timer field");
      return 0;
  }
}

static void set_field_to_zero(struct Timer* timer, Timer_field_t field)
{
  assert(timer);
  int field_current_value = timer_get_field_from_seconds(field,
    timer->duration);
  timer->duration -= timer_convert_field_to_seconds(field, field_current_value);
}

static void set_field_to_max(struct Timer* timer, Timer_field_t field)
{
  assert(timer);
  set_field_to_zero(timer, field);
  timer->duration += timer_convert_field_to_seconds(field,
    get_max_value(field) - 1);
}

int timer_get_length_seconds(const struct Timer* timer)
{
  assert(timer);
  return timer->duration;
}

int timer_get_remaining_seconds(const struct Timer* timer)
{
  assert(timer);
  int remaining = timer_get_remaining_or_elapsed_seconds(timer);
  return remaining > 0 ? remaining : 0;
}

int timer_get_remaining_or_elapsed_seconds(const struct Timer* timer)
{
  assert(timer);
  // If the timer isn't started
  if (!timer_is_running(timer) && !timer_is_paused(timer)) {
    return timer_get_length_seconds(timer);
  }
  return timer_get_length_seconds(timer) - get_elapsed_seconds(timer);
}

static int get_elapsed_seconds(const struct Timer* timer)
{
  assert(timer);
  if (timer->start_time_seconds <= 0) {
    return abs(timer->start_time_seconds);
  }
  return time(NULL) - timer->start_time_seconds;
}

void timer_update(struct Timer* timer)
{
  assert(timer);
  if (!timer_is_running(timer)) {
    // Timer not running; nothing to update
    return ;
  }
  model_notify_remaining(timer->id, timer_get_remaining_seconds(timer));
  model_notify_timer_state(timer->id, timer_get_state(timer));
}

void timer_start(struct Timer* timer)
{
  assert(timer);
  if (timer->start_time_seconds > 0) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Timer already started");
    return;
  }
  timer->start_time_seconds += time(NULL);
  timer->start_time_modified_state = MODIFIED_STATE_SINGLE;
  model_notify_remaining(timer->id, timer_get_remaining_seconds(timer));
  model_notify_timer_state(timer->id, timer_get_state(timer));
}

void timer_pause(struct Timer* timer)
{
  assert(timer);
  if (timer->start_time_seconds <= 0) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Timer not started");
    return;
  }
  timer->start_time_seconds -= time(NULL);
  timer->start_time_modified_state = MODIFIED_STATE_SINGLE;
  model_notify_remaining(timer->id, timer_get_remaining_seconds(timer));
  model_notify_timer_state(timer->id, timer_get_state(timer));
}

void timer_reset(struct Timer* timer)
{
  assert(timer);
  timer->start_time_seconds = DEFAULT_VALUE;
  timer->vibrate_count = DEFAULT_VALUE;
  // Only save if the timer has been saved since it was last started.
  if (timer->saved_since_started) {
    timer->start_time_modified_state = MODIFIED_STATE_SINGLE;
  } else {
    timer->start_time_modified_state = MODIFIED_STATE_NOT_MODIFIED;
  }
  model_notify_remaining(timer->id, timer_get_remaining_seconds(timer));
  model_notify_timer_state(timer->id, timer_get_state(timer));
}

void timer_set_vibrate_count(struct Timer* timer, int vibrate_count)
{
  assert(timer);
  assert(vibrate_count >= 0);
  timer->vibrate_count = vibrate_count;
}

int timer_get_vibrate_count(const struct Timer* timer)
{
  assert(timer);
  return timer->vibrate_count;
}

Timer_state_t timer_get_state(const struct Timer* timer)
{
  assert(timer);
  const int timer_running = timer_is_running(timer);
  const int timer_paused = timer_is_paused(timer);
  const int timer_elapsed = timer_is_elapsed(timer);

  if (timer_elapsed) {
    return TIMER_STATE_ELAPSED;
  }
  if (timer_running) {
    return TIMER_STATE_STARTED;
  }
  if (timer_paused) {
    return TIMER_STATE_PAUSED;
  }
  return TIMER_STATE_NOT_STARTED;
}

static int timer_is_running(const struct Timer* timer)
{
  assert(timer);
  return timer->start_time_seconds > 0 ? 1 : 0;
}

static int timer_is_paused(const struct Timer* timer)
{
  assert(timer);
  return (timer->start_time_seconds <= 0 &&
    get_elapsed_seconds(timer) > 0) ? 1 : 0;
}

static int timer_is_elapsed(const struct Timer* timer)
{
  assert(timer);
  if (timer_get_length_seconds(timer) <= 0) {
    return 0;
  }
  return get_elapsed_seconds(timer) >= timer_get_length_seconds(timer) ? 1 : 0;
}

/** @} // Timer */
