/**
 * @addtogroup assert
 * @{
 *
 * Macro used to check preconditions and print an error message and exit the app
 * if the precondition is not satisfied.
 *
 * Derived from the standard assert implementation, which is subject to the
 * following copyright notice:
 *
 * Copyright (c) 1992, 1993
 *  The Regents of the University of California.  All rights reserved.
 * (c) UNIX System Laboratories, Inc.
 * All or some portions of this file are derived from material licensed
 * to the University of California by American Telephone and Telegraph
 * Co. or Unix System Laboratories, Inc. and are reproduced herein with
 * the permission of UNIX System Laboratories, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *  This product includes software developed by the University of
 *  California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * @(#)assert.h  8.2 (Berkeley) 1/21/94
 *
 * %$FreeBSD: src/include/assert.h,v 1.4 2002/03/23 17:24:53 imp Exp $
 */

#ifndef ASSERT_H
#define ASSERT_H

#include <pebble.h>

/**
 * Check the precondition and print an error message.
 *
 * If this is a debug build also exit the app.
 *
 * @param e Precondition to check.
 */
#define assert(e)  \
    ((void) ((e) ? ((void)0) : __assert (#e, __FILE__, __LINE__)))
#ifdef NDEBUG
/**
 * @internal
 * Just print an error message.
 */
#define __assert(e, file, line) \
    ((void)APP_LOG(APP_LOG_LEVEL_ERROR, "%s:%u: failed assertion - continuing: '%s'\n", file, line, e))
#else
/**
 * @internal
 * Print an error message and exit.
 */
#define __assert(e, file, line) \
    ((void)APP_LOG(APP_LOG_LEVEL_ERROR, "%s:%u: failed assertion - exiting: '%s'\n", file, line, e), window_stack_pop_all(false))
#endif

/** @} // assert */

#endif /*ASSERT_H*/
