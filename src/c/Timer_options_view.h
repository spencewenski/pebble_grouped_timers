#ifndef TIMER_OPTIONS_VIEW_H
#define TIMER_OPTIONS_VIEW_H

/**
 * @addtogroup Timer_options_view Timer options view
 * @{
 *
 * {@link View} implementation for displaying the {@link Timer} options.
 */

struct View;

/**
 * Create a {@link Timer_options_view timer options view}.
 *
 * @param  timer_id Id of the {@link Timer} to display the options for.
 * @return          The abstract {@link View} handle for the
 *                  {@link Timer_options_view timer options view}.
 */
struct View* timer_options_view_create(int timer_id);

/** @} // Timer_options_view */

#endif /*TIMER_OPTIONS_VIEW_H*/
