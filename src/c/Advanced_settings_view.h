#ifndef ADVANCED_SETTINGS_VIEW_H
#define ADVANCED_SETTINGS_VIEW_H

/**
 * @addtogroup Advanced_settings_view Advanced settings view
 * @{
 *
 * {@link View} implementation to display the app's {@link Advanced_settings}.
 */

struct View;

/**
 * Create a {@link Advanced_settings_view settings view}.
 *
 * @return The abstract {@link View} handle for the
 *         {@link Advanced_settings_view advanced settings view}.
 */
struct View* advanced_settings_view_create();

/** @} // Advanced_settings_view */

#endif /*ADVANCED_SETTINGS_VIEW_H*/
