#ifndef SETTINGS_VIEW_DATA_H
#define SETTINGS_VIEW_DATA_H

#include <pebble.h>

/**
 * @addtogroup Settings_view_data Settings view data
 * @{
 *
 * {@link Settings} data used for a {@link View}, such as the
 * {@link Settings_view} and the {@link Countdown_view}.
 *
 * This is provided only to reduce code duplication between the
 * {@link Settings_view} and the {@link Countdown_view}
 */

#include "Settings_fields.h"

struct Settings_view_data;
struct Settings_view_icons;

/**
 * Create a {@link Settings_view_data} object.
 *
 * @return A new {@link Settings_view_data} object.
 */
struct Settings_view_data* settings_view_data_create();

/**
 * Destroy the given {@link Settings_view_data}.
 *
 * @param settings_view_data {@link Settings_view_data} to destroy.
 */
void settings_view_data_destroy(struct Settings_view_data* settings_view_data);

/**
 * Set the values for the {@link Settings_view_data}.
 *
 * @param settings_view_data {@link Settings_view_data} to set the values for.
 * @param repeat_style       {@link Repeat_style_t} to set.
 * @param progress_style     {@link Progress_style_t} to set.
 * @param vibrate_interval   {@link Vibrate_interval_t} to set.
 * @param vibrate_style      {@link Vibrate_style_t} to set.
 */
void settings_view_data_set_values(
  struct Settings_view_data* settings_view_data, Repeat_style_t repeat_style,
  Progress_style_t progress_style, Vibrate_interval_t vibrate_interval,
  Vibrate_style_t vibrate_style);

/**
 * Get the {@link Repeat_style_t} from the {@link Settings_view_data}.
 *
 * @param  settings_view_data {@link Settings_view_data} to get the
 *                            {@link Repeat_style_t} from.
 * @return                    The {@link Repeat_style_t} from the given
 *                            {@link Settings_view_data}.
 */
Repeat_style_t settings_view_data_get_repeat_style(
  const struct Settings_view_data* settings_view_data);

/**
 * Get the {@link Progress_style_t} from the {@link Settings_view_data}.
 *
 * @param  settings_view_data {@link Settings_view_data} to get the
 *                            {@link Progress_style_t} from.
 * @return                    The {@link Progress_style_t} from the given
 *                            {@link Settings_view_data}.
 */
Progress_style_t settings_view_data_get_progress_style(
  const struct Settings_view_data* settings_view_data);

/**
 * Get the {@link Vibrate_interval_t} from the {@link Settings_view_data}.
 *
 * @param  settings_view_data {@link Settings_view_data} to get the
 *                            {@link Vibrate_interval_t} from.
 * @return                    The {@link Vibrate_interval_t} from the given
 *                            {@link Settings_view_data}.
 */
Vibrate_interval_t settings_view_data_get_vibrate_interval(
  const struct Settings_view_data* settings_view_data);

/**
 * Get the {@link Vibrate_style_t} from the {@link Settings_view_data}.
 *
 * @param  settings_view_data {@link Settings_view_data} to get the
 *                            {@link Vibrate_style_t} from.
 * @return                    The {@link Vibrate_style_t} from the given
 *                            {@link Settings_view_data}.
 */
Vibrate_style_t settings_view_data_get_vibrate_style(
  const struct Settings_view_data* settings_view_data);

/**
 * Get the {@link Vibrate_style_t} for the given group from its
 * {@link Settings_view_data}.
 *
 * @param  group_id               The id of the group to get the
 *                                {@link Vibrate_style_t} for.
 * @param  default_settings_data  The default {@link Settings_view_data}.
 * @param  group_settings_data    The group's {@link Settings_view_data}.
 * @return                        The {@link Vibrate_style_t} from the given
 *                                group.
 */
Vibrate_style_t settings_view_data_get_group_vibrate_style(
  int group_id,
  const struct Settings_view_data* default_settings_data,
  const struct Settings_view_data* group_settings_data);

/**
 * Get the {@link Vibrate_style_t} for the given group from its
 * {@link Vibrate_style_t}.
 *
 * @param  group_id       The id of the group to get the
 *                        {@link Vibrate_style_t} for.
 * @param  default_style  The default {@link Vibrate_style_t}.
 * @param  group_style    The group's {@link Vibrate_style_t}.
 * @return                The {@link Vibrate_style_t} for the given group.
 */
Vibrate_style_t settings_view_data_get_group_vibrate_style_from_styles(
  int group_id, Vibrate_style_t default_style, Vibrate_style_t group_style);

/**
 * Get the bitmap for the given {@link Settings_field_t}.
 *
 * @param  settings_view_icons   {@link Settings_view_icons} to get the bitmap
 *                               from.
 * @param  group_id              Id of the group the {@link Settings_view_data}
 *                               is for, or negative if the
 *                               {@link Settings_view_data} is the default
 *                               {@link Settings}.
 * @param  default_settings_data The {@link Settings_view_data} for the default
 *                               {@link Settings}.
 * @param  group_settings_data   The {@link Settings_view_data} for a group's
 *                               {@link Settings}.
 * @param  settings_field        {@link Settings_field_t} to get the icon for.
 * @return                       The bitmap for the given
 *                               {@link Settings_field_t}.
 */
GBitmap* settings_view_icons_get_for_field(
  struct Settings_view_icons* settings_view_icons,
  int group_id,
  const struct Settings_view_data* default_settings_data,
  const struct Settings_view_data* group_settings_data,
  Settings_field_t settings_field);

/**
 * Create a {@link Settings_view_icons} object.
 *
 * @return A new {@link Settings_view_icons} object.
 */
struct Settings_view_icons* settings_view_icons_create();

/**
 * Destroy the given {@link Settings_view_icons}.
 *
 * The icons should be unloaded before this is called. Unload the icons using
 * {@link settings_view_icons_unload}.
 *
 * @param settings_view_icons {@link Settings_view_icons} to destroy.
 */
void settings_view_icons_destroy(
  struct Settings_view_icons* settings_view_icons);

/**
 * Unload the settings icons in the given {@link Settings_view_icons} object.
 *
 * @param settings_view_icons {@link Settings_view_icons} object to unload the
 *                            settings icons from.
 */
void settings_view_icons_unload(
  struct Settings_view_icons* settings_view_icons);

/** @} // Settings_view_data */

#endif /*SETTINGS_VIEW_DATA_H*/
