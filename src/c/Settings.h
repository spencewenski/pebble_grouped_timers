#ifndef SETTINGS_H
#define SETTINGS_H

/**
 * @addtogroup Settings
 * @{
 *
 * Settings of the app or a {@link Group}.
 */

#include "Settings_fields.h"

struct Settings;

/**
 * Create a {@link Settings} object.
 *
 * @param  group_id Id of the {@link Group} the {@link Settings} are for, or
 *                  negative if these are the global app {@link Settings}.
 * @return          A new {@link Settings} object.
 */
struct Settings* settings_create(int group_id);

/**
 * Destroy the given {@link Settings} object.
 *
 * @param settings {@link Settings} object to destroy.
 */
void settings_destroy(struct Settings* settings);

/**
 * Load a {@link Settings} object from persistent storage.
 *
 * The {@link Settings Settings'} group id isn't saved to persistent storage, so
 * it must be provided here. Should be used when the app is starting to load
 * saved {@link Settings}.
 *
 * @param  group_id Id of the {@link Group} these {@link Settings} are for, or
 *                  negative if for the global app {@link Settings}.
 * @return          A {@link Settings} object loaded from persistent storage.
 */
struct Settings* settings_load(int group_id);

/**
 * Save the given {@link Settings} to persistent storage.
 *
 * @param settings {@link Settings} to save to persistent storage.
 */
void settings_save(const struct Settings* settings);

/**
 * Delete the {@link Settings} from persistent storage.
 *
 * @param settings {@link Settings} to delete from persistent storage.
 */
void settings_persist_delete(const struct Settings* settings);

/**
 * Broadcast the {@link Settings Settings'} state to the {@link Model}.
 *
 * @param settings  {@link Settings} to broadcast the state of.
 */
void settings_broadcast_state(const struct Settings* settings);

/**
 * Increment the given {@link Settings_field_t} by one.
 *
 * @param settings        {@link Settings} to edit.
 * @param settings_field  {@link Settings_field_t} to edit.
 */
void settings_increment_field(struct Settings* settings, Settings_field_t settings_field);

/**
 * Get the {@link Repeat_style_t} from the {@link Settings}.
 *
 * @param  settings {@link Settings} to get the {@link Repeat_style_t} from.
 * @return          The {@link Repeat_style_t} for the given {@link Settings}.
 */
Repeat_style_t settings_get_repeat_style(const struct Settings* settings);

/**
 * Get the {@link Progress_style_t} from the {@link Settings}.
 *
 * @param  settings {@link Settings} to get the {@link Progress_style_t} from.
 * @return          The {@link Progress_style_t} for the given {@link Settings}.
 */
Progress_style_t settings_get_progress_style(const struct Settings* settings);

/**
 * Get the {@link Vibrate_interval_t} from the {@link Settings}.
 *
 * @param  settings {@link Settings} to get the {@link Vibrate_interval_t} from.
 * @return          The {@link Vibrate_interval_t} for the given {@link Settings}.
 */
Vibrate_interval_t settings_get_vibrate_interval(const struct Settings* settings);

/**
 * Get the {@link Vibrate_style_t} from the {@link Settings}.
 *
 * @param  settings {@link Settings} to get the {@link Vibrate_style_t} from.
 * @return          The {@link Vibrate_style_t} for the given {@link Settings}.
 */
Vibrate_style_t settings_get_vibrate_style(const struct Settings* settings);

/** @} // Settings */

#endif /*SETTINGS_H*/
