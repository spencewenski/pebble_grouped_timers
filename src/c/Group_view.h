#ifndef GROUP_VIEW_H
#define GROUP_VIEW_H

/**
 * @addtogroup Group_view Group view
 * @{
 *
 * {@link View} implementation to display a {@link Group}.
 */

struct View;

/**
 * Create a {@link Group_view group view}.
 *
 * @param  group_id Id of the {@link Group} to display.
 * @return          The abstract {@link View} handle for the
 *                  {@link Group_view group view}.
 */
struct View* group_view_create(int group_id);

/** @} // Group_view */

#endif /*GROUP_VIEW_H*/
