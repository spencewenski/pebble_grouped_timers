#ifndef ADVANCED_SETTINGS_H
#define ADVANCED_SETTINGS_H

/**
 * @addtogroup Advanced_settings Advanced settings
 * @{
 *
 * Advanced app settings.
 */

#include "Advanced_settings_fields.h"

struct Advanced_settings;

/**
 * Create a {@link Advanced_settings} object.
 *
 * @return A new {@link Advanced_settings} object.
 */
struct Advanced_settings* advanced_settings_create();

/**
 * Destroy the given {@link Advanced_settings} object.
 *
 * @param settings {@link Advanced_settings} object to destroy.
 */
void advanced_settings_destroy(struct Advanced_settings* settings);

/**
 * Load a {@link Advanced_settings} object from persistent storage.
 *
 * @return A {@link Advanced_settings} object loaded from persistent storage.
 */
struct Advanced_settings* advanced_settings_load();

/**
 * Save the given {@link Advanced_settings} to persistent storage.
 *
 * @param settings {@link Advanced_settings} to save to persistent storage.
 */
void advanced_settings_save(const struct Advanced_settings* settings);

/**
 * Broadcast the {@link Advanced_settings Advanced settings'} state to the
 * {@link Model}.
 *
 * @param settings  {@link Advanced_settings} to broadcast the state of.
 */
void advanced_settings_broadcast_state(
  const struct Advanced_settings* settings);

/**
 * Increment the given {@link Advanced_settings_field_t} by one.
 *
 * @param settings        {@link Advanced_settings} to edit.
 * @param settings_field  {@link Advanced_settings_field_t} to edit.
 */
void advanced_settings_increment_field(struct Advanced_settings* settings,
  Advanced_settings_field_t settings_field);

/**
 * Get the {@link Extended_vibes_state_t} from the {@link Advanced_settings}.
 *
 * @param  settings {@link Advanced_settings} to get the
 *                  {@link Extended_vibes_state_t} from.
 * @return          The {@link Extended_vibes_state_t} for the given
 *                  {@link Advanced_settings}.
 */
Extended_vibes_state_t advanced_settings_get_extended_vibes_state(
  const struct Advanced_settings* settings);

/** @} // Advanced_settings */

#endif /*ADVANCED_SETTINGS_H*/
