/**
 * @internal
 * @addtogroup persist_utility
 * @{
 */

#include "persist_utility.h"
#include "assert.h"
#include "Modified_state.h"

#include <pebble.h>

/**
 * Key used for storing the {@link PERSIST_VERSION}.
 */
#define PERSIST_VERSION_KEY 0

/**
 * The current version of the persistent storage layout.
 *
 * This should be incremented when a breaking change is made to the storage
 * layout.
 */
#define PERSIST_VERSION 1

/**
 * The key used to save the maximum persist key that was used when saving the
 * app data.
 *
 * The value of the max persist key should be 1 greater than the actual max
 * persist key.
 */
#define MAX_PERSIST_KEY_KEY 1

/**
 * The maximum persist key used when saving data to the persistent storage.
 *
 * This value is saved using the {@link MAX_PERSIST_KEY_KEY} and used when
 * loading the app's data from persistent storage.
 */
static int s_max_save_persist_key = INVALID_PERSIST_KEY;

/**
 * The maximum persist key used when loading from persistent storage.
 *
 * If data has previously been saved, this value is read from
 * {@link MAX_PERSIST_KEY_KEY}
 */
static int s_max_load_persist_key = INVALID_PERSIST_KEY;

/**
 * The current persist key for loading objects.
 */
static int s_current_load_persist_key = INVALID_PERSIST_KEY;

/**
 * The current persist key for saving objects.
 */
static int s_current_save_persist_key = INVALID_PERSIST_KEY;

/**
 * The current modified state. If this is {@link MODIFIED_STATE_MULTIPLE},
 * it will override whatever value is passed to {@link persist_save} and force
 * the item to be saved even if the item's state is
 * {@link MODIFIED_STATE_NOT_MODIFIED}.
 */
static Modified_state_t s_modified_state = MODIFIED_STATE_NOT_MODIFIED;

/**
 * First persist key to use for saving the app's data.
 *
 * Values below {@link FIRST_PERSIST_KEY} are used to save data such as the
 * {@link PERSIST_VERSION_KEY} and {@link MAX_PERSIST_KEY_KEY}.
 */
#define FIRST_PERSIST_KEY 100

void persist_start_load()
{
  s_current_load_persist_key = FIRST_PERSIST_KEY;
  if (!persist_exists(MAX_PERSIST_KEY_KEY)) {
    s_max_load_persist_key = INVALID_PERSIST_KEY;
  } else {
    s_max_load_persist_key = persist_read_int(MAX_PERSIST_KEY_KEY);
  }
  s_modified_state = MODIFIED_STATE_NOT_MODIFIED;
}

void persist_start_save()
{
  persist_write_int(PERSIST_VERSION_KEY, PERSIST_VERSION);
  s_current_save_persist_key = FIRST_PERSIST_KEY;
  s_modified_state = MODIFIED_STATE_NOT_MODIFIED;
}

void persist_finish_load()
{
  assert(s_max_load_persist_key == INVALID_PERSIST_KEY || s_current_load_persist_key == s_max_load_persist_key);
  s_modified_state = MODIFIED_STATE_NOT_MODIFIED;
}

void persist_finish_save()
{
  s_max_save_persist_key = s_current_save_persist_key;
  persist_write_int(MAX_PERSIST_KEY_KEY, s_max_save_persist_key);
  // Delete unused persist keys
  for (int i = s_max_save_persist_key; i < s_max_load_persist_key; ++i) {
    persist_delete(i);
  }
  s_modified_state = MODIFIED_STATE_NOT_MODIFIED;
}

int next_persist_exists() {
  return persist_exists(s_current_load_persist_key) ? 0 : 1;
}

int persist_load(void* buffer, int buffer_size)
{
  assert(persist_exists(s_current_load_persist_key));
  persist_read_data(s_current_load_persist_key, buffer, buffer_size);
  return s_current_load_persist_key++;
}

void persist_save(void* buffer, int buffer_size,
  const Modified_state_t modified_state)
{
  // We always increment the persist key
  const int persist_key = s_current_save_persist_key++;
  // If the modified state is MODIFIED_STATE_MULTIPLE, we want to save this item
  // and everything that comes after this item.
  if (modified_state == MODIFIED_STATE_MULTIPLE) {
    s_modified_state = modified_state;
  }
  // Save the item if it's been modified, or if a previous item had modified
  // state MODIFIED_STATE_MULTIPLE
  if (s_modified_state == MODIFIED_STATE_MULTIPLE ||
      modified_state == MODIFIED_STATE_SINGLE) {
    persist_write_data(persist_key, buffer, buffer_size);
  }
}

int persist_get_current_version()
{
  return PERSIST_VERSION;
}

int persist_get_saved_version()
{
  if (!persist_exists(PERSIST_VERSION_KEY)) {
    return -1;
  }
  return persist_read_int(PERSIST_VERSION_KEY);
}

/** @} // persist_utility */
