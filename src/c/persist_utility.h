#ifndef PERSIST_UTILITY_H
#define PERSIST_UTILITY_H

/**
 * @addtogroup persist_utility Persist Utility
 * @{
 *
 * Utilities to use for loading and saving to persistent storage.
 */

#include "Modified_state.h"

/**
 * Value used to indicate that a persist key is invalid.
 */
#define INVALID_PERSIST_KEY -1

/**
 * Initialize persistence variables.
 *
 * Should be called before app data is loaded.
 */
void persist_start_load();

/**
 * Initialize persistence variables.
 *
 * Should be called before app data is saved.
 */
void persist_start_save();

/**
 * Finish the app load from persistent storage.
 *
 * Should be called after app data is loaded.
 */
void persist_finish_load();

/**
 * Finish the app save to persistent storage.
 *
 * Should be called after app data is saved.
 */
void persist_finish_save();

/**
 * Check if the next persist key exists.
 *
 * @return Zero if the next persist key exists, non-zero otherwise.
 */
int next_persist_exists();

/**
 * Load the data in the next persist key into the given buffer.
 *
 * @param  buffer      Buffer to load the data into.
 * @param  buffer_size Size of the buffer.
 * @return             The persist key that was used to load the data.
 */
int persist_load(void* buffer, int buffer_size);

/**
 * Save given data into the next persist key.
 *
 * @param buffer          Buffer to save to persistent storage.
 * @param buffer_size     Size of the buffer.
 * @param modified_state  The modified state of the item in the provided buffer.
 */
void persist_save(void* buffer, int buffer_size,
  const Modified_state_t modified_state);

/**
 * Get the current persist version.
 *
 * @return The current persist version.
 */
int persist_get_current_version();

/**
 * Get the persist version that data was last saved with, or negative if no data
 * has been saved
 */
int persist_get_saved_version();

/** @} // persist_utility */

#endif /*PERSIST_UTILITY_H*/
