/**
 * @internal
 * @addtogroup Model
 * @{
 */

#include "Model.h"
#include "List.h"
#include "Utility.h"
#include "assert.h"
#include "View.h"
#include "Settings.h"
#include "Settings_fields.h"
#include "Group.h"
#include "Timer.h"
#include "Timer_state.h"
#include "persist_utility.h"
#include "globals.h"
#include "vibrate_utility.h"
#include "Controller.h"
#include "Vibrate_manager.h"
#include "Modified_state.h"
#include "Advanced_settings.h"

/**
 * The value to give to the {@link Model Model's} {@link Settings} to indicate
 * that the {@link Settings} are the app settings and not {@link Group}
 * settings.
 */
#define MODEL_GROUP_ID -1

/**
 * Data used by the {@link Model}.
 */
struct Model {
  /**
   * {@link View Views} that are attached to the {@link Model}.
   *
   * The {@link View Views} are owned by the {@link Controller}.
   */
  struct List* views;
  /**
   * {@link Group Groups} that have been added to the {@link Model}.
   */
  struct List* groups;
  /**
   * The global app {@link Settings}.
   */
  struct Settings* settings;
  /**
   * The {@link Advanced_settings} for the app.
   */
  struct Advanced_settings* advanced_settings;
  /**
   * Handle for the Pebble app timer used to update the {@link Timer Timers}
   * in the {@link Model} every second.
   */
  AppTimer* app_timer_handle;
  /**
   * The modified state of the {@link Model}'s data.
   */
  Modified_state_t modified_state;
};
/**
 * The singleton {@link Model} instance.
 */
static struct Model* s_model;

/**
 * Data required to save the {@link Model} to persistent storage.
 */
struct Model_save_data {
  /**
   * The number of {@link Group Groups} that exist in the app.
   */
  int num_groups;
};

/**
 * Create a {@link Model} object.
 *
 * @return A new {@link Model} object.
 */
static struct Model* model_create();

/**
 * Destroy the {@link Model} object.
 *
 * @param model The {@link Model} object to destroy.
 */
static void model_destroy(struct Model* model);

/**
 * Load the app's data from persistent storage.
 */
static void model_load();

/**
 * Save the app's data to persistent storage.
 */
static void model_save();

/**
 * Update all the {@link Timer Timers} in the app.
 */
static void model_update();

/**
 * Start the Pebble app timer used to update the app's {@link Timer Timers}
 * every second.
 *
 * If the app timer is already running, or if no {@link Timer Timers} are
 * running, do nothing.
 */
static void model_start_app_timer();

/**
 * Function to give to the Pebble app timer to update the app every second.
 *
 * @param data Unused.
 */
static void model_app_timer_handler(void* data);

/**
 * Signature of a function used to get an object by its id.
 *
 * @param  id Id of the object to get.
 * @return    The object with the given id.
 */
typedef void* (*Model_get_by_id_fp_t) (int id);

/**
 * Get the next available id.
 *
 * @param  func_ptr {@link Model_get_by_id_fp_t} implementation used to check
 *                  if an id is already in use.
 * @return          The next available id.
 */
static int get_next_id(Model_get_by_id_fp_t func_ptr);

/**
 * Signature of a function to apply an operation on a {@link View}.
 *
 * @param  view {@link View} to apply the operation to.
 */
typedef void (*Model_apply_to_view_fp_t) (struct View* view);

/**
 * Apply the given {@link Model_apply_to_view_fp_t} implemenation to all the
 * {@link View Views}.
 *
 * @param func_ptr {@link Model_apply_to_view_fp_t} implemenation to all the
 * {@link View Views}.
 */
static void model_apply_to_views(Model_apply_to_view_fp_t func_ptr);

/**
 * Get the id of the {@link Group} the {@link Timer} belongs to.
 *
 * @param  timer_id Id of the {@link Timer} to get the {@link Group} id for.
 * @return          The id of the {@link Group} the {@link Timer} belongs to, or
 *                  negative if the {@link Timer} doesn't exist.
 */
static int get_group_id(int timer_id);

/**
 * Get the number of {@link Timer Timers} that are currently running.
 *
 * @return The number of {@link Timer Timers} that are currently running.
 */
static int num_timers_running();

/**
 * Handle the {@link Timer} elapsing.
 *
 * Vibrate and open the {@link Countdown_view} for the {@link Timer}.
 *
 * @param group {@link Group} that the elapsed {@link Timer} belongs to.
 * @param timer {@link Timer} that elapsed.
 */
static void model_handle_timer_elapsed(struct Group* group,
  struct Timer* timer);

/**
 * Schedule an app wakeup to start the app when the next {@link Timer} will
 * elapse.
 */
static void model_schedule_wakeup();

/**
 * Handle an app wakeup.
 *
 * If the wakeup has a {@link Timer} id, vibrate and open the
 * {@link Countdown_view} for the {@link Timer}. Should be called once in
 * {@link model_start}.
 */
static void model_handle_wakeup();

/**
 * Display time until next wakeup in menu row for the app.
 *
 * Should be called once when the app is closing.
 *
 * Implements the AppGlanceReloadCallback.
 *
 * @param session The AppGlanceReloadSession for this update.
 * @param limit   Number of entries that can be added to the app's glance.
 * @param context Timestamp of next wakeup in seconds since the epoch, or NULL
 *                if no wakeup is scheduled
 */
static void update_app_glance_callback(AppGlanceReloadSession *session,
  size_t limit, void *context);

int model_get_next_group_id()
{
  return get_next_id((Model_get_by_id_fp_t)model_get_group);
}

void model_add_group(struct Group* group)
{
  assert(s_model);
  assert(group);
  list_add(s_model->groups, group);
  group_broadcast_state(group);
  // We have data after groups, so if we add a group we need to resave
  // everything.
  s_model->modified_state = MODIFIED_STATE_MULTIPLE;
}

void model_remove_group(struct Group* group)
{
  assert(s_model);
  assert(group);
  model_notify_group_gone(group_get_id(group));
  const int index = list_find_ptr_index(s_model->groups, group);
  list_remove(s_model->groups, index);
  // We have data after groups, so if we remove a group we need to resave
  // everything.
  s_model->modified_state = MODIFIED_STATE_MULTIPLE;
}

struct Group* model_get_group(int group_id)
{
  assert(s_model);
  assert(group_id >= 0);
  return list_find_arg(s_model->groups, &group_id,
    (List_compare_arg_fp_t)group_compare_id);
}

int model_get_next_timer_id()
{
  return get_next_id((Model_get_by_id_fp_t)model_get_timer);
}

static int get_next_id(Model_get_by_id_fp_t func_ptr)
{
  assert(s_model);
  assert(func_ptr);
  int new_id = rand();
  while (func_ptr(new_id)) {
    new_id = rand();
  }
  return new_id;
}

void model_add_timer(struct Timer* timer, int group_id)
{
  assert(s_model);
  assert(timer);
  assert(group_id >= 0);
  struct Group* group = model_get_group(group_id);
  group_add_timer(group, timer);
  timer_broadcast_state(timer);
}

void model_remove_timer(struct Timer* timer)
{
  assert(s_model);
  assert(timer);
  model_notify_timer_gone(timer_get_id(timer));
  for (int i = 0; i < list_size(s_model->groups); ++i) {
    group_remove_timer(list_get(s_model->groups, i), timer);
  }
}

struct Timer* model_get_timer(int timer_id)
{
  assert(s_model);
  assert(timer_id >= 0);
  for (int i = 0; i < list_size(s_model->groups); ++i) {
    struct Timer* timer =
      group_get_timer_by_id(list_get(s_model->groups, i), timer_id);
    if (timer) {
      return timer;
    }
  }
  return NULL;
}

struct Timer* model_get_next_timer(int timer_id)
{
  assert(s_model);
  assert(timer_id >= 0);
  int group_id = get_group_id(timer_id);
  struct Group* group = model_get_group(group_id);
  return group_get_next_timer(group, timer_id);
}

struct Settings* model_get_settings()
{
  assert(s_model);
  return s_model->settings;
}

struct Advanced_settings* model_get_advanced_settings()
{
  assert(s_model);
  return s_model->advanced_settings;
}

void model_notify_duration(int timer_id, int duration)
{
  assert(s_model);
  assert(timer_id >= 0);
  int group_id = get_group_id(timer_id);
  assert(group_id >= 0);
  for (int i = 0; i < list_size(s_model->views); ++i) {
    view_update_duration(list_get(s_model->views, i), group_id, timer_id,
      duration);
  }
}

void model_notify_remaining(int timer_id, int remaining)
{
  assert(s_model);
  assert(timer_id >= 0);
  for (int i = 0; i < list_size(s_model->views); ++i) {
    view_update_remaining(list_get(s_model->views, i), timer_id, remaining);
  }
}

void model_notify_timer_gone(int timer_id)
{
  assert(s_model);
  assert(timer_id >= 0);
  for (int i = 0; i < list_size(s_model->views); ++i) {
    view_update_remove_timer(list_get(s_model->views, i), timer_id);
  }
}

void model_notify_timer_state(int timer_id, Timer_state_t timer_state)
{
  assert(s_model);
  assert(timer_id >= 0);
  for (int i = 0; i < list_size(s_model->views); ++i) {
    view_update_timer_state(list_get(s_model->views, i), timer_id, timer_state);
  }
  model_start_app_timer();
}

static int get_group_id(int timer_id)
{
  assert(s_model);
  assert(timer_id >= 0);
  for (int i = 0; i < list_size(s_model->groups); ++i) {
    struct Group* group = list_get(s_model->groups, i);
    struct Timer* timer = group_get_timer_by_id(group, timer_id);
    if (timer) {
      return group_get_id(group);
    }
  }
  return -1;
}

void model_notify_group_exists(int group_id)
{
  assert(s_model);
  assert(group_id >= 0);
  for (int i = 0; i < list_size(s_model->views); ++i) {
    view_update_group_exists(list_get(s_model->views, i), group_id);
  }
}

void model_notify_group_gone(int group_id)
{
  assert(s_model);
  assert(group_id >= 0);
  for (int i = 0; i < list_size(s_model->views); ++i) {
    view_update_remove_group(list_get(s_model->views, i), group_id);
  }
}

void model_notify_settings(int group_id, Repeat_style_t repeat_style,
  Progress_style_t progress_style, Vibrate_interval_t vibrate_interval,
  Vibrate_style_t vibrate_style)
{
  assert(s_model);
  for (int i = 0; i < list_size(s_model->views); ++i) {
    view_update_settings(list_get(s_model->views, i), group_id, repeat_style,
      progress_style, vibrate_interval, vibrate_style);
  }
}

void model_notify_advanced_settings(
  Extended_vibes_state_t extended_vibes_state)
{
  assert(s_model);
  for (int i = 0; i < list_size(s_model->views); ++i) {
    view_update_advanced_settings(list_get(s_model->views, i),
      extended_vibes_state);
  }
}

void model_notify_timer_tracking(int timer_id, int previous_timer_id)
{
  assert(s_model);
  for (int i = 0; i < list_size(s_model->views); ++i) {
    view_update_timer_tracking(list_get(s_model->views, i), timer_id,
      previous_timer_id);
  }
}

void model_notify_error(char* msg)
{
  assert(s_model);
  for (int i = 0; i < list_size(s_model->views); ++i) {
    view_update_error(list_get(s_model->views, i), msg);
  }
}

static void model_apply_to_views(Model_apply_to_view_fp_t func_ptr)
{
  assert(s_model);
  list_for_each(s_model->views, (List_for_each_fp_t) func_ptr);
}

void model_attach_view(struct View* view)
{
  assert(s_model);
  assert(view);
  if (list_find_ptr(s_model->views, view)) {
    return;
  }
  list_add(s_model->views, view);
  model_apply_to_views(view_start_update);
  list_for_each(s_model->groups, (List_for_each_fp_t)group_broadcast_state);
  settings_broadcast_state(s_model->settings);
  advanced_settings_broadcast_state(s_model->advanced_settings);
  // The data didn't change, and the new view will be loaded when it is pushed,
  // so the views don't need to be refreshed.
  model_apply_to_views(view_finish_update_no_refresh);
}

void model_detach_view(struct View* view)
{
  assert(s_model);
  assert(view);
  list_remove_ptr(s_model->views, view);
}

static void model_start_app_timer()
{
  assert(s_model);
  // App timer already running
  if (s_model->app_timer_handle) {
    return;
  }
  // No Timers running
  if (num_timers_running() <= 0) {
    return;
  }
  s_model->app_timer_handle = app_timer_register(MS_PER_SECOND,
    model_app_timer_handler, NULL);
}

static void model_app_timer_handler(void* data)
{
  assert(s_model);
  s_model->app_timer_handle = NULL;
  model_update();
  model_start_app_timer();
}

static int num_timers_running()
{
  assert(s_model);
  int count = 0;
  for (int i = 0; i < list_size(s_model->groups); ++i) {
    struct Group* group = list_get(s_model->groups, i);
    for (int j = 0; j < group_size(group); ++j) {
      struct Timer* timer = group_get_timer(group, j);
      Timer_state_t timer_state = timer_get_state(timer);
      if (timer_state == TIMER_STATE_STARTED ||
          timer_state == TIMER_STATE_ELAPSED) {
        ++count;
      }
    }
  }
  return count;
}

static void model_update()
{
  assert(s_model);
  model_apply_to_views(view_start_update);
  for (int i = 0; i < list_size(s_model->groups); ++i) {
    struct Group* group = list_get(s_model->groups, i);
    for (int j = 0; j < group_size(group); ++j) {
      struct Timer* timer = group_get_timer(group, j);
      timer_update(timer);
      model_handle_timer_elapsed(group, timer);
    }
  }
  model_apply_to_views(view_finish_update);
}

static void model_handle_timer_elapsed(struct Group* group,
  struct Timer* timer)
{
  assert(s_model);
  assert(group);
  assert(timer);

  const Timer_state_t timer_state = timer_get_state(timer);
  if (timer_state != TIMER_STATE_ELAPSED) {
    return;
  }
  vibrate_manager_vibrate(group, timer);

  const struct Settings* settings = group_get_settings(group);
  if (settings_get_progress_style(settings) == PROGRESS_STYLE_AUTO) {
    controller_next_timer(group_get_id(group), timer_get_id(timer));
  }
}

void model_start()
{
  s_model = model_create();
  model_load();
  vibrate_manager_init();
  model_handle_wakeup();
  // Now that the wakeup has been handled, cancel all wakeups
  wakeup_cancel_all();
  model_update();
  model_start_app_timer();
}

static void model_handle_wakeup()
{
  assert(s_model);
  if (launch_reason() != APP_LAUNCH_WAKEUP) {
    return;
  }
  WakeupId wakeup_id = 0;
  int32_t timer_id = 0;
  wakeup_get_launch_event(&wakeup_id, &timer_id);
  if (timer_id < 0) {
    return;
  }
  // If we were launched with a wakeup and a timer_id, that means we need
  // to vibrate and show the countdown window
  int group_id = get_group_id(timer_id);
  vibrate_manager_vibrate_immediate(
    settings_get_vibrate_style(group_get_settings(model_get_group(group_id))));
  controller_open_countdown_view(group_id, timer_id);
}

void model_stop()
{
  assert(s_model);
  // Cancel the app timer if one is running
  if (s_model->app_timer_handle) {
    app_timer_cancel(s_model->app_timer_handle);
  }
  model_schedule_wakeup();
  vibrate_manager_uninit();
  model_save();
  model_destroy(s_model);
  s_model = NULL;
}

static void model_schedule_wakeup()
{
  assert(s_model);
  // Find the next time a timer will elapse and/or vibrate
  int min_next_time = -1;
  int timer_id = -1;
  for (int i = 0; i < list_size(s_model->groups); ++i) {
    struct Group* group = list_get(s_model->groups, i);
    for (int j = 0; j < group_size(group); ++j) {
      struct Timer* timer = group_get_timer(group, j);
      Timer_state_t timer_state = timer_get_state(timer);
      if (timer_state == TIMER_STATE_NOT_STARTED ||
          timer_state == TIMER_STATE_PAUSED) {
        continue;
      }
      int next_time = min_next_time;
      if (timer_state == TIMER_STATE_STARTED) {
        next_time = timer_get_remaining_seconds(timer);
      } else if (timer_state == TIMER_STATE_ELAPSED) {
        next_time = seconds_until_next_vibrate(group, timer);
      }
      if (next_time <= 0) {
        continue;
      }
      if (min_next_time < 0 || next_time < min_next_time) {
        min_next_time = next_time;
        timer_id = timer_get_id(timer);
      }
    }
  }
  if (min_next_time <= 0) {
    app_glance_reload(update_app_glance_callback, NULL);
    return;
  }
  int wakeup_time = time(NULL) + min_next_time;
  app_glance_reload(update_app_glance_callback, &wakeup_time);
  wakeup_schedule(wakeup_time, timer_id, false);
}

static void update_app_glance_callback(AppGlanceReloadSession *session,
  size_t limit, void *context)
{
  // This should never happen, but developers should always ensure they are
  // not adding more slices than are available
  if (limit < 1) return;

  if (!context) {
    return;
  }
  int wakeup_time = *((int*) context);

  // Create the subtitle template string
  const int SUBTITLE_TEMPLATE_BUF_LENGTH = 100;
  char template_string[SUBTITLE_TEMPLATE_BUF_LENGTH];
  template_string[0] = '\0';
  snprintf(template_string, SUBTITLE_TEMPLATE_BUF_LENGTH,
    "{time_until(%d)|format(>=1M:'%%T',>0S:':%%0S')}", wakeup_time);

  // Create the AppGlanceSlice
  const AppGlanceSlice entry = (AppGlanceSlice) {
    .layout = {
      .icon = APP_GLANCE_SLICE_DEFAULT_ICON,
      .subtitle_template_string = template_string
    },
    .expiration_time = wakeup_time
  };

  const AppGlanceResult result = app_glance_add_slice(session, entry);
  if (result != APP_GLANCE_RESULT_SUCCESS) {
    APP_LOG(APP_LOG_LEVEL_ERROR, "AppGlance Error: %d", result);
  }
}

static void model_load()
{
  assert(s_model);
  persist_start_load();
  int persist_version = persist_get_saved_version();
  if (persist_version < 0) {
    APP_LOG(APP_LOG_LEVEL_INFO, "No save data exists, not loading.");
    return;
  }
  if (persist_version != persist_get_current_version()) {
    APP_LOG(APP_LOG_LEVEL_INFO,
      "Persist version changed from %d to %d, not loading.",
      persist_get_current_version(),
      persist_version);
    return;
  }
  struct Model_save_data model_save_data;
  persist_load(&model_save_data, sizeof(struct Model_save_data));
  // Load settings
  settings_destroy(s_model->settings);
  s_model->settings = settings_load(MODEL_GROUP_ID);
  // Load groups
  for (int i = 0; i < model_save_data.num_groups; ++i) {
    list_add(s_model->groups, group_load());
  }
  // Load advanced settings
  advanced_settings_destroy(s_model->advanced_settings);
  s_model->advanced_settings = advanced_settings_load();
  // The model has been loaded, it doesn't need to be saved until it is
  // modified.
  s_model->modified_state = MODIFIED_STATE_NOT_MODIFIED;
  persist_finish_load();
}

static void model_save()
{
  assert(s_model);
  persist_start_save();
  struct Model_save_data model_save_data;
  model_save_data.num_groups = list_size(s_model->groups);
  persist_save(&model_save_data, sizeof(struct Model_save_data),
    s_model->modified_state);
  settings_save(s_model->settings);
  for (int i = 0; i < list_size(s_model->groups); ++i) {
    group_save(list_get(s_model->groups, i));
  }
  advanced_settings_save(s_model->advanced_settings);
  persist_finish_save();
}

static struct Model* model_create()
{
  struct Model* model = (struct Model*) safe_alloc(sizeof(struct Model));
  model->views = list_create();
  model->groups = list_create();
  model->settings = settings_create(MODEL_GROUP_ID);
  model->advanced_settings = advanced_settings_create();
  model->app_timer_handle = NULL;
  // The model hasn't been loaded, assume it needs to be saved.
  model->modified_state = MODIFIED_STATE_SINGLE;
  return model;
}

static void model_destroy(struct Model* model)
{
  assert(model);
  list_destroy(model->views);
  list_for_each(model->groups, (List_for_each_fp_t)group_destroy);
  list_destroy(model->groups);
  settings_destroy(model->settings);
  advanced_settings_destroy(model->advanced_settings);
  free(model);
}

/** @} // Model */
