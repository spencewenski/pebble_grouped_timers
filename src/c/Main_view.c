/**
 * @internal
 * @addtogroup Main_view
 * @{
 */

#include "Main_view.h"
#include "View.h"
#include "Utility.h"
#include "List.h"
#include "assert.h"
#include "draw_utility.h"
#include "Group.h"
#include "globals.h"
#include "Controller.h"
#include "timer_utility.h"
#include "Timer_state.h"
#include "View_interface.h"
#include "locale_framework/src/localize.h"

#ifndef NDEBUG
// Only needed for the {@link create_test_data} method.
#include "Settings_fields.h"
#endif /* NDEBUG */

#include <pebble.h>

/**
 * Number of menu sections in the {@link Main_view}.
 */
#define MAIN_MENU_NUM_SECTIONS 2

/**
 * Size of the buffer to use to store the list of {@link Timer} text strings
 * for the {@link Group} subtitles.
 */
#define SUBTITLE_TEXT_BUFFER_LENGTH 100

/**
 * The number rows in the app options section. Value depends on if this is a
 * debug or a release build.
 */
#define SETTINGS_NUM_ROWS SETTINGS_NUM_ROWS_IMPL
#ifdef NDEBUG
#define SETTINGS_NUM_ROWS_IMPL 4
#else
#define SETTINGS_NUM_ROWS_IMPL 5
#endif /* NDEBUG */

/**
 * @internal
 * @addtogroup Main_view_interface_impl
 * @{
 *
 * Implementations of the {@link View_interface} for the {@link Group_view}.
 */

/** {@link view_push_fp_t} */
static void main_view_push(void* data);
/** {@link view_destroy_fp_t} */
static void main_view_destroy(void* data);
/** {@link view_update_duration_fp_t} */
static int main_view_update_duration(void* data, int group_id, int timer_id,
  int duration);
/** {@link view_update_remaining_fp_t} */
static int main_view_update_remaining(void* data, int timer_id, int remaining);
/** {@link view_update_remove_timer_fp_t} */
static int main_view_update_remove_timer(void* data, int timer_id);
/** {@link view_update_timer_state_fp_t} */
static int main_view_update_timer_state(void* data, int timer_id,
  Timer_state_t timer_state);
/** {@link view_update_group_exists_fp_t} */
static int main_view_update_group_exists(void* data, int group_id);
/** {@link view_update_remove_group_fp_t} */
static int main_view_update_remove_group(void* data, int group_id);
/** {@link view_refresh_fp_t} */
static void main_view_refresh(void* data);

/** @} // Main_view_interface_impl */

#ifndef NDEBUG
/**
 * Create test data.
 *
 * Mostly just so I don't have to keep re-creating the {@link Timer Timers} that
 * I like to use.
 */
void create_test_data();
#endif /*NDEBUG*/

/**
 * Data required for the {@link Main_view} to display a {@link Timer}.
 */
struct Timer_data {
  /**
   * Id of the {@link Timer}.
   */
  int timer_id;
  /**
   * Duration of the {@link Timer} in seconds.
   */
  int duration;
  /**
   * Amount of time remaining for the {@link Timer} in seconds.
   */
  int remaining;
  /**
   * Current {@link Timer_state} of the {@link Timer}.
   */
  Timer_state_t timer_state;
};

/**
 * Create a {@link Timer_data} object.
 *
 * @param  timer_id Id of the {@link Timer}.
 * @return          A new {@link Timer_data} object.
 */
static struct Timer_data* timer_data_create(int timer_id);

/**
 * Destroy the {@link Timer_data} object.
 *
 * @param timer_data {@link Timer_data} object to destroy.
 */
static void timer_data_destroy(struct Timer_data* timer_data);

/**
 * Compare the given {@link Timer} id to the one in the given
 * {@link Timer_data}.
 *
 * @param  timer_id_ptr     {@link Timer} id to comapre to the id in the
 *                          {@link Timer_data}.
 * @param  timer_data_other {@link Timer_data} that contains the id to compare
 *                          to the given {@link Timer} id.
 * @return                  Zero if the ids are equal, positive if the given
 *                          {@link Timer} id is greater than the id in the
 *                          {@link Timer_data}, negative otherwise.
 */
static int timer_data_compare_id(const int* timer_id_ptr,
  const struct Timer_data* timer_data_other);

/**
 * Data required for the {@link Main_view} to display a {@link Group}.
 */
struct Group_data {
  /**
   * {@link Timer_data} for the {@link Group}.
   */
  struct List* timer_data;
  /**
   * Id of the {@link Group}.
   */
  int group_id;
};

/**
 * Create a {@link Group_data} object.
 *
 * @param  group_id Id of the {@link Group}.
 * @return          A new {@link Group_data} object.
 */
static struct Group_data* group_data_create(int group_id);

/**
 * Destroy the {@link Group_data} object.
 *
 * @param group_data {@link Group_data} object to destroy.
 */
static void group_data_destroy(struct Group_data* group_data);

/**
 * Add a {@link Timer_data} to the {@link Group_data}.
 *
 * @param group_data {@link Group_data} to add the {@link Timer_data} to.
 * @param timer_data {@link Timer_data} to add to the {@link Group_data}.
 */
static void group_data_add_timer(struct Group_data* group_data,
  struct Timer_data* timer_data);

/**
 * Remove the {@link Timer_data} from the {@link Group_data}.
 *
 * @param group_data {@link Group_data} to remove the {@link Timer_data} from.
 * @param timer_id   Id of the {@link Timer_data} to remove from the
 *                   {@link Group_data}.
 */
static void group_data_remove_timer(struct Group_data* group_data,
  int timer_id);

/**
 * Get the {@link Timer_data} from the {@link Group_data}.
 *
 * @param  group_data {@link Group_data} to get the {@link Timer_data} from.
 * @param  timer_id   Id of the {@link Timer} to get the {@link Timer_data} for.
 * @return            {@link Timer_data} for the {@link Timer} with the given
 *                    id.
 */
static struct Timer_data* group_data_get_timer(
  const struct Group_data* group_data, int timer_id);

/**
 * Compare the given {@link Group} id to the one in the given
 * {@link Group_data}.
 *
 * @param  group_id_ptr     {@link Group} id to comapre to the id in the
 *                          {@link Group_data}.
 * @param  group_data_other {@link Group_data} that contains the id to compare
 *                          to the given {@link Group} id.
 * @return                  Zero if the ids are equal, positive if the given
 *                          {@link Group} id is greater than the id in the
 *                          {@link Group_data}, negative otherwise.
 */
static int group_data_compare_id(const int* group_id_ptr,
  const struct Group_data* group_data_other);

/**
 * Data used by the {@link Main_view}.
 */
struct Main_view {
  /**
   * Pebble window for this view.
   */
  Window* window;
  /**
   * Pebble menu layer to display the {@link Group Group's} data.
   */
  MenuLayer* menu_layer;
  /**
   * Pebble status bar layer to display the time at the top of the screen.
   */
  StatusBarLayer* status_bar_layer;
  /**
   * Parent {@link View} object for this {@link Main_view}.
   */
  struct View* view;
  /**
   * {@link Group_data} for the {@link Group Group's} in the .
   */
  struct List* group_data;
};

/**
 * Get the {@link Timer_data} from the {@link Main_view}.
 *
 * @param  main_view  {@link Main_view} to get the {@link Timer_data} from.
 * @param  timer_id   Id of the {@link Timer} to get the {@link Timer_data} for.
 * @return            {@link Timer_data} for the {@link Timer} with the given
 *                    id.
 */
static struct Timer_data* main_view_get_timer(const struct Main_view* main_view,
  int timer_id);

/**
 * Get the {@link Group_data} from the {@link Main_view}.
 *
 * @param  main_view  {@link Main_view} to get the {@link Group_data} from.
 * @param  group_id   Id of the {@link Group} to get the {@link Group_data} for.
 * @return            {@link Group_data} for the {@link Group} with the given
 *                    id.
 */
static struct Group_data* main_view_get_group(const struct Main_view* main_view,
  int group_id);

/**
 * Draw a row of the {@link Main_view}.
 *
 * @param ctx        Pebble graphics context to use to draw the row.
 * @param cell_layer Pebble layer to use to draw the row.
 * @param row_index  Index of the row.
 * @param data       {@link Main_view} object used to draw the row.
 */
static void main_view_draw_row(GContext* ctx, const Layer* cell_layer,
  uint16_t row_index, void* data);

/**
 * Get the text for a subtitle of a row of the {@link Main_view}.
 *
 * The subtitle is a list of the {@link Timer Timers} in the {@link Group}
 *
 * @param buf        Buffer to write the text to.
 * @param buf_size   Size of the buffer.
 * @param group_data {@link Group_data} to use to create the subtitle.
 */
static void get_subtitle_text(char* buf, int buf_size,
  const struct Group_data* group_data);

// WindowHandlers
static void window_load_handler(Window* window);
static void window_appear_handler(Window* window);
static void window_unload_handler(Window* window);

// MenuLayerCallbacks
static uint16_t menu_get_num_sections_callback(MenuLayer* menu_layer,
  void* data);
static uint16_t menu_get_num_rows_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data);
static void menu_draw_row_callback(GContext* ctx, const Layer* cell_layer,
  MenuIndex* cell_index, void* data);
static void menu_draw_header_callback(GContext* ctx, const Layer* cell_layer,
  uint16_t section_index, void* data);
static void menu_select_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data);
static void menu_select_long_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data);

struct View* main_view_create()
{
  struct Main_view* main_view = safe_alloc(sizeof(struct Main_view));
  main_view->group_data = list_create();
  main_view->window = NULL;
  main_view->menu_layer = NULL;
  main_view->status_bar_layer = NULL;
  struct View_interface view_interface = (struct View_interface) {
    .destroy = main_view_destroy,
    .update_duration = main_view_update_duration,
    .update_remaining = main_view_update_remaining,
    .update_remove_timer = main_view_update_remove_timer,
    .update_timer_state = main_view_update_timer_state,
    .update_group_exists = main_view_update_group_exists,
    .update_remove_group = main_view_update_remove_group,
    .refresh = main_view_refresh,
    .push = main_view_push
  };
  main_view->view = view_create(main_view, &view_interface);
  return main_view->view;
}

static void main_view_destroy(void* data)
{
  assert(data);
  struct Main_view* main_view = (struct Main_view*) data;
  list_for_each(main_view->group_data, (List_for_each_fp_t)group_data_destroy);
  list_destroy(main_view->group_data);
  free(main_view);
}

static int main_view_update_duration(void* data, int group_id, int timer_id,
  int duration)
{
  assert(data);
  assert(timer_id >= 0);
  assert(duration >= 0);
  struct Main_view* main_view = (struct Main_view*) data;
  struct Timer_data* timer_data = main_view_get_timer(main_view, timer_id);
  // If the view doesn't have this timer yet, create the timer
  if (!timer_data) {
    timer_data = timer_data_create(timer_id);
    struct Group_data* group_data = main_view_get_group(main_view, group_id);
    group_data_add_timer(group_data, timer_data);
  }
  timer_data->duration = duration;
  timer_data->remaining = duration;
  return 1;
}

static int main_view_update_remaining(void* data, int timer_id, int remaining)
{
  assert(data);
  assert(timer_id >= 0);
  struct Main_view* main_view = (struct Main_view*) data;
  struct Timer_data* timer_data = main_view_get_timer(main_view, timer_id);
  assert(timer_data);
  timer_data->remaining = remaining;
  return 1;
}

static int main_view_update_remove_timer(void* data, int timer_id)
{
  assert(data);
  assert(timer_id >= 0);
  struct Main_view* main_view = (struct Main_view*) data;
  for (int i = 0; i < list_size(main_view->group_data); ++i) {
    struct Timer_data* timer_data =
      group_data_get_timer(list_get(main_view->group_data, i), timer_id);
    if (timer_data) {
      group_data_remove_timer(list_get(main_view->group_data, i), timer_id);
      timer_data_destroy(timer_data);
      return 1;
    }
  }
  return 0;
}

static int main_view_update_timer_state(void* data, int timer_id,
  Timer_state_t timer_state)
{
  assert(data);
  struct Main_view* main_view = (struct Main_view*) data;
  struct Timer_data* timer_data = main_view_get_timer(main_view, timer_id);
  assert(timer_data);
  timer_data->timer_state = timer_state;
  return 1;
}

static int main_view_update_group_exists(void* data, int group_id)
{
  assert(data);
  assert(group_id >= 0);
  struct Main_view* main_view = (struct Main_view*) data;
  struct Group_data* group_data = main_view_get_group(main_view, group_id);
  // If the group is already in the view, do nothing
  if (group_data) {
    return 0;
  }
  group_data = group_data_create(group_id);
  list_add(main_view->group_data, group_data);
  return 1;
}

static int main_view_update_remove_group(void* data, int group_id)
{
  assert(data);
  assert(group_id >= 0);
  struct Main_view* main_view = (struct Main_view*) data;
  struct Group_data* group_data = main_view_get_group(main_view, group_id);
  assert(group_data);
  list_remove_ptr(main_view->group_data, group_data);
  group_data_destroy(group_data);
  return 1;
}

static void main_view_refresh(void* data)
{
  assert(data);
  struct Main_view* main_view = (struct Main_view*) data;
  assert(main_view->menu_layer);
  menu_layer_reload_data(main_view->menu_layer);
}

static struct Timer_data* main_view_get_timer(const struct Main_view* main_view,
  int timer_id)
{
  assert(main_view);
  assert(timer_id >= 0);
  for (int i = 0; i < list_size(main_view->group_data); ++i) {
    struct Timer_data* timer_data =
      group_data_get_timer(list_get(main_view->group_data, i), timer_id);
    if (timer_data) {
      return timer_data;
    }
  }
  return NULL;
}

static struct Group_data* main_view_get_group(const struct Main_view* main_view,
  int group_id)
{
  assert(main_view);
  assert(group_id >= 0);
  return list_find_arg(main_view->group_data, &group_id,
    (List_compare_arg_fp_t)group_data_compare_id);
}

static struct Timer_data* timer_data_create(int timer_id)
{
  struct Timer_data* timer_data = safe_alloc(sizeof(struct Timer_data));
  timer_data->timer_id = timer_id;
  timer_data->duration = 0;
  timer_data->remaining = 0;
  timer_data->timer_state = TIMER_STATE_NOT_STARTED;
  return timer_data;
}

static void timer_data_destroy(struct Timer_data* timer_data)
{
  assert(timer_data);
  free(timer_data);
}

static int timer_data_compare_id(const int* timer_id_ptr,
  const struct Timer_data* timer_data_other)
{
  assert(timer_id_ptr);
  assert(timer_data_other);
  return *timer_id_ptr - timer_data_other->timer_id;
}

static struct Group_data* group_data_create(int group_id)
{
  struct Group_data* group_data = safe_alloc(sizeof(struct Group_data));
  group_data->group_id = group_id;
  group_data->timer_data = list_create();
  return group_data;
}

static void group_data_destroy(struct Group_data* group_data)
{
  assert(group_data);
  list_for_each(group_data->timer_data, (List_for_each_fp_t)timer_data_destroy);
  list_destroy(group_data->timer_data);
  free(group_data);
}

static void group_data_add_timer(struct Group_data* group_data,
  struct Timer_data* timer_data)
{
  assert(group_data);
  assert(timer_data);
  if (group_data_get_timer(group_data, timer_data->timer_id)) {
    return;
  }
  list_add(group_data->timer_data, timer_data);
}

static void group_data_remove_timer(struct Group_data* group_data, int timer_id)
{
  assert(group_data);
  assert(timer_id >= 0);
  struct Timer_data* timer_data = group_data_get_timer(group_data, timer_id);
  if (!timer_data) {
    return;
  }
  list_remove_ptr(group_data->timer_data, timer_data);
}

static struct Timer_data* group_data_get_timer(
  const struct Group_data* group_data, int timer_id)
{
  assert(group_data);
  assert(timer_id >= 0);
  return list_find_arg(group_data->timer_data, &timer_id,
    (List_compare_arg_fp_t)timer_data_compare_id);
}

static int group_data_compare_id(const int* group_id_ptr,
  const struct Group_data* group_data_other)
{
  assert(group_id_ptr);
  assert(group_data_other);
  return *group_id_ptr - group_data_other->group_id;
}

static void main_view_push(void* data)
{
  assert(data);
  struct Main_view* main_view = (struct Main_view*) data;

  main_view->window = window_create();

  assert(main_view->window);

  window_set_user_data(main_view->window, main_view);
  window_set_window_handlers(main_view->window, (WindowHandlers) {
    .load = window_load_handler,
    .appear = window_appear_handler,
    .unload = window_unload_handler
  });
  window_stack_push(main_view->window, false);
}

static void window_load_handler(Window* window)
{
  struct Main_view* main_view = window_get_user_data(window);
  assert(main_view);

  Layer* window_layer = window_get_root_layer(window);

  // Status bar layer
  main_view->status_bar_layer = status_bar_layer_create();
  layer_add_child(window_layer,
    status_bar_layer_get_layer(main_view->status_bar_layer));

  // Menu layer
  GRect bounds = layer_get_bounds(window_layer);
  bounds = status_bar_adjust_window_bounds(bounds);
  main_view->menu_layer = menu_layer_create(bounds);
  assert(main_view->menu_layer);

  menu_layer_set_callbacks(main_view->menu_layer, main_view,
    (MenuLayerCallbacks) {
      .get_num_sections = menu_get_num_sections_callback,
      .get_num_rows = menu_get_num_rows_callback,
      .get_cell_height = PBL_IF_ROUND_ELSE(menu_cell_get_height_round, NULL),
      .get_header_height = menu_get_header_height_callback,
      .draw_header = menu_draw_header_callback,
      .draw_row = menu_draw_row_callback,
      .select_click = menu_select_click_callback,
      .select_long_click = menu_select_long_click_callback
    }
  );
  menu_layer_set_click_config_onto_window(main_view->menu_layer, window);
  layer_add_child(window_layer, menu_layer_get_layer(main_view->menu_layer));
}

static void window_appear_handler(Window* window)
{
  struct Main_view* main_view = window_get_user_data(window);
  assert(main_view);

  menu_layer_reload_data(main_view->menu_layer);
}

static void window_unload_handler(Window* window)
{
  struct Main_view* main_view = window_get_user_data(window);
  assert(main_view);

  menu_layer_destroy(main_view->menu_layer);
  main_view->menu_layer = NULL;

  status_bar_layer_destroy(main_view->status_bar_layer);
  main_view->status_bar_layer = NULL;

  window_destroy(main_view->window);
  main_view->window = NULL;

  controller_on_view_closed(main_view->view);
}

static uint16_t menu_get_num_sections_callback(MenuLayer* menu_layer,
  void* data)
{
  return MAIN_MENU_NUM_SECTIONS;
}

static uint16_t menu_get_num_rows_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data)
{
  assert(menu_layer);
  assert(data);
  struct Main_view* main_view = (struct Main_view*) data;

  switch (section_index) {
    case 0:
      // Groups
      return list_size(main_view->group_data);
    case 1:
      // App options
      return SETTINGS_NUM_ROWS;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid section index: %d", section_index);
      return 0;
  }
  return 0;
}

static void menu_draw_row_callback(GContext* ctx, const Layer* cell_layer,
  MenuIndex* cell_index, void* data)
{
  switch (cell_index->section) {
    case 0:
      main_view_draw_row(ctx, cell_layer, cell_index->row, data);
      return;
    case 1:
      // App options section
      if (cell_index->row == 0) {
        // New group
        menu_cell_basic_draw(ctx, cell_layer, LOCALIZE("New group"), NULL, NULL);
        return;
      }
      if (cell_index->row == 1) {
        // Settings
        menu_cell_basic_draw(ctx, cell_layer, LOCALIZE("Default settings"), NULL, NULL);
        return;
      }
      if (cell_index->row == 2) {
        // Advanced settings
        menu_cell_basic_draw(ctx, cell_layer, LOCALIZE("Advanced"), NULL, NULL);
        return;
      }
      if (cell_index->row == 3) {
        // App info
        menu_cell_basic_draw(ctx, cell_layer, LOCALIZE("App info"), NULL, NULL);
        return;
      }
#ifndef NDEBUG
      if (cell_index->row == 4) {
        // Create test data
        menu_cell_basic_draw(ctx, cell_layer, LOCALIZE("Create test data"), NULL, NULL);
        return;
      }
#endif /* NDEBUG */
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid section index: %d",
        cell_index->section);
      return;
  }
}

static void main_view_draw_row(GContext* ctx, const Layer* cell_layer,
  uint16_t row_index, void* data)
{
  assert(ctx);
  assert(cell_layer);
  assert(data);
  struct Main_view* main_view = (struct Main_view*) data;

  assert(in_range(row_index, 0, list_size(main_view->group_data)));

  struct Group_data* group_data = list_get(main_view->group_data, row_index);
  assert(group_data);

  // Get the main text
  char menu_text[MENU_TEXT_BUFFER_LENGTH];
  int num_timers = list_size(group_data->timer_data);
  char* timer_text = num_timers == 1 ? LOCALIZE("Timer") : LOCALIZE("Timers");
  snprintf(menu_text, sizeof(menu_text), "%d %s", num_timers, timer_text);

  // Get the subtitle text
  char subtitle_text[SUBTITLE_TEXT_BUFFER_LENGTH];
  subtitle_text[0] = '\0';
  get_subtitle_text(subtitle_text, sizeof(subtitle_text), group_data);

  // Draw the text for the row
  menu_cell_basic_draw(ctx, cell_layer, menu_text,
    strlen(subtitle_text) > 0 ? subtitle_text : NULL, NULL);
}

static void get_subtitle_text(char* buf, int buf_size,
  const struct Group_data* group_data)
{
  assert(buf);
  assert(group_data);
  int end_index = 0;
  char timer_text[TIMER_TEXT_BUFFER_LENGTH];

  // Concat the timers in the group into a list separated by commas, e.g.,
  // 1:23, 11:11, :05
  for (int i = 0;
       i < list_size(group_data->timer_data) && end_index < buf_size;
       ++i) {
    struct Timer_data* timer_data = list_get(group_data->timer_data, i);
    get_timer_text_duration(timer_text, sizeof(timer_text),
      timer_data->remaining);
    end_index += snprintf(buf + end_index, buf_size - end_index, timer_text);
    if (i < list_size(group_data->timer_data) - 1) {
      end_index += snprintf(buf + end_index, buf_size - end_index, ",  ");
    }
  }
}

static void menu_draw_header_callback(GContext* ctx, const Layer* cell_layer,
  uint16_t section_index, void* data)
{
  switch (section_index) {
    case 0:
      // Groups
      menu_cell_draw_header(ctx, cell_layer, LOCALIZE("Timer groups"));
      return;
    case 1:
      // App options
      menu_cell_draw_header(ctx, cell_layer, LOCALIZE("App options"));
      return;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid section index: %d", section_index);
      return;
  }
}

static void menu_select_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data)
{
  assert(data);
  struct Main_view* main_view = (struct Main_view*) data;

  switch (cell_index->section) {
    case 0:
      // Group
      assert(in_range(cell_index->row, 0, list_size(main_view->group_data)));
      struct Group_data* group_data =
        list_get(main_view->group_data, cell_index->row);
      controller_open_group_view(group_data->group_id);
      break;
    case 1:
      // App options
      if (cell_index->row == 0) {
        // New timer group
        controller_open_group_view(controller_create_group());
      } else if (cell_index->row == 1) {
        // Settings
        controller_open_settings_view(-1);
      } else if (cell_index->row == 2) {
        // Advanced settings
        controller_open_advanced_settings_view();
      } else if (cell_index->row == 3) {
        // App info
        controller_open_app_info_view();
      }
#ifndef NDEBUG
      else if (cell_index->row == 4) {
        create_test_data();
      }
#endif /* NDEBUG */
      break;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Invalid section index: %d",
        cell_index->section);
      return;
  }
}

static void menu_select_long_click_callback(MenuLayer* menu_layer,
  MenuIndex* cell_index, void* data)
{
  assert(data);
  struct Main_view* main_view = (struct Main_view*) data;

  if (cell_index->section != 0) {
    return;
  }
  assert(in_range(cell_index->row, 0, list_size(main_view->group_data)));

  struct Group_data* group_data = list_get(main_view->group_data,
    cell_index->row);
  if (list_size(group_data->timer_data) <= 0) {
    return;
  }
  // Find the first timer in the group that is running. If no timers are
  // running, find the first timer that is paused. If no timers are running
  // or paused, select the first timer in the group. Make sure the timer is
  // running and then open the countdown view for the timer.
  struct Timer_data* timer_data = NULL;
  for (int i = 0; i < list_size(group_data->timer_data); ++i) {
    struct Timer_data* timer_tmp = list_get(group_data->timer_data, i);
    if (timer_tmp->timer_state == TIMER_STATE_STARTED ||
        timer_tmp->timer_state == TIMER_STATE_ELAPSED) {
      timer_data = timer_tmp;
      break;
    }
    if (!timer_data && timer_tmp->timer_state == TIMER_STATE_PAUSED) {
      timer_data = timer_tmp;
      // Continue to try to find a timer that is running
      continue;
    }
  }
  if (!timer_data) {
    timer_data = list_get(group_data->timer_data, 0);
  }
  assert(timer_data);
  if (timer_data->timer_state == TIMER_STATE_NOT_STARTED ||
      timer_data->timer_state == TIMER_STATE_PAUSED) {
    controller_start_timer(timer_data->timer_id);
  }
  controller_open_countdown_view(group_data->group_id, timer_data->timer_id);
  return;
}

#ifndef NDEBUG
void create_test_data()
{
  // Timer group 1
  int group_id = controller_create_group();
  int timer_id = controller_create_timer(group_id);
  controller_edit_timer(timer_id, TIMER_FIELD_MINUTES, 15);
  timer_id = controller_create_timer(group_id);
  controller_edit_timer(timer_id, TIMER_FIELD_MINUTES, 45);
  // Repeat style group
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  // Progress style wait for user
  controller_edit_settings(group_id, SETTINGS_FIELD_PROGRESS_STYLE);
  // Vibrate style medium
  controller_edit_settings(group_id, SETTINGS_FIELD_VIBRATE_INTERVAL);
  controller_edit_settings(group_id, SETTINGS_FIELD_VIBRATE_INTERVAL);

  // Timer group 2
  group_id = controller_create_group();
  timer_id = controller_create_timer(group_id);
  controller_edit_timer(timer_id, TIMER_FIELD_SECONDS, 20);
  // Repeat style group
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  // Progress style auto
  controller_edit_settings(group_id, SETTINGS_FIELD_PROGRESS_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_PROGRESS_STYLE);

  // Timer group 3
  group_id = controller_create_group();
  timer_id = controller_create_timer(group_id);
  controller_edit_timer(timer_id, TIMER_FIELD_SECONDS, 5);
  timer_id = controller_create_timer(group_id);
  controller_edit_timer(timer_id, TIMER_FIELD_SECONDS, 30);
  // Repeat style group
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  // Progress style auto
  controller_edit_settings(group_id, SETTINGS_FIELD_PROGRESS_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_PROGRESS_STYLE);

  // Timer group 4
  group_id = controller_create_group();
  timer_id = controller_create_timer(group_id);
  controller_edit_timer(timer_id, TIMER_FIELD_SECONDS, 5);
  timer_id = controller_create_timer(group_id);
  controller_edit_timer(timer_id, TIMER_FIELD_MINUTES, 1);
  // Repeat style group
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  // Progress style auto
  controller_edit_settings(group_id, SETTINGS_FIELD_PROGRESS_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_PROGRESS_STYLE);

  // Timer group 5
  group_id = controller_create_group();
  timer_id = controller_create_timer(group_id);
  controller_edit_timer(timer_id, TIMER_FIELD_SECONDS, 5);
  timer_id = controller_create_timer(group_id);
  controller_edit_timer(timer_id, TIMER_FIELD_SECONDS, 20);
  // Repeat style group
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_REPEAT_STYLE);
  // Progress style auto
  controller_edit_settings(group_id, SETTINGS_FIELD_PROGRESS_STYLE);
  controller_edit_settings(group_id, SETTINGS_FIELD_PROGRESS_STYLE);
}

#endif /*NDEBUG*/

/** @} // Main_view */
