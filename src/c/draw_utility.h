#ifndef DRAW_UTILITY_H
#define DRAW_UTILITY_H

/**
 * @addtogroup draw_utility Draw utility
 * @{
 *
 * Utilities to help with drawing on the screen.
 *
 * These are some utilities and commonly used functions used for drawing
 * graphics on the screen.
 */

#include <pebble.h>

/**
 * Get the height of a menu layer header.
 *
 * @param  menu_layer     The MenuLayer for which the data is requested. Unused.
 * @param  section_index  The section index for which the cell height is
 *                        requested. Unused.
 * @param  data           The callback context. Unused.
 * @return                The height of a menu layer section header.
 */
int16_t menu_get_header_height_callback(MenuLayer* menu_layer,
  uint16_t section_index, void* data);

/**
 * Draw the header of a section of a menu layer.
 *
 * @param ctx        The destination graphics context.
 * @param cell_layer The layer of the cell to draw
 * @param text       Text to draw.
 */
void menu_cell_draw_header(GContext* ctx, const Layer* cell_layer,
  const char* text);

#ifdef PBL_ROUND
/**
 * Get the height of a menu row on a watch with a round face.
 *
 * @param  menu_layer The MenuLayer for which the data is requested.
 * @param  cell_index The MenuIndex for which the cell height is requested.
 * @param  data       The callback context. Unused.
 * @return            Height of the menu row.
 */
int16_t menu_cell_get_height_round(MenuLayer* menu_layer, MenuIndex* cell_index,
  void* data);
#endif

/**
 * Get the {@link Timer} fields as a string formatted as HH:MM:SS.
 *
 * @param buf      Buffer to write the string to.
 * @param buf_size Size of the buffer.
 * @param hours    Hours field of the {@link Timer}.
 * @param minutes  Minutes field of the {@link Timer}.
 * @param seconds  Seconds field of the {@link Timer}.
 */
void get_timer_text(char* buf, int buf_size, int hours, int minutes,
  int seconds);

/**
 * Get the duration as a string formated as HH:MM:SS.
 *
 * @param buf      Buffer to write the string to.
 * @param buf_size Size of the buffer.
 * @param duration Duration in seconds to get as a string.
 */
void get_timer_text_duration(char* buf, int buf_size, int duration);

/**
 * Adjust the given bounds by the amount taken up by a status bar.
 *
 * @param  bounds Bounds to modify.
 * @return        The modified bounds.
 */
GRect status_bar_adjust_window_bounds(GRect bounds);

/**
 * Adjust the given bounds by the amount taken up by an action bar.
 *
 * @param  bounds Bounds to modify.
 * @return        The modified bounds.
 */
GRect action_bar_adjust_window_bounds(GRect bounds);

/**
 * Abstract font sizes to use in the app.
 *
 * Used to define the maximum preferred font size for dynamically setting the
 * font size of a string.
 */
typedef enum {
  /** Font size 0. The smallest font size possible. */
  PREFERRED_FONT_SIZE_0,
  /** Font size 1. */
  PREFERRED_FONT_SIZE_1,
  /** Font size 2. */
  PREFERRED_FONT_SIZE_2,
  /** Font size 3. */
  PREFERRED_FONT_SIZE_3,
  /** Font size 4. */
  PREFERRED_FONT_SIZE_4,
  /** Font size 5. The largest font size possible */
  PREFERRED_FONT_SIZE_5
} Preferred_font_size_t;

/**
 * Get the Leco font that fits in the given frame.
 *
 * @param  preferred_font_size  The {@link Preferred_font_size_t preferred font
 *                              size}.
 * @param  text                 The text that will be drawn in the frame.
 * @param  frame_width          The width of the frame.
 * @param  frame_height         The height of the frame
 * @return                      The font size that is the min of the preferred
 *                              size and the size that will fit in the frame.
 *                              Will always at least return the smallest
 *                              available Leco font.
 */
GFont get_leco_font(const Preferred_font_size_t preferred_font_size,
  const char* text, const int frame_width, const int frame_height);

/**
 * Get the bounds that vertically center the text in the given frame.
 *
 * @param  text         Text to center.
 * @param  font         Font of the text.
 * @param  frame_width  Width of the frame.
 * @param  frame_height Height of the frame.
 * @return              Bounds vertically centered in the given frame. The
 *                      bounds have the minimum height that will contain
 *                      the text.
 */
GRect vertical_center_text(const char* text, const GFont font, int frame_width,
  int frame_height);

/**
 * Get the minimum size of frame/bounds that will contain the text.
 *
 * @param  text  Text to the the min bounds for.
 * @param  font  Font of the text.
 * @return       The minimum size of frame/bounds that will contain the text.
 */
GSize get_min_size_for_text(const char* text, const GFont font);

/**
 * Set the palette of the given bitmap.
 *
 * @param bitmap                Bitmap to set the palette of.
 * @param highlighted           Zero if the bitmap is highlighted, non-zero
 *                              otherwise.
 */
void set_palette(GBitmap* bitmap, int highlighted);

/** @} // draw_utility */

#endif /*DRAW_UTILITY_H*/
