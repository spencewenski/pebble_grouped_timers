Publishing steps
================

Steps to publish a new version of the app to the Pebble app store.

## Steps
1. Increment the version number in the Makefile
2. Update any app store assets
    - Screenshots
    - Banner screenshots
    - Update the banners.md and/or screenshots.md if necessary
3. Update the CHANGELOG
4. Update the app store description if necessary
5. Update the README.md if necessary
    - At least add the new release to the download instructions
6. Package all the above changes into a merge request
7. Once merged, create a tag that corresponds to the new version number in
   the Makefile
    - e.g., name the tag 1.2 for version number 1.2
    - Tag descriptions for version numbers are a bit redundant, so they are
    optional
8. Push the tag
9. Once the gitlab pipeline completes, download the build artifacts from the
   'release_publish' stage
10. Go to the [Pebble dev portal](https://dev-portal.getpebble.com/)
11. Upload the `.pbw` file and paste the new parts of the CHANGELOG into the
    changelog section of the app submission form.
12. Upload any other modified assets
