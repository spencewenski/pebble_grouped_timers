#!/usr/bin/env python

"""
Modify values of the package.json file of a Pebble app.
"""

import sys
import argparse
import json

def update_file(package_json, filename):
  """Write the new package json to the given file.

  :param package_json: Json object to write to the file.
  :param filename: Name of the file to write to.
  """
  with open(filename, 'wb') as data_file:
    json.dump(package_json, data_file, indent=4, sort_keys=True, separators=(',', ': '))


def read_json_file(filename):
  """Read the given file into a json object.

  :param filename: Name of the file to read.
  """
  with open(filename) as data_file:
    return json.load(data_file)


def parse_command_line_args(argv):
  """Parse the command line arguments into an argparse object.

  :param argv: List of command line arguments.
  """
  parser = argparse.ArgumentParser(__doc__)
  parser.add_argument('-f', '--file', help='Resume JSON data',
    default="package.json")
  parser.add_argument('-u', '--uuid', help='New uuid')
  parser.add_argument('-v', '--version', help='App version')
  parser.add_argument('-d', '--display', help='App display name')
  parser.add_argument('-n', '--name', help='App name')
  return parser.parse_args(argv)


def main(argv):
  args = parse_command_line_args(argv)
  package_json = read_json_file(args.file)
  if args.uuid:
    package_json['pebble']['uuid'] = args.uuid

  if args.version:
    package_json['version'] = args.version

  if args.display:
    package_json['pebble']['displayName'] = args.display

  if args.name:
    package_json['name'] = args.name

  update_file(package_json, args.file)


if __name__ == '__main__':
  main(sys.argv[1:])
