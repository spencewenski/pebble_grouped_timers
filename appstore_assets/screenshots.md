Screenshots
===========

## Setup
1. App
    - Settings:
        - Repeat Style: Repeat Group
        - Progress Style: Wait for user
        - Vibrate Interval: Every minute
        - Vibrate Style: Double pulse
2. Group 1
    - Timers:
        - :30
        - 11:11
    - Settings:
        - Repeat Style: Repeat single
        - Progress Style: Auto start next
        - Vibrate Interval: Use default settings
        - Vibrate Style: Use default settings
3. Group 2
    - Timers:
        - :42
        - 1:00:00
    - Settings: (Doesn't matter, will just show settings for group 1)

## Screenshots
1. Main_view
    - Basalt, Diorite, Emery
        - Highlight first line
    - Chalk
        - Highlight second line
2. Group_view
    - Group 1
    - Start the 11:11 timer
    - Basalt, Diorite, Emery
        - Highlight first line
    - Chalk
        - Highlight second line
3. Countdown_view
    - Group 1
    - 11:11 timer
    - Start the timer and wait for it to get to <= 10:59
4. Settings_view
    - App settings
    - Basalt, Diorite, Emery
        - Highlight first line
    - Chalk
        - Highlight second line
5. Settings_view
    - Group 1 settings
    - Basalt, Diorite, Emery
        - Highlight first line
    - Chalk
        - Highlight second line
