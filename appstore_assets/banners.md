Banners
=======

## Banner 0
### Text
- Group timers into lists
- Repeat timers and groups
- Auto-start next timer or wait for user input
### Screenshot
- Same as Screenshot 1

## Banner 1
### Text
- Group settings can either use or override the default settings
### Screenshot
- Same as Screenshot 5

## Banner 2
### Text
- No hard-coded limit on the number of groups or timers
- Supports running multiple timers simultaneously
### Screenshot setup
1. Create a bunch of timers and groups
    - Group 1: 20 timers
        - Chalk: times don't matter
        - :30
        - 11:11
        - 1:08:00
        - 1:00
    - Group 2: 2 timers
        - :42
        - 1:00:00
    - Group 3: 15 timers
        - Chalk: times don't matter
        - 1:00:10
        - 50:00
        - 15:10
### Screenshot
1. Main_view
    - Basalt, Diorite, Emery
        - Highlight first line
    - Chalk
        - Highlight second line


