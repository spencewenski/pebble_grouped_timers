# Makefile to make it easy to switch between release and debug targets

OPTIONS :=
PROG := prog
# Pebble device/emulator names
EMULATOR_A := aplite
EMULATOR_B := basalt
EMULATOR_C := chalk
EMULATOR_D := diorite
EMULATOR_E := emery
EMULATOR_TARGET := $(EMULATOR_B)
# App UUID
RELEASE_UUID := 51bc9efb-8881-449e-bec6-6a7e59a69c80
BETA_UUID := ed6628bd-8bbd-4012-9803-252f9731cc67
UUID := $(RELEASE_UUID)
# App version
RELEASE_VERSION := 1.5
BETA_VERSION := 1.4
VERSION := $(RELEASE_VERSION)
# App display name
RELEASE_DISPLAY_NAME := Grouped Timers
BETA_DISPLAY_NAME := [Beta] $(RELEASE_DISPLAY_NAME)
DISPLAY_NAME := $(RELEASE_DISPLAY_NAME)
# App name
RELEASE_NAME := grouped-timers
BETA_NAME := grouped-timers-beta
NAME := $(RELEASE_NAME)

all: $(PROG)

debug: OPTIONS +=
debug: $(PROG)

opt: OPTIONS +=
opt: $(PROG)

beta: OPTIONS += --release
beta: UUID = $(BETA_UUID)
beta: VERSION = $(BETA_VERSION)
beta: DISPLAY_NAME = $(BETA_DISPLAY_NAME)
beta: NAME = $(BETA_NAME)
beta: opt

release: OPTIONS += --release
release: opt

install_a: EMULATOR_TARGET := $(EMULATOR_A)
install_a: install
install_b: EMULATOR_TARGET := $(EMULATOR_B)
install_b: install
install_c: EMULATOR_TARGET := $(EMULATOR_C)
install_c: install
install_d: EMULATOR_TARGET := $(EMULATOR_D)
install_d: install
install_e: EMULATOR_TARGET := $(EMULATOR_E)
install_e: install

localize:
	src/c/locale_framework/gen_dict.py src/c resources/i18n/locale_en_us.json
	src/c/locale_framework/dict2bin.py resources/i18n/locale_es_es.json
	src/c/locale_framework/dict2bin.py resources/i18n/locale_fr_fr.json
	src/c/locale_framework/dict2bin.py resources/i18n/locale_de_de.json
	src/c/locale_framework/dict2bin.py resources/i18n/locale_it_it.json
	src/c/locale_framework/dict2bin.py resources/i18n/locale_pt_pt.json

$(PROG): localize
	python edit_package_json.py -f 'package.json' -u "$(UUID)" -v "$(VERSION)" -d "$(DISPLAY_NAME)" -n "$(NAME)"
	pebble build options $(OPTIONS)

install:
	pebble install --emulator $(EMULATOR_TARGET)

clean:
	pebble clean
	rm resources/i18n/*.bin

# Clear persisted data from running emulators
clean_emulators:
	pebble wipe && pebble kill
